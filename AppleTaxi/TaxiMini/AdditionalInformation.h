//
//  AdditionalInformation.h
//  TaxiMini
//
//  Created by Nail Sharipov on 16/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhoneNumberField.h"

@interface AdditionalInformation : UIViewController<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet PhoneNumberField *additonalPhoneNumber;

@end
