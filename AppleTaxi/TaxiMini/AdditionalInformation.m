//
//  AdditionalInformation.m
//  TaxiMini
//
//  Created by Nail Sharipov on 16/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "AdditionalInformation.h"
#import "UserData.h"

@interface AdditionalInformation ()

@end

@implementation AdditionalInformation

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:@"UIKeyboardDidHideNotification"
                                               object:nil];
*/
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UserData *user = [UserData getInstance];
    _additonalPhoneNumber.unformatText = user.secondPhoneNumber;
//    _comment.text = user.comment;
}

- (IBAction)navigationPressed:(id)sender
{
    NSString *secondPhone = _additonalPhoneNumber.unformatText;
    
    UserData *user = [UserData getInstance];
    
    if (secondPhone.length == 10) {
        user.secondPhoneNumber = secondPhone;
    }
    
    [[self navigationController] popViewControllerAnimated:YES];
}
/*
- (void) keyboardWillShow:(NSNotification *)note {
    // resize tableView
    NSDictionary *userInfo = [note userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _textBottomSpace.constant = kbSize.height;
}

- (void) keyboardDidHide:(NSNotification *)note {
    // fullscreen
    _textBottomSpace.constant = 160.0f;
}


- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    return NO;
}
*/
@end
