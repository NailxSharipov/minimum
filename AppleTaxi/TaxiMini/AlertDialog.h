//
//  AlertDialog.h
//  TaxiMini
//
//  Created by Nail Sharipov on 25/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertDialog : UIAlertView<UIAlertViewDelegate>

typedef void (^onCloseHandler)();;

@property(nonatomic, copy) onCloseHandler onClose;

+ (void)dialogWithText:(NSString *)text onClose:(onCloseHandler)onClose;
+ (void)messageWithText:(NSString *)text;
+ (void)errorWithText:(NSString *)text;


@end
