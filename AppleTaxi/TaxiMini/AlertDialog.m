//
//  AlertDialog.m
//  TaxiMini
//
//  Created by Nail Sharipov on 25/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "AlertDialog.h"

@implementation AlertDialog

- (id)initWithTitle:(NSString *)title message:(NSString *)message onClose:(onCloseHandler)onClose {
    self = [super initWithTitle:title message:message delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:@"OK", nil];
    if (self) {
        _onClose = onClose;
        [self setDelegate:self];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidEnterBackground:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
    }
    
    return self;
}

+ (void)dialogWithText:(NSString *)text onClose:(onCloseHandler)onClose {
    AlertDialog *alertDialog = [[AlertDialog alloc] initWithTitle:@"" message:text onClose:onClose];
    [alertDialog show];
}

+ (void)messageWithText:(NSString *)text {
    AlertDialog *alertDialog = [[AlertDialog alloc] initWithTitle:@"" message:text delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertDialog show];
}

+ (void)errorWithText:(NSString *)text
{
    AlertDialog *alertDialog = [[AlertDialog alloc] initWithTitle:@"Ошибка" message:text delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertDialog show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex) {
        _onClose();
    }
}

- (void)applicationDidEnterBackground:(NSNotification *)notification {
    [super dismissWithClickedButtonIndex:[self cancelButtonIndex] animated:NO];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}


@end
