//
//  B52Label.m
//  TaxiMini
//
//  Created by Nail Sharipov on 27/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "B52Label.h"

@implementation B52Label

- (void) baseInit
{
    self.font = [UIFont fontWithName:@"B52" size:self.font.pointSize];
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self baseInit];
    }
    return self;
}

@end
