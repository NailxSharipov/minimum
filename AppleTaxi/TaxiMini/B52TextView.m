//
//  B52TextView.m
//  TaxiMini
//
//  Created by Nail Sharipov on 27/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "B52TextView.h"

@implementation B52TextView

- (void) baseInit
{
    self.font = [UIFont fontWithName:@"B52" size:self.font.pointSize];
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self baseInit];
    }
    return self;
}

@end
