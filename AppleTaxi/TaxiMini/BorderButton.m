//
//  BorderButton.m
//  TaxiMini
//
//  Created by Nail Sharipov on 02/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "BorderButton.h"

@implementation BorderButton

- (void)commonInit
{
    CALayer *top = [CALayer layer];
    top.frame = CGRectMake(0.0f, 0.0f, self.frame.size.width, 1.0f);
    top.backgroundColor = [UIColor colorWithWhite:0.4f alpha:0.4f].CGColor;
    [self.layer addSublayer:top];
    
    CALayer *bottom = [CALayer layer];
    bottom.frame = CGRectMake(0.0f, self.frame.size.height - 1.0f, self.frame.size.width, 1.0f);
    bottom.backgroundColor = [UIColor colorWithWhite:0.4f alpha:0.4f].CGColor;
    [self.layer addSublayer:bottom];
    
    //self.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.4f];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

@end
