//
//  BorderWhiteButton.h
//  TaxiMini
//
//  Created by Nail Sharipov on 04/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "B52Button.h"

@interface BorderWhiteButton : B52Button

@property(nonatomic) BOOL isTopBorder;
@property(nonatomic) BOOL isBottomBorder;
@property(weak, nonatomic, readonly) UIImageView* leftIco;
@property(weak, nonatomic, readonly) UIImageView* rightIco;

@end
