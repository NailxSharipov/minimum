//
//  BorderWhiteButton.m
//  TaxiMini
//
//  Created by Nail Sharipov on 04/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "BorderWhiteButton.h"

@implementation BorderWhiteButton
{
    CALayer *_bottomBorder;
    CALayer *_topBorder;
}

- (void)commonInit
{
    CGSize size = self.frame.size;
    
    _topBorder = [CALayer layer];
    _topBorder.frame = CGRectMake(0.0f, 0.0f, size.width, 1.0f);
    _topBorder.backgroundColor = [UIColor colorWithWhite:0.4f alpha:0.4f].CGColor;
    [self.layer addSublayer:_topBorder];
    
    _bottomBorder = [CALayer layer];
    _bottomBorder.frame = CGRectMake(0.0f, size.height - 1.0f, size.width, 1.0f);
    _bottomBorder.backgroundColor = [UIColor colorWithWhite:0.4f alpha:0.4f].CGColor;
    [self.layer addSublayer:_bottomBorder];
    
    self.backgroundColor = [UIColor colorWithWhite:1.0f alpha:1.0f];


//    self.titleLabel.font = [UIFont fontWithName:@"B52" size:18];
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self setTitleColor:[UIColor darkTextColor] forState: UIControlStateNormal];
    
    CGFloat icoSize = 36.0f;
    CGFloat margin = 0.5f * (size.height - icoSize);
    {
        CGRect rect = CGRectMake(margin, margin, icoSize, icoSize);
        UIImageView *ico = [[UIImageView alloc] initWithFrame:rect];
        [self addSubview:ico];
        _leftIco = ico;
    }
    
    {
        CGRect rect = CGRectMake(size.width - margin - icoSize, margin, icoSize, icoSize);
        UIImageView *ico = [[UIImageView alloc] initWithFrame:rect];
        [self addSubview:ico];
        _rightIco = ico;
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)setIsTopBorder:(BOOL)isTopBorder
{
    _isTopBorder = isTopBorder;
    _topBorder.hidden = !isTopBorder;
}

- (void)setIsBottomBorder:(BOOL)isBottomBorder
{
    _isBottomBorder = isBottomBorder;
    _bottomBorder.hidden = !isBottomBorder;
}
@end
