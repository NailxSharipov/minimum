//
//  Brand.h
//  Minimum
//
//  Created by Nail Sharipov on 15/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Brand : NSObject

@property (strong, nonatomic) NSString *brandId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *phone;

@end
