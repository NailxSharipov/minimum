//
//  ChangePassword.h
//  TaxiMini
//
//  Created by Nail Sharipov on 15/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePassword : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *password;
@end
