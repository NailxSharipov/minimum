//
//  ChangePassword.m
//  TaxiMini
//
//  Created by Nail Sharipov on 15/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "ChangePassword.h"
#import "TaxiMiniGetAPI.h"
#import "AlertDialog.h"
#import "UserData.h"

@interface ChangePassword ()

@end

@implementation ChangePassword

- (IBAction)navigationPressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)changePasswordPressed:(id)sender {
    
    NSString *password = _password.text;
    if (password == nil || password.length < 5) {
        [AlertDialog messageWithText:@"Пароль должен быть не меньше 5 символов"];
    } else {
        TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
        [getAPI changePassword:password OnReady: ^(NSString *message)
         {
             if (message == nil) {
                 [AlertDialog messageWithText:@"Пароль изменен"];
                 [UserData getInstance].password = password;
             } else {
                 [AlertDialog messageWithText:message];
             }
         }];
    }
    
}
@end
