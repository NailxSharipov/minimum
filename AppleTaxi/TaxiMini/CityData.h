//
//  CityData.h
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityData : NSObject

@property (strong, nonatomic) NSString *cityId;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *url;

@property (strong, nonatomic) NSString *brandId;
@property (strong, nonatomic) NSString *brand;
@property (strong, nonatomic) NSString *phonePrefix;
@property (strong, nonatomic) NSString *money;

- (id) initWithDictionary: (NSDictionary *)dictionary;
- (id) init;

@end
