//
//  CityData.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "CityData.h"

@implementation CityData

- (id) initWithDictionary: (NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        {
            NSString *value = [dictionary objectForKey:@"id"];
            if (value != nil) {
                _cityId = value;
            }
        }
        {
            NSString *value = [dictionary objectForKey:@"title"];
            if (value != nil) {
                _title = value;
            }
        }
        {
            NSString *value = [dictionary objectForKey:@"url"];
            if (value != nil) {
                _url = value;
            }
        }
    }
    return self;
}

- (id) init
{
    self = [super init];
    if (self) {
        _cityId = nil;
        _title = @"Уфа";
        _url = nil;
    }
    return self;
}


@end
