//
//  CitySelectScreen.h
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitySelectScreen : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *titleCity;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end
