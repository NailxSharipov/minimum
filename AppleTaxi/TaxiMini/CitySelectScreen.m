//
//  CitySelectScreen.m
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "CitySelectScreen.h"
#import "ServerData.h"
#import "UserData.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "TaxiMiniGetAPI.h"

@implementation CitySelectScreen
{
    NSArray *_cities;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _cities = [[ServerData getInstance] cities];
    [_table addPullToRefreshWithActionHandler:^{
        TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
        [getAPI loadCitiesOnReady:^(NSArray *cities)
        {
            [ServerData getInstance].cities = cities;
            _cities = cities;
            [_table.pullToRefreshView stopAnimating];
            [_table reloadData];
        }
        ];
    }];
    _table.pullToRefreshView.arrowColor = [UIColor whiteColor];
    [_table.pullToRefreshView setTitle:@"" forState: SVPullToRefreshStateAll];
    [_table.pullToRefreshView setSubtitle:@"" forState: SVPullToRefreshStateAll];
    
    TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
    [getAPI loadCitiesOnReady:^(NSArray *cities)
     {
         [ServerData getInstance].cities = cities;
         _cities = cities;
         [_table reloadData];
     }
     ];

}

-(void) viewWillAppear:(BOOL)animated
{
    CityData *cityData = [UserData getInstance].city;
    if (cityData != nil) {
        [self.titleCity setTitle: cityData.title forState: UIControlStateNormal];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_cities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CityTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont fontWithName:@"B52" size:18];
    }
    
    CityData * cityData = [_cities objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [cityData title];
    
    NSString *userCityId = [[[UserData getInstance] city] cityId];
    NSString *cityId = [cityData cityId];
    
    if ([cityId isEqualToString:userCityId]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CityData * cityData = [_cities objectAtIndex:indexPath.row];
    [[UserData getInstance] setCity:cityData];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)navigationPressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}


@end
