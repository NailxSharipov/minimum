//
//  CurrentOrder.h
//  Minimum
//
//  Created by Nail Sharipov on 09/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentOrder : UIViewController

+(CurrentOrder *)getInstance;

@end
