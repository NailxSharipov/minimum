//
//  CurrentOrder.m
//  Minimum
//
//  Created by Nail Sharipov on 09/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "CurrentOrder.h"
#import "OrderData.h"
#import "TaxiMiniGetAPI.h"
#import "OrderManager.h"
#import "AlertDialog.h"
#import "TXCurrentOrderTableViewSource.h"
#import "TXOrderCellEvent.h"

@interface CurrentOrder () <TXOrderCellEvent>
@property (weak, nonatomic) IBOutlet UITableView *orderView;


@end

@implementation CurrentOrder
{
    TXCurrentOrderTableViewSource *_currentOrderTableViewSource;
    NSTimer *_timer;
}

static CurrentOrder *_instance = nil;

+(CurrentOrder *)getInstance
{
    if (_instance == nil) {
        _instance = [[CurrentOrder alloc] init];
    }
    return _instance;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _instance = self;
   
    _currentOrderTableViewSource = [[TXCurrentOrderTableViewSource alloc] init];
    _currentOrderTableViewSource.tableView = _orderView;
    _currentOrderTableViewSource.delegate = self;

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadOrder];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:20.0
                                     target:self
                                   selector:@selector(targetMethod:)
                                   userInfo:nil
                                    repeats:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    if (_timer != nil) {
        [_timer invalidate];
    }
}

-(void)reloadOrder {
    TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
    [getAPI getCurrentOrderOnReady:^(NSArray *orders) {
        [_currentOrderTableViewSource setItems:orders];
    }];
}


- (IBAction)navigationPressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void) targetMethod:(NSTimer *)timer
{
    [self reloadOrder];
}

- (void)onCancel:(OrderData*)orderData {
    TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
    
    [getAPI cancelOrder:orderData.orderId OnCancel:^(BOOL result) {
        if (result) {
            [[OrderManager getInstance] cancelOrderId:orderData.orderId];
            [self reloadOrder];
            [AlertDialog messageWithText:@"Ваш заказ отменен"];
        }
    }];
}

@end
