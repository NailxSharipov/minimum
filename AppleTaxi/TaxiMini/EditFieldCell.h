//
//  PhoneNumberCell.h
//  TaxiMini
//
//  Created by Nail Sharipov on 01/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditFieldCell : UITableViewCell

+ (CGFloat) HEIGHT;
+ (NSString *) PHONE_NUMBER;
+ (NSString *) PASSWORD;

@property (strong, nonatomic) UITextField* editField;

@end
