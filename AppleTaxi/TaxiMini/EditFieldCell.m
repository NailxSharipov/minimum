//
//  PhoneNumberCell.m
//  TaxiMini
//
//  Created by Nail Sharipov on 01/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "EditFieldCell.h"
#import "PhoneNumberField.h"
#import "B52Label.h"

@implementation EditFieldCell

static CGFloat height = 44.0f;
static CGFloat margin = 2.0f;
static NSString *phoneNumber = @"PHONE_NUMBER";
static NSString *password = @"PASSWORD";

+ (CGFloat) HEIGHT
{
    return height;
}

+ (NSString *) PHONE_NUMBER
{
    return phoneNumber;
}

+ (NSString *) PASSWORD
{
    return password;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat x = margin;
        CGFloat imgSize = height - 2.0f * margin;
        CGRect imgRect = CGRectMake(x, margin, imgSize, imgSize);
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:imgRect];
        [self addSubview:imgView];
    
        x = x + imgSize + margin;
        CGFloat titleWidth = 82.0f;
        CGRect titleRect = CGRectMake(x, margin, titleWidth, imgSize);
        
        UILabel *title = [[B52Label alloc] initWithFrame:titleRect];
        [self addSubview:title];
        
        x = x + titleWidth + margin;
        CGFloat editFieldWidth = self.frame.size.width - x - margin;
        CGRect editFieldRect = CGRectMake(x, margin, editFieldWidth, imgSize);
        
        if ([reuseIdentifier isEqualToString:phoneNumber]) {
            imgView.image = [UIImage imageNamed:@"ico_phone"];
            title.text = @"Телефон";
            _editField = [[PhoneNumberField alloc] initWithFrame:editFieldRect];
            
        } else if ([reuseIdentifier isEqualToString:password]) {
            imgView.image = [UIImage imageNamed:@"ico_lock"];
            title.text = @"Пароль";
            _editField = [[TextFieldModern alloc] initWithFrame:editFieldRect];
        }
        title.font = [UIFont fontWithName:@"B52" size:18.0f];
        _editField.font = [UIFont fontWithName:@"B52" size:18.0f];
        _editField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self addSubview:_editField];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
