//
//  GeoLocation.m
//  TaxiMini
//
//  Created by Nail Sharipov on 10/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "GeoLocationGoogle.h"
#import "URLEditor.h"
#import "HttpTextDataRequest.h"

@implementation GeoLocationGoogle

static NSString* SERVER_URL = @"http://maps.googleapis.com/maps/api/geocode/json";

- (void)searchByLocationLat:(CGFloat)lat Lon:(CGFloat)lon OnReadyLocation:(OnReadyLocationHandler)onReady
{
    [self searchByName:nil Lat:lat Lon:lon OnReadyLocation:onReady];
}

- (LocationData *)parseLocationFromText :(NSDictionary *)json {
    NSArray *data = json[@"results"];
    
    for (NSDictionary *obj in data) {
        NSArray *types = obj[@"types"];
        
        if ([self isContain:types Key:@"street_address"]) {
            NSArray *components = obj[@"address_components"];
            NSString *number = nil;
            NSString *street = nil;
        
            
            for (NSDictionary *componentData in components) {
                NSArray *componentTypes = componentData[@"types"];
                if ([self isContain:componentTypes Key:@"street_number"]) {
                    number = componentData[@"long_name"];
                }
                if ([self isContain:componentTypes Key:@"route"]) {
                    street = componentData[@"long_name"];
                }
            }
            
            if (number != nil && street != nil) {
                LocationData *locationData = [[LocationData alloc] init];
                
                NSDictionary *geometry = obj[@"geometry"];
                if (geometry != nil) {
                    NSDictionary *location = geometry[@"location"];
                    float lat = [location[@"lat"] floatValue];
                    float lng = [location[@"lng"] floatValue];
                    
                    locationData.lat = lat;
                    locationData.lng = lng;
                    
                    NSLog(@"lat: %f, lng: %f", lat, lng);
                }
                locationData.number = number;
                locationData.street = street;
                
                return locationData;
            }
        }
    }
    
    return nil;
}

- (void)searchByName: (NSString*)name Lat:(CGFloat)lat Lon:(CGFloat)lon OnReadyLocation:(OnReadyLocationHandler)onReady
{
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:SERVER_URL];
    
    if (name != nil) {
        [urlEditor addParameter:@"address" Value:name];
    }
    
    if (lat != 0.0 && lon != 0.0) {
        [urlEditor addParameter:@"latlng" Value:[NSString stringWithFormat:@"%f,%f", lat, lon]];
    }
    
    [urlEditor addParameter:@"sensor" Value:@"true"];
    [urlEditor addParameter:@"language" Value:@"ru"];
    
    
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                 onDataReady:^(NSData* data)
     {
         NSError *error = nil;
         
         NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                              options:NSJSONReadingMutableContainers
                                                                error:&error];
         
         if (error) {
             onReady(nil);
         } else {
             LocationData *loacation = [self parseLocationFromText:json];
             onReady(loacation);
         }
     }
                     onError:^()
     {
         NSLog(@"Error place search");
         onReady(nil);
     }];
}


- (BOOL)isContain:(NSArray *)data Key: (NSString *)key
{
    for (NSString *str in data) {
        if ([str isEqualToString: key]) {
            return YES;
        }
    }
    return NO;
}


@end
