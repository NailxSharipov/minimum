//
//  GeoLocationOSMNominatim.h
//  Minimum
//
//  Created by Nail Sharipov on 06/10/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationData.h"

@interface GeoLocationOSMNominatim : NSObject

typedef void (^OnReadyLocationHandler)(LocationData *);
- (void)searchByLocationLat:(CGFloat)lat Lon:(CGFloat)lon OnReadyLocation:(OnReadyLocationHandler)onReady;
- (void)searchByName: (NSString*)name Lat:(CGFloat)lat Lon:(CGFloat)lon OnReadyLocation:(OnReadyLocationHandler)onReady;

@end
