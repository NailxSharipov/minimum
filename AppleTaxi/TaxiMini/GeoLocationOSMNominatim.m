//
//  GeoLocationOSMNominatim.m
//  Minimum
//
//  Created by Nail Sharipov on 06/10/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "GeoLocationOSMNominatim.h"
#import "URLEditor.h"
#import "HttpTextDataRequest.h"

@implementation GeoLocationOSMNominatim

static NSString* SERVER_URL = @"http://nominatim.openstreetmap.org/reverse";

- (void)searchByLocationLat:(CGFloat)lat Lon:(CGFloat)lon OnReadyLocation:(OnReadyLocationHandler)onReady
{
    [self searchByName:nil Lat:lat Lon:lon OnReadyLocation:onReady];
}

- (LocationData *)parseLocationFromText :(NSDictionary *)json {
    LocationData *locationData = nil;
    NSDictionary *address = json[@"address"];
    if (address != nil) {
        NSString *houseNumber = address[@"house_number"];
        NSString *road = address[@"road"];
        
        if (houseNumber != nil && road != nil) {
            locationData = [[LocationData alloc] init];

            locationData.number = houseNumber;
            locationData.street = road;
            
            NSLog(@"%@ %@", road, houseNumber);
        }
    }
    return locationData;
}

- (void)searchByName: (NSString*)name Lat:(CGFloat)lat Lon:(CGFloat)lon OnReadyLocation:(OnReadyLocationHandler)onReady
{
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:SERVER_URL];

    [urlEditor addParameter:@"format" Value:@"json"];
//    [urlEditor addParameter:@"zoom" Value:@"17"];
    
    if (name != nil) {
//        [urlEditor addParameter:@"address" Value:name];
    }
    
    if (lat != 0.0 && lon != 0.0) {
        [urlEditor addParameter:@"lat" Value:[NSString stringWithFormat:@"%f", lat]];
        [urlEditor addParameter:@"lon" Value:[NSString stringWithFormat:@"%f", lon]];
    }
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                 onDataReady:^(NSData* data)
    {
        NSError *error = nil;

        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];

        if (error) {
            onReady(nil);
        } else {
            LocationData *loacation = [self parseLocationFromText:json];
            onReady(loacation);
        }
    }
        onError:^()
    {
        NSLog(@"Error place search");
        onReady(nil);
    }];
}


- (BOOL)isContain:(NSArray *)data Key: (NSString *)key
{
    for (NSString *str in data) {
        if ([str isEqualToString: key]) {
            return YES;
        }
    }
    return NO;
}

@end
