//
//  HistoryCellEvent.h
//  Minimum
//
//  Created by Nail Sharipov on 09/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HistoryData.h"

@protocol HistoryCellEvent <NSObject>

@required
- (void)onRepeat:(HistoryData*)historyData;

@optional
- (void)onDelete:(HistoryData*)historyData;


@end
