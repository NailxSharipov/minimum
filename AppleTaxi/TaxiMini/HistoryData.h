//
//  HistoryData.h
//  Minimum
//
//  Created by Nail Sharipov on 31/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderData.h"

@interface HistoryData : NSObject

@property(strong, nonatomic) NSString *date;
@property(strong, nonatomic, readonly) NSDate *dateTime;
@property(strong, nonatomic) OrderData *order;

-(BOOL)isEqualTo: (HistoryData *)second;

@end
