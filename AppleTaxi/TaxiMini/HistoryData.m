//
//  HistoryData.m
//  Minimum
//
//  Created by Nail Sharipov on 31/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "HistoryData.h"
#import "PlaceData.h"

@implementation HistoryData

static NSDateFormatter* _dateFormat;

- (void)setDate:(NSString *)date
{
    //28.08.2014 12:44
    if (_dateFormat == nil) {
        _dateFormat = [[NSDateFormatter alloc] init];
        [_dateFormat setDateFormat:@"dd.MM.yyyy hh:mm"];
    }
    _dateTime = [_dateFormat dateFromString:date];
    _date = date;
}

-(BOOL)isEqualTo: (HistoryData *)second
{
    NSUInteger n = _order.places.count;
    if (n != second.order.places.count) {
        return NO;
    } else if (_order.places.count > 1) {
        BOOL equalFlag = YES;
        for (NSUInteger i = 0; i < n; i++) {
            PlaceData * placeA = _order.places[i];
            PlaceData * placeB = second.order.places[i];
            if (![placeA isEqual: placeB]) {
                equalFlag = NO;
                break;
            }
        }
        return equalFlag;
    } else {
        return NO;
    }
}

@end
