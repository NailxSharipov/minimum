//
//  HistoryManager.h
//  Minimum
//
//  Created by Nail Sharipov on 31/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryManager : NSObject

@property (strong, nonatomic, readonly) NSMutableArray* histories;
@property (strong, nonatomic, readonly) NSArray* places;

+ (instancetype)getInstance;

- (void) loadHistory;

- (void) clear;

@end
