//
//  HistoryManager.m
//  Minimum
//
//  Created by Nail Sharipov on 31/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "HistoryManager.h"
#import "TaxiMiniGetAPI.h"
#import "HistoryData.h"
#import "PlaceData.h"
#import "PlaceDataTime.h"

@implementation HistoryManager

static HistoryManager *_instance;

- (id) init
{
    self = [super init];
    if (self) {
        _histories = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (instancetype)getInstance
{
    if (_instance == nil) {
        _instance = [[HistoryManager alloc] init];
    }
    
    return _instance;
}

- (void) loadHistory
{
    TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
    [getAPI getHistoryOnReady:^(NSArray *histories)
     {
         [self fillHistory:histories];
     }];
    
}

- (void) fillHistory: (NSArray *)histories
{
    if (histories != nil) {
    
        //_histories = histories;
        NSArray* sortHistories = [histories sortedArrayUsingComparator:^NSComparisonResult(HistoryData *h0, HistoryData *h1){
            return [h1.dateTime compare:h0.dateTime];
        }];

        _histories = [[NSMutableArray alloc] initWithArray: sortHistories];

        // delete repeats
        {
            NSUInteger n = _histories.count;
            for(NSUInteger i = n - 1; i > 0; i--) {
                for(NSInteger j = i - 1; j >= 0; j--) {
                    if ([_histories[i] isEqualTo: _histories[j]]) {
                        [_histories removeObjectAtIndex:i];
                        break;
                    }
                }
            }
        }

        NSMutableArray *allPlaces = [[NSMutableArray alloc] init];
        
        for (HistoryData *history in histories) {
            for (PlaceData *place in history.order.places) {
                PlaceDataTime *placeTime = [[PlaceDataTime alloc] init];
                placeTime.time = history.dateTime;
                placeTime.place = place;
                [allPlaces addObject:placeTime];
            }
        }
        
        NSArray *sortedPlaces = [allPlaces sortedArrayUsingComparator:^NSComparisonResult(PlaceDataTime *pdt0, PlaceDataTime *pdt1){
            return [pdt1.time compare:pdt0.time];
        }];
        
        NSMutableArray *uniquePlaces = [[NSMutableArray alloc] init];
        for (PlaceDataTime *placeTime in sortedPlaces) {
            
            NSString *idPlace = placeTime.place.objectAddress;
            BOOL isUnique = YES;
            for (PlaceData *place in uniquePlaces) {
                if ([place.objectAddress isEqualToString:idPlace]) {
                    isUnique = NO;
                    break;
                }
            }
            
            if (isUnique) {
                [uniquePlaces addObject:placeTime.place];
            }
        }
        
        _places = uniquePlaces;
    }
}

- (void) clear
{
    _histories = nil;
    _places = nil;
}


@end
