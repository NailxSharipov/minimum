//
//  HistoryMenuCell.h
//  Minimum
//
//  Created by Nail Sharipov on 09/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryData.h"
#import "HistoryCellEvent.h"

@interface HistoryMenuCell : UITableViewCell

@property (weak, nonatomic) HistoryData* historyData;
@property (weak, nonatomic) NSObject<HistoryCellEvent>* delegate;

+ (CGFloat) getHeightCount:(NSUInteger) count;



@end
