//
//  HistoryMenuCell.m
//  Minimum
//
//  Created by Nail Sharipov on 09/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "HistoryMenuCell.h"
#import "B52Label.h"
#import "PlaceData.h"
#import "WhiteButton.h"

@implementation HistoryMenuCell
{
    UIView *_mainView;
}
static CGFloat height = 24.0f;

+ (CGFloat) getHeightCount:(NSUInteger) count
{
    return [HistoryMenuCell getMainHeight: count];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect rect = self.frame;

        CGFloat heightMain = [HistoryMenuCell getMainHeight: 2];
        
        CGRect rectMain = CGRectMake(0.0f, 0.0f, rect.size.width, heightMain);
        _mainView = [[UIView alloc] initWithFrame:rectMain];
        _mainView.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.7f];
        [self addSubview:_mainView];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

/*
 */


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+ (CGFloat) getMainHeight: (NSUInteger)count {
    return height * count;
}

- (void)setHistoryData:(HistoryData *)historyData
{
    _historyData = historyData;
    NSArray *viewsToRemove = [_mainView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }

    
    NSArray *places = historyData.order.places;
    
    NSUInteger count = places.count;
    
    CGRect rectMain = _mainView.frame;
    rectMain.size.height = [HistoryMenuCell getMainHeight: count];
    _mainView.frame = rectMain;
    
    
    for (NSUInteger i = 0; i < count; i++) {
        PlaceData *placeData = places[i];
        
        CGRect rectPlace = CGRectMake(4.0f, i * height + 4.0f, 200.0f, 16.0f);
        UILabel *placeLabel = [[UILabel alloc] initWithFrame:rectPlace];
        placeLabel.font = [UIFont fontWithName:@"B52" size:17.0f];
        placeLabel.text = placeData.objectAddress;
        [_mainView addSubview:placeLabel];
    }
    
    // repeat
    
    CGFloat repeatWidth = 96.0f;
    CGFloat repeatHeight = 28.0f;
    CGRect repeatRect = CGRectMake(178.0f, 10.0f, repeatWidth, repeatHeight);
    
    UIButton *repeat = [[WhiteButton alloc] initWithFrame:repeatRect Radius:6];
    [repeat setTitle:@"повторить" forState:UIControlStateNormal];
    repeat.titleLabel.font = [UIFont fontWithName:@"B52" size:17.0f];
    [repeat setTitleColor:[UIColor colorWithWhite:0.15f alpha:1.0f] forState:UIControlStateNormal];
    [repeat addTarget:self action:@selector(repeatClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [_mainView addSubview:repeat];
}

-(void)repeatClick:(id)sender
{
    [_delegate onRepeat:_historyData];
}

@end
