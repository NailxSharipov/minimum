//
//  HistoryPlaceCell.h
//  Minimum
//
//  Created by Nail Sharipov on 07/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryPlaceCell : UITableViewCell

+ (CGFloat) HEIGHT;

@property (strong, nonatomic) UILabel* address;

@end
