//
//  HistoryPlaceCell.m
//  Minimum
//
//  Created by Nail Sharipov on 07/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "HistoryPlaceCell.h"
#import "B52Label.h"

@implementation HistoryPlaceCell

static CGFloat height = 44.0f;
static CGFloat margin = 4.0f;

+ (CGFloat) HEIGHT
{
    return height;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect rect = self.frame;
        CGFloat imgWidth = height - 2 * margin;
        CGRect titleRect = CGRectMake(margin, 0.0f, rect.size.width - margin, rect.size.height);
        
        _address = [[B52Label alloc] initWithFrame:titleRect];
        _address.font = [UIFont fontWithName:@"B52" size:19.0f];
        _address.backgroundColor = [UIColor clearColor];
        [self addSubview:_address];

        CGRect imgRect = CGRectMake(rect.size.width - imgWidth - margin, margin, imgWidth, imgWidth);
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:imgRect];
        imgView.image = [UIImage imageNamed:@"ico_plus"];
        
        [self addSubview:imgView];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
