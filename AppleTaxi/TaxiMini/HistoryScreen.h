//
//  HistoryScreen.h
//  Minimum
//
//  Created by Nail Sharipov on 09/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryCellEvent.h"

@interface HistoryScreen : UIViewController<HistoryCellEvent>

@end
