//
//  HistoryScreen.m
//  Minimum
//
//  Created by Nail Sharipov on 09/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "HistoryScreen.h"
#import "HistoryData.h"
#import "HistoryManager.h"
#import "PlaceManager.h"
#import "MainScreen.h"
#import "TaxiMiniGetAPI.h"
#import "TXHistoryTableViewSource.h"

@interface HistoryScreen ()

@property (weak, nonatomic) IBOutlet UITableView *historyTable;

@end

@implementation HistoryScreen
{
    TXHistoryTableViewSource *_historyDataSource;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _historyDataSource = [[TXHistoryTableViewSource alloc] init];
    _historyDataSource.tableView = self.historyTable;
    _historyDataSource.delegate = self;
}

- (IBAction)navigationPressed:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _historyDataSource.items = [HistoryManager getInstance].histories;
}

- (void)onDelete:(HistoryData*)historyData
{
    [[HistoryManager getInstance].histories removeObject:historyData];
    TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
    [getAPI deleteHistoryOrder: historyData.order.orderId];
    [_historyDataSource removeItem:historyData];
}
 
- (void)onRepeat:(HistoryData*)historyData
{
    PlaceManager *placeManager = [PlaceManager getInstance];
    [placeManager setWithHistory:historyData];
    
    [[MainScreen getInstance] closeMenu];
    
}
@end