//
//  HttpTextDataRequest.h
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpTextDataRequest : NSObject

typedef void (^ReadyHandler)(NSString *);
typedef void (^ReadyDataHandler)(NSData *);
typedef void (^ErrorHandler)(void);

-(void)doGetRequestURL: (NSURL*)url onReady: (ReadyHandler) onReadyHandler onError: (ErrorHandler) onErrorHandler;
-(void)doGetRequestURL: (NSURL*)url onDataReady: (ReadyDataHandler) onReadyDataHandler onError: (ErrorHandler) onErrorHandler;

@end