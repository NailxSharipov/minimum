//
//  HttpTextDataRequest.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "HttpTextDataRequest.h"

@implementation HttpTextDataRequest
{
    ReadyHandler onReady;
    ReadyDataHandler onReadyData;
    ErrorHandler onError;
    NSMutableData* receivedData;
}

-(void)doGetRequestURL: (NSURL*)url onDataReady: (ReadyDataHandler) onReadyDataHandler onError: (ErrorHandler) onErrorHandler
{
    onReadyData = onReadyDataHandler;
    onError = onErrorHandler;
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"ru-RU" forHTTPHeaderField:@"Accept-Language"];

    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection)
    {
        receivedData = [[NSMutableData alloc] init];
    }
    NSLog(@"request to %@", url);
}

-(void)doGetRequestURL: (NSURL*)url onReady: (ReadyHandler) onReadyHandler onError: (ErrorHandler) onErrorHandler
{
    onReady = onReadyHandler;
    onError = onErrorHandler;

    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:@"ru-RU" forHTTPHeaderField:@"Accept-Language"];
    
    NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection)
    {
        receivedData = [[NSMutableData alloc] init];
    }
    NSLog(@"request to %@", url);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString *errorString = [[NSString alloc] initWithFormat:@"Connection failed! Error - %@ %@ %@",
                             [error localizedDescription],
                             [error description],
                             [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]];
    NSLog(@"error: %@", errorString);
    onError();
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (onReady != nil) {
//    windows-1251
        NSString *text = [[NSString alloc] initWithData:receivedData encoding:NSWindowsCP1251StringEncoding];
        onReady(text);
    }
    if (onReadyData != nil) {
        onReadyData(receivedData);
    }
}

@end
