//
//  RuTaxiGetAPI.h
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderData.h"
#import "Brand.h"

@protocol IRuTaxiGetAPI <NSObject>

@required


typedef void (^OnReadyCitiesHandler)(NSArray*);
- (void) loadCitiesOnReady: (OnReadyCitiesHandler) onReady;


typedef void (^OnLoginHandler)(BOOL, NSString *);
- (void) loginPhoneNumber: (NSString *) phoneNumber Password: (NSString *)password OnReady:(OnLoginHandler) onReady;


typedef void (^OnRemindHandler)(NSString *);
- (void) remindPasswordForPhoneNumber: (NSString *)phoneNumber IsCall: (BOOL) isCall OnReady:(OnRemindHandler) onReady;

typedef void (^OnChangeHandler)(NSString *);
- (void) changePassword: (NSString *)password OnReady:(OnChangeHandler) onReady;

typedef void (^OnSearchPlaceHandler)(NSArray *);
- (void) searchPlace: (NSString *)key OnReady:(OnSearchPlaceHandler) onReady;
- (void) searchPlace: (NSString *)key ObjectType:(NSUInteger) objectType OnReady:(OnSearchPlaceHandler) onReady;

typedef void (^OnAddRatingHandler)(void);
- (void) addRating: (BOOL)like Commet:(NSString *)comment OnReady:(OnAddRatingHandler) onReady;

typedef void (^orderPriceHandler)(NSDictionary *);
- (void) makeOrder: (BOOL)order OnReady:(orderPriceHandler) onReady;

typedef void (^onHistoryLoaded)(NSArray *);
- (void) getHistoryOnReady:(onHistoryLoaded) onReady;

typedef void (^onCurrentOrderLoaded)(NSArray *);
- (void) getCurrentOrderOnReady:(onCurrentOrderLoaded) onReady;

typedef void (^onCancelOrder)(BOOL);
- (void) cancelOrder:(NSString *) orderId OnCancel:(onCancelOrder) onCancel;
/*
typedef void (^onBrandLoaded)(Brand *);
- (void) getBrandOnReady:(onBrandLoaded) onReady;
*/
- (void) deleteHistoryOrder:(NSString *) orderId;

@end
