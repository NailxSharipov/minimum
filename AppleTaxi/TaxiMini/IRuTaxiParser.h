//
//  IRuTaxiParser.h
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderData.h"
#import "Brand.h"

@protocol IRuTaxiParser <NSObject>

- (NSArray *)getCitiesFromText: (NSString *) text;

- (NSArray *)login: (NSString *) text;

- (NSString *)remindPassword: (NSString *) text;

- (NSString *)changePassword: (NSString *) text;

- (NSArray *)getPlacesFromText: (NSString *) text;

- (NSDictionary *) makeOrder: (NSString *) text;

- (NSArray *)getHistory: (NSString *) text;

- (NSArray *)getOrders: (NSString *) text;

//- (Brand *)getBrand: (NSString *) text;

@end
