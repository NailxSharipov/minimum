//
//  LocationData.h
//  TaxiMini
//
//  Created by Nail Sharipov on 10/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationData : NSObject

@property(strong, nonatomic) NSString* number;
@property(strong, nonatomic) NSString* streetType;
@property(strong, nonatomic) NSString* street;
@property(nonatomic) float lat;
@property(nonatomic) float lng;

- (NSString *) streetName;
+ (NSString *) relaceReservedWords: (NSString *) original;

@end
