//
//  LocationData.m
//  TaxiMini
//
//  Created by Nail Sharipov on 10/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "LocationData.h"

@implementation LocationData

-(NSString *) streetName
{
    return [LocationData relaceReservedWords:_street];
}

+ (NSString *) relaceReservedWords: (NSString *) original
{
    NSString *shortName = [NSString stringWithString:original];
    
    shortName = [shortName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    shortName = [shortName stringByReplacingOccurrencesOfString:@"улица" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"улица" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"площадь" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"проспект" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"набережная" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"бульвар" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"просек" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"переулок" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"шоссе" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"аллея" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"тупик" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"холм" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"проезд" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"район" withString:@""];
    shortName = [shortName stringByReplacingOccurrencesOfString:@"метро" withString:@""];
    
    shortName = [shortName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return shortName;
}





@end
