//
//  LoginScreen.h
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhoneNumberField.h"


@interface LoginScreen : UIViewController<UITableViewDelegate, UITableViewDataSource>

@end
