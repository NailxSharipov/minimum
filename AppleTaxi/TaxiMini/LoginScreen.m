//
//  LoginScreen.m
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "LoginScreen.h"
#import "TaxiMiniBackground.h"
#import "TaxiMiniGetAPI.h"
#import "UserData.h"
#import "AlertDialog.h"
#import "EditFieldCell.h"


@implementation LoginScreen {
    PhoneNumberField *_phoneNumber;
    TextFieldModern *_password;
}

- (void)viewDidLoad {
    _phoneNumber.autocorrectionType = UITextAutocorrectionTypeNo;
}

- (IBAction)LoginButtonPressed:(id)sender {
    NSString *phoneNumber = _phoneNumber.unformatText;
    NSString *password = _password.text;
    
    TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];

    [getAPI loginPhoneNumber:phoneNumber Password:password OnReady:^(BOOL isLogin, NSString *message)
    {
        if (isLogin) {
            [[UserData getInstance] setPhoneNumber:phoneNumber];
            [[UserData getInstance] setPassword:password];
            [self performSegueWithIdentifier: @"LoginToMain" sender: self];
        } else if (message != nil) {
            if (message.length < 256) {
                [AlertDialog messageWithText:message];
            } else {
                // здесь прикол что если с 3g и денег нет идет переадрисация на html пополнения счета
                [AlertDialog messageWithText:@"Сервер не отвечает"];
            }
        }
    }];
}

- (IBAction)smsPasswordPressed:(id)sender {
    [self remindPassword];
}

// Login Table
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = indexPath.row;
    NSString *identifier;
    if (index == 0) {
        identifier = [EditFieldCell PHONE_NUMBER];
    } else if (index == 1) {
        identifier = [EditFieldCell PASSWORD];
    }
    
    EditFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[EditFieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        if (index == 0) {
            _phoneNumber = (PhoneNumberField *)cell.editField;
            _phoneNumber.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            [_phoneNumber setUnformatText:[[UserData getInstance] phoneNumber]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        } else if (index == 1) {
            _password = (TextFieldModern *)cell.editField;
            _password.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            _password.secureTextEntry = YES;
            [_password setText: [[UserData getInstance] password]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = indexPath.row;
    if (index == 0) {
        [_phoneNumber becomeFirstResponder];
    } else if (index == 1) {
        [_password becomeFirstResponder];
    }
}

- (void)remindPassword {
    NSString *phone = _phoneNumber.unformatText;
    if (phone == nil || phone.length == 0) {
        [AlertDialog messageWithText:@"Заполните номер телефона"];
    } else {
        TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
        [getAPI remindPasswordForPhoneNumber:phone IsCall: NO OnReady:^(NSString *message)
        {
            if (message != nil) {
                if (message.length < 256) {
                    [AlertDialog messageWithText:message];
                } else {
                    // здесь прикол что если с 3g и денег нет идет переадрисация на html пополнения счета
                    [AlertDialog messageWithText:@"Сервер не отвечает"];
                }
            } else {
                [AlertDialog messageWithText:@"Введен некорректный номер телефона"];
            }
        }];
    }
}

@end
