//
//  MainScreen.h
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaceView.h"
#import "SWRevealViewController.h"
#import "TaxiMiniBackground.h"


@interface MainScreen : UIViewController<TaxiMiniBackgroundDelegate>

@property (weak, nonatomic) IBOutlet UIButton *titleCity;

@property (strong, nonatomic) IBOutlet TaxiMiniBackground *background;
@property (weak, nonatomic) IBOutlet PlaceView *placeView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIView *priceView;

+ (instancetype)getInstance;

-(void) updatePrice;
-(void) closeMenu;

@end
