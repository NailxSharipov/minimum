//
//  MainScreen.m
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "MainScreen.h"
#import "PlaceScreen.h"
#import "UserData.h"
#import "CityData.h"
#import "PlaceManager.h"
#import "TaxiMiniGetAPI.h"
#import "AlertDialog.h"
#import "HistoryManager.h"
#import "CurrentOrder.h"
#import "OrderManager.h"
#import "TaxiMiniBackground.h"

@implementation MainScreen {
    PlaceData *_selectedPlace;
    TaxiMiniGetAPI *_getAPI;
    BOOL isMenu;
}

static MainScreen* _instance;

+ (instancetype)getInstance {
    return _instance;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _instance = self;
    _getAPI = [[TaxiMiniGetAPI alloc] init];
    [_placeView setOnCellPressedEvent:^() {
        if (!isMenu) {
            [self performSegueWithIdentifier: @"MainToPlace" sender: self];
        }
    }];
    
    
    [_placeView setOnReloadEvent:^() {
        [self updatePrice];
    }];
    
    ((TaxiMiniBackground*)self.view).delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[HistoryManager getInstance] loadHistory];
    
    [[UserData getInstance] setFromAddress: ^() {
        [_placeView reloadData];
    }];
    
    isMenu = NO;
    CityData *cityData = [[UserData getInstance] city];
    if (cityData != nil && cityData.brandId != nil) {
        [self.titleCity setTitle: cityData.title forState: UIControlStateNormal];
    } else {
        [self.titleCity setTitle: @"Выберите город" forState: UIControlStateNormal];
    }
    [_placeView reloadData];
    
    [self updatePrice];
}

- (void)updatePrice {
    if([[PlaceManager getInstance] isValid]) {
        [_getAPI makeOrder:NO OnReady: ^(NSDictionary *result)
        {
        if (result != nil) {
                if ([[result objectForKey:@"code"] integerValue] == 1) {
                    NSString *price = [result objectForKey:@"price"];
                    _priceLabel.text = [NSString stringWithFormat:@"%@ рублей", price];
                }
            }
        }];
    } else {
        _priceLabel.text = @"";
    }
}

- (IBAction)citySelectPressed:(id)sender {
    [self performSegueWithIdentifier: @"MainToCitySelect" sender: self];
}

- (IBAction)makeOrder:(id)sender {
    [self createOrder];
}

- (void)createOrder {
    CityData *cityData = [[UserData getInstance] city];
    if (cityData != nil && cityData.brandId != nil) {
        if([[PlaceManager getInstance] getSize] > 1 && _priceLabel.text != nil && [_priceLabel.text length] > 0) {
            
            PlaceData *from = [PlaceManager getInstance].places[0];
            
            if (from.porch == nil || from.porch.length == 0) {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Внимание" message:@"Укажите куда подать машину" delegate:self cancelButtonTitle:@"Отменить" otherButtonTitles:@"Продолжить", nil];
                alert.alertViewStyle = UIAlertViewStylePlainTextInput;
                
                UITextField * alertTextField = [alert textFieldAtIndex:0];
                alertTextField.keyboardType = UIKeyboardTypeDefault;
                alertTextField.placeholder = @"Куда подать машину";
                
                [alert show];
            } else {
                [AlertDialog dialogWithText:@"Вы действительно хотите оформить заказ?" onClose: ^()
                {
                    [_getAPI makeOrder:YES OnReady: ^(NSDictionary *result)
                    {
                        if (result != nil) {
                            NSLog(@"result: %@", result);
                            NSString *orderId = result[@"orderId"];
                            if ([result[@"code"] integerValue] == 1 && orderId != nil) {
                                [OrderManager getInstance].currentOrderId = orderId;
                                [AlertDialog messageWithText:@"Ваш заказ принят"];
                                [self goToCurrentOrder];
                            }
                        }
                    }];
                }];
            }
        } else {
            [AlertDialog messageWithText:@"Не указан пункт отправления либо пункт назначения"];
        }
    }
}

- (IBAction)menuPressed:(id)sender {
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        BOOL isOpen = [revealViewController isOpen];
        [self onMenu: isOpen];
        [revealViewController revealToggle:self];
    }
}

- (void)closeMenu {
    [_placeView reloadData];
    [self updatePrice];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        BOOL isOpen = [revealViewController isOpen];
        [self onMenu: isOpen];
        if(!isOpen) {
            [revealViewController revealToggle:self];
        }
    }
}

- (void)openMenu {
    [_placeView reloadData];
    [self updatePrice];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        BOOL isOpen = [revealViewController isOpen];
        [self onMenu: isOpen];
        if(isOpen) {
            [revealViewController revealToggle:self];
        }
    }
}

- (void)onMenu:(BOOL)isOpen {
    isMenu = isOpen;
    [_placeView setHidden:isOpen];
    [_priceView setHidden:isOpen];
    if (isOpen) {
        [(TaxiMiniBackground*)self.view setImageHidden:YES];
    } else {
        [(TaxiMiniBackground*)self.view setImageHidden:NO];
    }
}

- (void)goToCurrentOrder {
    [_placeView reloadData];
    [self updatePrice];
    
    [self openMenu];
    
    UINavigationController *navigationController = (UINavigationController *)self.revealViewController.rearViewController;
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: navigationController.viewControllers];
    
    UIViewController *first = navigationArray[0];
    
    [navigationArray removeAllObjects];
   
    [navigationArray addObject:first];
    
    navigationController.viewControllers = navigationArray;

    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CurrentOrder"];
    
    [navigationController pushViewController:controller animated:NO];
 
}

- (void)swipeLeft {
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        BOOL isOpen = [revealViewController isOpen];
        [self onMenu: isOpen];
        [revealViewController revealToggle:self];
    }
}

- (void)tap {
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        BOOL isOpen = [revealViewController isOpen];
        [self onMenu: isOpen];
        [revealViewController revealToggle:self];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        UITextField * alertTextField = [alertView textFieldAtIndex:0];
        NSString* porch = alertTextField.text;
        if (porch != nil || porch.length > 0) {
            PlaceData *from = [PlaceManager getInstance].places[0];
            from.porch = porch;
        }
        [self createOrder];
    }
}

@end
