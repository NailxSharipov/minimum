//
//  MenuBar.m
//  TaxiMini
//
//  Created by Nail Sharipov on 14/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "MenuBar.h"

@implementation MenuBar

- (void)commonInit
{
    CGSize size = self.frame.size;
    
    CALayer *rightBorder = [CALayer layer];
    rightBorder.frame = CGRectMake(0.0f, 0.0f, 1.0f, size.height);
    rightBorder.backgroundColor = [UIColor colorWithWhite:0.4f alpha:0.4f].CGColor;
    [self.layer addSublayer:rightBorder];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

@end
