//
//  MenuScreen.h
//  TaxiMini
//
//  Created by Nail Sharipov on 14/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "HistoryCellEvent.h"

@interface MenuScreen : UIViewController<HistoryCellEvent>

@property (weak, nonatomic) IBOutlet UITableView *historyTable;

@end
