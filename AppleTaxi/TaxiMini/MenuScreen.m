//
//  MenuScreen.m
//  TaxiMini
//
//  Created by Nail Sharipov on 14/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "MenuScreen.h"
#import "HistoryData.h"
#import "HistoryMenuCell.h"
#import "HistoryManager.h"
#import "PlaceManager.h"
#import "MainScreen.h"
#import "TXHotHistoryTableViewSource.h"

@interface MenuScreen ()

@end

@implementation MenuScreen

{
    TXHotHistoryTableViewSource* _hotHistoryDataSource;
}

static NSString *repeatMenuCell = @"RepeatMenuCell";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self customSetup];
}

- (void)applicationFinishedRestoringState
{
    [self customSetup];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController ) {
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
    
    _hotHistoryDataSource = [[TXHotHistoryTableViewSource alloc] init];
    _hotHistoryDataSource.tableView = self.historyTable;
    _hotHistoryDataSource.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _hotHistoryDataSource.items = [HistoryManager getInstance].histories;

}

- (void)onRepeat:(HistoryData*)historyData
{
    PlaceManager *placeManager = [PlaceManager getInstance];
    [placeManager setWithHistory:historyData];
    
    [[MainScreen getInstance] closeMenu];
}

- (void)onDelete:(HistoryData*)historyData {

}

@end
