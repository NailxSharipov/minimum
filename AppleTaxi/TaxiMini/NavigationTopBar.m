//
//  NavigationTopBar.m
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "NavigationTopBar.h"
#import "TXThinLineView.h"

@implementation NavigationTopBar

- (void)commonInit
{
    TXThinLineView *line = [[TXThinLineView alloc] init];
    line.frame = CGRectMake(0.0f, self.frame.size.height - 1.0f, self.frame.size.width, 1.0f);
    line.lineColor = [[UIColor alloc] initWithWhite:0.4f alpha:0.4f];
    line.backgroundColor = [UIColor clearColor];
    line.align = TSLineAlignBottom;
    line.depth = 1.0f;
    [self addSubview:line];
    self.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.2f];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

@end
