//
//  OrderData.h
//  TaxiMini
//
//  Created by Nail Sharipov on 17/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderData : NSObject

@property (strong, nonatomic)NSString *orderId;
@property (strong, nonatomic)NSArray *places;

@property (strong, nonatomic)NSString *from;
@property (strong, nonatomic)NSString *to;
@property (strong, nonatomic)NSString *status;
@property (strong, nonatomic)NSString *price;
@property (strong, nonatomic)NSString *car;
@property (strong, nonatomic)NSString *color;
@property (strong, nonatomic)NSString *carNumber;
@property (strong, nonatomic)NSString *phone;
@property (nonatomic)BOOL isCanceled;
@property (nonatomic)int code;

- (NSString *)getAddress;


-(BOOL)isEqualTo: (OrderData *)second;

@end
