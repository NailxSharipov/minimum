//
//  OrderData.m
//  TaxiMini
//
//  Created by Nail Sharipov on 17/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "OrderData.h"
#import "PlaceData.h"

@implementation OrderData

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (NSString *)getAddress {
    NSMutableString *address = [[NSMutableString alloc] init];
    if (self.places != nil) {
        NSUInteger i = 1;
        NSUInteger n = self.places.count;
        for(PlaceData *place in self.places) {
            [address appendString:place.objectAddress];
            if (i < n) {
                [address appendString:@"\n"];
                i++;
            }
        }
    }
    return address;
}

-(BOOL)isEqualTo: (OrderData *)second {
    return _orderId != nil && second.orderId != nil && [_orderId isEqualToString:second.orderId];
}

@end
