//
//  OrderManager.h
//  TaxiMini
//
//  Created by Nail Sharipov on 17/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderData.h"

@interface OrderManager : NSObject

@property (strong, nonatomic, readonly) NSMutableArray* canceled;
@property (strong, nonatomic) NSString* currentOrderId;

+ (instancetype)getInstance;

- (void)cancelOrderId:(NSString *)orderId;

- (BOOL)isCanceledOrder:(NSString *)orderId;



@end
