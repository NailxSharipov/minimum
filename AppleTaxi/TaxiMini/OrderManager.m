//
//  OrderManager.m
//  TaxiMini
//
//  Created by Nail Sharipov on 17/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "OrderManager.h"


@implementation OrderManager

static OrderManager *_instance;

- (id) init
{
    self = [super init];
    if (self) {
        _canceled = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (instancetype)getInstance
{
    if (_instance == nil) {
        _instance = [[OrderManager alloc] init];
    }
    return _instance;
}

- (void)cancelOrderId:(NSString *)orderId
{
    [_canceled addObject:orderId];
}

- (BOOL)isCanceledOrder:(NSString *)orderId
{
    for (NSString* objId in _canceled) {
        if ([objId isEqualToString:orderId]) {
            return YES;
        }
    }
    return NO;
}



@end
