//
//  PhoneNumberField.h
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldModern.h"

@interface PhoneNumberField : TextFieldModern

@property (strong, nonatomic) NSString *helpText;
@property (strong, nonatomic) NSString *unformatText;

@end
