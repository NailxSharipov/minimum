//
//  PhoneNumberField.m
//  TaxiMini
//
//  Created by Nail Sharipov on 22/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "PhoneNumberField.h"

@implementation PhoneNumberField

- (void) commonInit
{
    [super commonInit];
     _unformatText = @"";
}

- (NSString *) getTextFormat: (NSString *)unformatText
{
    NSInteger length = [unformatText length];
    NSString *text;
    
    if (length == 0) {
        text = @"+7";
    } else if (length < 3) {
        text = [NSString stringWithFormat:@"+7 (%@", unformatText];
    } else if (length < 7) {
        NSString * abc = [unformatText substringWithRange: NSMakeRange (0, 3)];
        NSString * end = [unformatText substringWithRange: NSMakeRange (3, length - 3)];
        
        text = [NSString stringWithFormat:@"+7 (%@) %@", abc, end];
    } else if (length < 9) {
        NSString * abc = [unformatText substringWithRange: NSMakeRange (0, 3)];
        NSString * def = [unformatText substringWithRange: NSMakeRange (3, 3)];
        NSString * end = [unformatText substringWithRange: NSMakeRange (6, length - 6)];
        
        text = [NSString stringWithFormat:@"+7 (%@) %@-%@", abc, def, end];
    } else {
        NSString * abc = [unformatText substringWithRange: NSMakeRange (0, 3)];
        NSString * def = [unformatText substringWithRange: NSMakeRange (3, 3)];
        NSString * gh = [unformatText substringWithRange: NSMakeRange (6, 2)];
        NSString * end = [unformatText substringWithRange: NSMakeRange (8, length - 8)];
        
        text = [NSString stringWithFormat:@"+7 (%@) %@-%@-%@", abc, def, gh, end];
    }
    
    return text;
}

- (void) setUnformatText:(NSString *)unformatText
{
    _unformatText = unformatText;
    self.text = [self getTextFormat:_unformatText];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger size = [string length];
    NSUInteger length = [_unformatText length];

    if ((size==0 && length > 0) || (size == 1 && ([string intValue] || [string isEqualToString:@"0"]) && length < 10)) {
        
        // backspace
        if (size == 0) {
            _unformatText = [_unformatText substringWithRange: NSMakeRange (0, length - 1)];
        } else {
            _unformatText = [NSString stringWithFormat:@"%@%@", _unformatText, string];
        }
        
        textField.text = [self getTextFormat:_unformatText];
    }

    return NO;
}


@end
