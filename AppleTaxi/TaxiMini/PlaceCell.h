//
//  PlaceCell.h
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PlaceData;
@interface PlaceCell : UIView

@property (strong, nonatomic, readonly) UIButton* main;
@property (strong, nonatomic, readonly) UIButton* cross;
@property (strong, nonatomic) PlaceData* order;
@property (nonatomic)BOOL isLast;
@property (nonatomic)BOOL isFirst;

+ (CGFloat) HEIGHT;

@end
