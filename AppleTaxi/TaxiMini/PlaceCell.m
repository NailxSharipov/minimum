//
//  OrderCell.m
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "PlaceCell.h"
#import "TXThinLineView.h"

@implementation PlaceCell
{
    CALayer *_bottomBorder;
    CALayer *_topBorder;
}

static CGFloat leftMargin = 32.0f;
static CGFloat height = 44.0f;

+ (CGFloat) HEIGHT
{
    return height;
}

- (void)commonInit {
    TXThinLineView *line = [[TXThinLineView alloc] init];
    line.frame = CGRectMake(leftMargin, self.frame.size.height - 1.0f, self.frame.size.width, 1.0f);
    line.lineColor = [[UIColor alloc] initWithWhite:0.4f alpha:0.4f];
    line.backgroundColor = [UIColor clearColor];
    line.align = TSLineAlignBottom;
    line.depth = 1.0f;
    [self addSubview:line];
    
    self.backgroundColor = [UIColor colorWithWhite:1.0f alpha:1.0f];
    
    CGFloat x = leftMargin;
    CGFloat mainWidth = self.frame.size.width - leftMargin - height;
    CGRect mainRect = CGRectMake(x, 0.0f, mainWidth, height);
    _main = [[UIButton alloc] initWithFrame:mainRect];
    _main.titleLabel.font = [UIFont fontWithName:@"B52" size:18];
    _main.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_main setTitleColor:[UIColor darkTextColor] forState: UIControlStateNormal];
    [self addSubview:_main];
    
    x = x + mainWidth;
    CGRect crossRect = CGRectMake(x, 0.0f, height, height);
    _cross = [[UIButton alloc] initWithFrame:crossRect];
    [_cross setBackgroundImage:[UIImage imageNamed:@"ico_cross"] forState:UIControlStateNormal];
    [self addSubview:_cross];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)setIsLast:(BOOL)isLast
{
    _isLast = isLast;
    _bottomBorder.hidden = !isLast;
}

- (void)setIsFirst:(BOOL)isFirst
{
    _isFirst = isFirst;
    _topBorder.hidden = isFirst;
}

@end
