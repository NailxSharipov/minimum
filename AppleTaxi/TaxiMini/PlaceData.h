//
//  Order.h
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchPlaceData.h"
#import "LocationData.h"

@interface PlaceData : NSObject

@property (readonly) NSUInteger unid;

@property (weak, nonatomic, readonly) NSString* text;
@property (weak, nonatomic, readonly) NSString* objectAddress;
@property (strong, nonatomic) NSString* action;
@property (nonatomic) BOOL isFrom;

@property (strong, nonatomic) NSString *objectId; // in our database

@property (strong, nonatomic) NSString *type; // 1-улицы, 2- остановки, 3-прочие объекты
@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *home;
@property (strong, nonatomic) NSString *porch;

@property (strong, nonatomic) LocationData *locationData;

- (id)initWithUnid: (NSUInteger)unid;

- (void)clear;

- (void)setWithSearchPlaceData: (SearchPlaceData *)searchPlaceData;

- (void)setWithPlaceData: (PlaceData *)placeData;

- (void)copyWith: (PlaceData *)placeData;

- (BOOL)isEmpty;

- (BOOL)isCorect;

- (BOOL)isEqual: (PlaceData *) placeData;

@end
