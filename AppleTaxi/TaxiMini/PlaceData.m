//
//  Order.m
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "PlaceData.h"
#import "LocationData.h"

@implementation PlaceData

- (id)initWithUnid: (NSUInteger)unid
{
    self = [super init];
    if (self) {
        _unid = unid;
    }
    return self;
}

- (void)clear
{
    _objectId = nil;
    _street = nil;
    _home = nil;
    _porch = nil;
    _locationData = nil;
}

-(void)setWithSearchPlaceData: (SearchPlaceData *)searchPlaceData
{
    if ([_type isEqualToString:@"1"]) {
        _street = [LocationData relaceReservedWords: searchPlaceData.name];
    } else {
        _street = searchPlaceData.name;
    }
    
    _type = searchPlaceData.type;
    _objectId = searchPlaceData.dataId;
    _home = nil;
}

- (void)setWithPlaceData: (PlaceData *)placeData
{
    _street = placeData.street;
    _type = placeData.type;
    _objectId = placeData.objectId;
    _home = placeData.home;
}

-(void)setLocationData:(LocationData *)locationData
{
    _locationData = locationData;
    _street = locationData.streetName;
    _home = locationData.number;
}

-(NSString*)text
{
    if (_street != nil) {
        if (_home == nil) {
            _home = @"";
        }
        return [NSString stringWithFormat:@"%@ %@",_street, _home];
    } else {
        return _action;
    }
}

-(NSString*)objectAddress
{
    if (_street != nil) {
        if (_home == nil) {
            _home = @"";
        }
        return [NSString stringWithFormat:@"%@ %@",_street, _home];
    } else {
        return nil;
    }
}

- (void)copyWith: (PlaceData *)placeData
{
    _objectId = placeData.objectId;
    
    _type = placeData.type;
    _street = placeData.street;
    _home = placeData.home;
    _porch = placeData.porch;
}

- (BOOL)isEmpty {
    return (_objectId == nil && _street == nil);
}

- (BOOL)isCorect {
    BOOL corect = _objectId != nil && _street != nil && _type != nil;
    if (corect && [_type isEqualToString:@"1"]) {
        corect = _home != nil && _home.length > 0;
    }
    return corect;
}

- (BOOL)isEqual: (PlaceData *) placeData {
    if (_objectId != nil && placeData.objectId != nil) {
        if (![_objectId isEqualToString:placeData.objectId]) {
            return NO;
        }
    }
    
    if (_type != nil && placeData.type != nil) {
        if (![_type isEqualToString:placeData.type]) {
            return NO;
        }
    }
    
    if (_street != nil && placeData.street != nil) {
        if (![_street isEqualToString:placeData.street]) {
            return NO;
        }
    }
    
    if (_home != nil && placeData.home != nil) {
        if (![_home isEqualToString:placeData.home]) {
            return NO;
        }
    }
    
    return YES;
}

@end
