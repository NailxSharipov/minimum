//
//  PlaceDataTime.h
//  Minimum
//
//  Created by Nail Sharipov on 01/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlaceData.h"

@interface PlaceDataTime : NSObject

@property(strong, nonatomic) PlaceData *place;
@property(strong, nonatomic) NSDate *time;
@property(strong, nonatomic) NSString *hashData;

@end
