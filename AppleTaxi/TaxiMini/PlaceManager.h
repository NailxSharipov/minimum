//
//  PlaceManager.h
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlaceData.h"
#import "HistoryData.h"

@interface PlaceManager : NSObject

@property (strong, nonatomic, readonly) NSMutableArray* places;
@property (strong, nonatomic) PlaceData* selectedPlace;

+ (instancetype)getInstance;

- (PlaceData *) addPlace;

- (void) removePlace:(PlaceData *)place;

- (NSUInteger) getSize;

- (void) clearAll;

- (void) setWithHistory:(HistoryData *) historyData;

- (void) switchPlaces:(NSUInteger) firstPlace;

- (NSArray *) getNotEmptyPlaces;

- (BOOL)isValid;


@end
