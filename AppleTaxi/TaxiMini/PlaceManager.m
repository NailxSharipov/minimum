//
//  PlaceManager.m
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "PlaceManager.h"

@implementation PlaceManager
{
    NSUInteger _unidCounter;
}

static PlaceManager *_instance;

- (id) init
{
    self = [super init];
    if (self) {
        _unidCounter = 0;
        _places = [[NSMutableArray alloc] init];
        [self addPlace];
        [self addPlace];
    }
    return self;
}

+ (instancetype)getInstance
{
    if (_instance == nil) {
        _instance = [[PlaceManager alloc] init];
    }
    
    return _instance;
}

- (PlaceData *) addPlace
{
    PlaceData *place = [[PlaceData alloc] initWithUnid:_unidCounter];
    [_places addObject:place];
    if (_places.count == 1) {
        place.action = @"Откуда";
        place.isFrom = YES;
    } else {
        place.action = @"Куда";
        place.isFrom = NO;
    }
    _unidCounter++;
    return place;
}

- (void) removePlace:(PlaceData *)place
{
    NSInteger index = [self getOrderIndex:place];
    if (index >= 2) {
        [_places removeObjectAtIndex:index];
    } else if (index == 1) {
        if (_places.count > 2) {
            [_places removeObjectAtIndex:index];
        } else {
            [place clear];
        }
    } else {
        [place clear];
        /*
        if (_places[1] != nil && ![_places[1] isEmpty]) {
            [_places[0] copyWith:_places[1]];
            if (_places.count > 2) {
                [_places removeObjectAtIndex:1];
            } else {
                [_places[1] clear];
            }
        }
         */
    }
}

- (NSInteger) getOrderIndex: (PlaceData *)order
{
    NSUInteger unid = order.unid;
    NSUInteger n = [_places count];
    for (NSUInteger i = 0; i < n; i++) {
        PlaceData *placeData = [_places objectAtIndex:i];
        if (placeData.unid == unid) {
            return i;
        }
    }
    return -1;
}

- (NSUInteger) getSize
{
    NSUInteger count = 0;
    for (PlaceData *place in _places) {
        if (place != nil && place.objectId != nil && place.objectId.length > 0) {
            count++;
        }
    }
    return count;
}

- (void) clearAll
{
    NSInteger n = _places.count;
    for (NSInteger i = n - 1; i >= 0; i--) {
        if (i >= 2) {
            [_places removeObjectAtIndex:i];
        } else {
            [_places[i] clear];
        }
    }

}

- (void) setWithHistory:(HistoryData *) historyData
{
    [self clearAll];
    NSUInteger i = 0;
    for (PlaceData *place in historyData.order.places) {
        if (i < 2) {
            [_places[i] setWithPlaceData:place];
        } else {
            [[self addPlace] setWithPlaceData:place];
        }
        i++;
    }

}

- (void) switchPlaces:(NSUInteger) firstPlace
{
    if (![_places[firstPlace] isEmpty] && ![_places[firstPlace + 1] isEmpty]) {
        PlaceData *place = [[PlaceData alloc] initWithUnid:0];
        [place copyWith: _places[firstPlace]];
        [_places[firstPlace] copyWith: _places[firstPlace + 1]];
        [_places[firstPlace + 1] copyWith: place];
    }
}

- (NSArray *) getNotEmptyPlaces {
    NSMutableArray *notEmptyPlaces = nil;
    if ([self isValid]) {
        notEmptyPlaces = [[NSMutableArray alloc] initWithCapacity:_places.count];
        for (PlaceData *place in _places) {
            [notEmptyPlaces addObject:place];
        }
    }
    
    return notEmptyPlaces;
}

- (BOOL)isValid {
    int count = 0;
    for (PlaceData* place in _places) {
        if ([place isCorect]) {
            count++;
        }
    }
    return count >= 2;
//    return ![_places[0] isEmpty] && [self getSize] >= 2;
}

@end
