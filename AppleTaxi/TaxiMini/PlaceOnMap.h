//
//  PlaceOnMap.h
//  TaxiMini
//
//  Created by Nail Sharipov on 10/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface PlaceOnMap : UIViewController<GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *location;
@property (weak, nonatomic) IBOutlet UIView *map;
@property (weak, nonatomic) IBOutlet UIImageView *markerView;

@end
