//
//  PlaceOnMap.m
//  TaxiMini
//
//  Created by Nail Sharipov on 10/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "PlaceOnMap.h"
#import "PlaceScreen.h"
#import "UserData.h"
#import "PlaceManager.h"
#import "GeoLocationOSMNominatim.h"
#import "TaxiMiniGetAPI.h"
#import "AlertDialog.h"

@interface PlaceOnMap ()

@end

@implementation PlaceOnMap
{
    GeoLocationOSMNominatim * _geo;
    GMSMapView *_mapView;
    TaxiMiniGetAPI *_getAPI;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _geo = [[GeoLocationOSMNominatim alloc] init];
    
    _getAPI = [[TaxiMiniGetAPI alloc] init];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (_mapView == nil) {
        UserData *user = [UserData getInstance];
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:user.latitude
                                                                longitude:user.longitude
                                                                     zoom:17];
        
        CGRect rect = _map.frame;
        rect.origin.x = 0.0f;
        rect.origin.y = 0.0f;
        
        _mapView = [GMSMapView mapWithFrame:rect camera:camera];
        _mapView.myLocationEnabled = YES;
        _mapView.delegate = self;
        
        [_map addSubview:_mapView];
        
        _markerView.layer.zPosition = 1;
    }
}


- (void) mapView: (GMSMapView *) mapView idleAtCameraPosition:(GMSCameraPosition *) position
{
    CLLocationCoordinate2D point = mapView.camera.target;
    [_geo searchByLocationLat: point.latitude Lon:point.longitude OnReadyLocation:^(LocationData *data)
     {
         if (data != nil) {
             [_getAPI searchPlace: [data streetName] ObjectType:1 OnReady:^(NSArray* streets){
                 if (streets != nil && streets.count > 0) {
                     for(SearchPlaceData *streetData in streets) {
                         if (streetData.isStreet) {
                             PlaceData *placeData = [PlaceManager getInstance].selectedPlace;
                             placeData.locationData = data;
                             placeData.objectId = streetData.dataId;
                             placeData.type = @"1";
                             NSString *address = [NSString stringWithFormat:@"%@ %@", data.street, data.number];
                             _location.text = address;
                             break;
                         }
                     }
                 } else {
                     _location.text = @"Адрес не найден";
                 }
             }];
         } else {
             _location.text = @"Адрес не найден";
             PlaceData *placeData = [PlaceManager getInstance].selectedPlace;
             [placeData clear];
         }
     }];
}

- (IBAction)navigationPressed:(id)sender {
    PlaceData *placeData = [PlaceManager getInstance].selectedPlace;
    
    if (placeData.isFrom) {
        [self.navigationController popViewControllerAnimated:YES];
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
        PlaceScreen* placeScreen = (PlaceScreen*)[navigationArray lastObject];
        
        [placeScreen.porchField becomeFirstResponder];
        
        
    } else {
        UINavigationController *navigationController = self.navigationController;
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: navigationController.viewControllers];
        [navigationArray removeLastObject];
        navigationController.viewControllers = navigationArray;
        [navigationController popViewControllerAnimated:YES];
    }
    
}



@end
