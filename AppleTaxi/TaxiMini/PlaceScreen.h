//
//  PlaceScreen.h
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaceData.h"
#import "TXSearchCellEvent.h"
#import "TXFastCellEvent.h"

@interface PlaceScreen : UIViewController<UISearchBarDelegate, UITextViewDelegate, TXSearchCellEvent, TXFastCellEvent>

@property (weak, nonatomic) IBOutlet UIButton *navigationTitle;

@property (weak, nonatomic) IBOutlet UISearchBar *searchField;

@property (weak, nonatomic) IBOutlet UILabel *statusField;

@property (weak, nonatomic) IBOutlet UITableView *placesTable;
@property (weak, nonatomic) IBOutlet UITableView *historyTable;
@property (weak, nonatomic) IBOutlet UIView *historyView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *houseLabel;
@property (weak, nonatomic) IBOutlet UITextField *houseField;
@property (weak, nonatomic) IBOutlet UIView *houseView;
@property (weak, nonatomic) IBOutlet UILabel *porchLabel;
@property (weak, nonatomic) IBOutlet UITextView *porchField;

@property (weak, nonatomic) IBOutlet UIView *porchView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *porchTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBottom;

@end
