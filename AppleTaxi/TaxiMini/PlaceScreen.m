//
//  PlaceScreen.m
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "PlaceScreen.h"
#import "PlaceManager.h"
#import "TaxiMiniGetAPI.h"
#import "HistoryManager.h"
#import "HistoryPlaceCell.h"
#import "TXSearchTableViewSource.h"
#import "TXFastTableViewSource.h"

@implementation PlaceScreen {
    TaxiMiniGetAPI *_getAPI;
    TXSearchTableViewSource *_placeDataSource;
    TXFastTableViewSource *_fastPlaceDataSource;
}

static NSString *EMPTY = @"Введите не менее 3-х букв";
static NSString *NO_RESULT = @"Совпадений не найденно";

static NSUInteger SEARCH_BASE = 0;
static NSUInteger SEARCH_TABLE = 1;
static NSUInteger SEARCH_READY = 2;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _placeDataSource = [[TXSearchTableViewSource alloc] init];
    _placeDataSource.tableView = _placesTable;
    _placeDataSource.delegate = self;

    _fastPlaceDataSource = [[TXFastTableViewSource alloc] init];
    _fastPlaceDataSource.tableView = _historyTable;
    _fastPlaceDataSource.delegate = self;
    [_fastPlaceDataSource setItems:[HistoryManager getInstance].places];

    _getAPI = [[TaxiMiniGetAPI alloc] init];
    
    _searchField.delegate = self;

    // search bar style
    {
        [_searchField setBackgroundImage:[UIImage new]];
        [_searchField setTranslucent:YES];

        UIImageView *searchIco = [UIImageView appearanceWhenContainedIn:[UISearchBar class], nil];
        if (searchIco != nil) {
            searchIco.image = nil;
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:@"UIKeyboardDidShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:@"UIKeyboardWillHideNotification"
                                               object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    PlaceData *place = [PlaceManager getInstance].selectedPlace;
    [self.navigationTitle setTitle:place.action forState:UIControlStateNormal];
    
    PlaceData *selectedPlace = [PlaceManager getInstance].selectedPlace;
    if (selectedPlace != nil && selectedPlace.objectId != nil) {
        [self setState:SEARCH_READY];
    } else {
        [self setState:SEARCH_BASE];
    }

    _statusField.text = EMPTY;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    [self setState:SEARCH_TABLE];
    [self search: _searchField.text];
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self search: searchText];
}

- (void)search: (NSString *)text {
    if (text.length >= 3) {
        [_getAPI searchPlace:text OnReady:^(NSArray *places)
         {
             if (places != nil && [places count] > 0) {
                 [_placeDataSource setItems:places];
                 _placesTable.hidden = NO;
                 [_placesTable reloadData];
             } else {
                 _placesTable.hidden = YES;
                 _statusField.text = NO_RESULT;
             }
         }];
    } else {
        _placesTable.hidden = YES;
        _statusField.text = EMPTY;
    }
}

- (IBAction)navigationPressed:(id)sender {
        PlaceData* placeData = [PlaceManager getInstance].selectedPlace;
        if ([SearchPlaceData isStreet: placeData.type] && _houseView.hidden == NO) {
            placeData.home = _houseField.text;
        }
        if (_porchLabel.text != nil && _porchView.hidden == NO) {
            placeData.porch = _porchField.text;
        }
    
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)keyboardDidShow:(NSNotification *)note {
    // resize tableView
    NSDictionary *userInfo = [note userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _tableViewBottom.constant = kbSize.height;
    _scrollBottom.constant = kbSize.height;
    
    if (_porchField.isFirstResponder) {
        float height = _scrollView.frame.size.height;
        [UIView animateWithDuration:0.0f animations:^
         {
             _scrollView.contentOffset = CGPointMake(0.0f, height);
         }];
    }
}

- (void)keyboardWillHide:(NSNotification *)note {
    [UIView setAnimationsEnabled:YES];
    // fullscreen
    _tableViewBottom.constant = 0.0f;
    _scrollBottom.constant = 0.0f;
}


- (void)setState:(NSUInteger)state {
    // clear all

    _placesTable.hidden = YES;
    _statusField.hidden = YES;

    _houseLabel.hidden = YES;
    _houseView.hidden = YES;
    
    _porchLabel.hidden = YES;
    _porchView.hidden = YES;
    
    _historyView.hidden = YES;
    
    _porchField.text = nil;
    _houseField.text = nil;
    _searchBottom.constant = 0.0f;

    if (state == SEARCH_BASE) {
        _searchField.text = @"";
        _historyView.hidden = NO;
    } else if (state == SEARCH_TABLE) {
        _statusField.hidden = NO;
    } else if (state == SEARCH_READY) {
        PlaceData* placeData = [PlaceManager getInstance].selectedPlace;
        _searchField.text = placeData.street;
        
        if ([SearchPlaceData isStreet: placeData.type]) {
            _houseLabel.hidden = NO;
            _houseView.hidden = NO;
            _porchTop.constant = 98.0f;
        } else {
            _houseLabel.hidden = YES;
            _houseView.hidden = YES;
            _porchTop.constant = 12.0f;
        }
        
        if (placeData.isFrom) {
            _porchLabel.hidden = NO;
            _porchView.hidden = NO;
            _porchField.text = placeData.porch;
        }
        
        if (placeData.isFrom && [SearchPlaceData isStreet: placeData.type]) {
            _searchBottom.constant = 196.0f;
        } else {
            _searchBottom.constant = 86.0f;
        }
        
        
        _houseField.text = placeData.home;
    }
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    return NO;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return YES;
}

- (IBAction)changeStreetPressed:(id)sender {
    [_searchField becomeFirstResponder];
}

- (void)onSelect:(SearchPlaceData*)place {
    PlaceData* selectedPlace = [PlaceManager getInstance].selectedPlace;
    [selectedPlace setWithSearchPlaceData:place];
    
    [_searchField resignFirstResponder];
    
    if (selectedPlace != nil && selectedPlace.objectId != nil) {
        [self setState:SEARCH_READY];
    }
}

- (void)onFastSelect:(PlaceData *)place {
    PlaceData* selectedPlace = [PlaceManager getInstance].selectedPlace;
    
    [selectedPlace setWithPlaceData:place];
    
    if ([selectedPlace isFrom]) {
        [self setState:SEARCH_READY];
        [_porchField becomeFirstResponder];
    } else {
        [[self navigationController] popViewControllerAnimated:YES];
    }
}

@end
