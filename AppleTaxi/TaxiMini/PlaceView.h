//
//  OrderView.h
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PlaceData;

@interface PlaceView : UIScrollView

typedef void (^OnCellPressed)(void);
typedef void (^OnReload)(void);

- (void)setOnCellPressedEvent: (OnCellPressed) onCellPressed;
- (void)setOnReloadEvent: (OnReload) onReload;
- (void)reloadData;

@end
