//
//  OrderView.m
//  TaxiMini
//
//  Created by Nail Sharipov on 03/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "PlaceView.h"
#import "PlaceCell.h"
#import "AddPlaceCell.h"
#import "PlaceManager.h"
#import "UserData.h"
#import "CityData.h"

@implementation PlaceView
{
    PlaceManager *_manager;
    NSMutableArray *_cells;
    AddPlaceCell *_addCell;
    OnCellPressed _onCellPressed;
    OnReload _onReload;
    NSMutableArray *_arrowButtons;
}
- (void)commonInit
{
    _manager = [PlaceManager getInstance];
    _cells = [[NSMutableArray alloc] init];
    
    NSUInteger n = _manager.places.count;
    CGSize size = self.frame.size;
    CGFloat height = [PlaceCell HEIGHT];
    CGRect rect = CGRectMake(0.0f, n * height, size.width, height);
    _addCell = [[AddPlaceCell alloc] initWithFrame:rect];
    [_addCell.main addTarget:self action:@selector(plusButtonPressed:) forControlEvents:UIControlEventTouchDown];
    
    [self addSubview:_addCell];

    [self reloadData];
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)reloadData
{
    NSUInteger cellCount = _cells.count;
    NSUInteger dataCount = _manager.places.count;
    
    CGSize size = self.frame.size;
    CGFloat height = [PlaceCell HEIGHT];
    
    // add new cells
    {
        for(NSUInteger i = cellCount; i < dataCount; i++) {
            CGRect rect = CGRectMake(0.0f, i * height, size.width, height);
            PlaceCell *cell = [[PlaceCell alloc] initWithFrame:rect];
            [cell.cross addTarget:self action:@selector(removeButtonPressed:) forControlEvents:UIControlEventTouchDown];
            [cell.main addTarget:self action:@selector(mainButtonPressed:) forControlEvents:UIControlEventTouchDown];
            [_cells addObject:cell];
            [self addSubview:cell];
        }
    }
    
    // move plus cell
    {
        _addCell.frame = CGRectMake(0.0f, dataCount * height, size.width, height);
    }
    
    // hide unused cells
    for(NSUInteger i = dataCount; i < cellCount; i++) {
        PlaceCell *cell = [_cells objectAtIndex:i];
        cell.hidden = YES;
    }
    
    // set data
    for(NSUInteger i = 0; i < dataCount; i++) {
        PlaceCell *cell = [_cells objectAtIndex:i];
        PlaceData *data = [_manager.places objectAtIndex:i];
        [cell.main setTitle:data.text forState:UIControlStateNormal];
        cell.order = data;
        cell.hidden = NO;
        cell.isFirst = (i==0);
        cell.isLast = NO;
    }
    
    [self setContentSize:CGSizeMake(size.width, (dataCount + 2) * height)];
   

    // add arrows button
    {
        if (_arrowButtons != nil) {
            for(UIButton *button in _arrowButtons) {
                [button removeFromSuperview];
            }
        }
        
        NSUInteger size = dataCount - 1;
        _arrowButtons = [[NSMutableArray alloc] initWithCapacity:size];
        
        CGFloat arrowHeight = 30.0f;
        CGFloat arrowWidth = 30.0f;
        
        UIImage *image = [UIImage imageNamed:@"ico_arrows"];
        
        for(NSUInteger i = 0; i < size; i++) {
            CGRect rect = CGRectMake(0.0f, (i + 1) * height - 0.5f * arrowHeight, arrowWidth, arrowHeight);
            UIButton *button = [[UIButton alloc] initWithFrame:rect];
            [button setBackgroundImage:image forState:UIControlStateNormal];
            [button addTarget:self action:@selector(arrowButtonPressed:) forControlEvents:UIControlEventTouchDown];
            button.tag = i;
            _arrowButtons[i] = button;
            [self addSubview:button];
        }
    }
    
    
    // update

    if (_onReload != nil) {
        _onReload();
    }
}

-(void)arrowButtonPressed:(UIButton*)sender
{
    [[PlaceManager getInstance] switchPlaces: sender.tag];
    [self reloadData];
}

-(void)removeButtonPressed:(UIButton*)sender
{
    PlaceCell *cell = (PlaceCell*)[sender superview];
    [_manager removePlace:cell.order];
    [self reloadData];
}

-(void)plusButtonPressed:(UIButton*)sender
{
    [_manager addPlace];
    [self reloadData];
}

-(void)mainButtonPressed:(UIButton*)sender
{
    CityData *cityData = [[UserData getInstance] city];
    if (cityData != nil && cityData.brandId != nil) {
    if (_onCellPressed != nil) {
        PlaceCell *cell = (PlaceCell*)[sender superview];
        _manager.selectedPlace = cell.order;
        _onCellPressed();
    }
    }
}

- (void) setOnCellPressedEvent: (OnCellPressed) onCellPressed
{
    _onCellPressed = onCellPressed;
}


- (void)setOnReloadEvent: (OnReload) onReload
{
    _onReload = onReload;
}

@end
