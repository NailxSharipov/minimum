//
//  PreloadScreen.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "PreloadScreen.h"
#import "TaxiMiniGetAPI.h"
#import "ServerData.h"
//#import "UserData.h"
//#import "HistoryManager.h"

@interface PreloadScreen ()

@end

@implementation PreloadScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
    TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];

    [getAPI loadCitiesOnReady:^(NSArray *cities)
    {
        [ServerData getInstance].cities = cities;
    }
    ];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self performSegueWithIdentifier: @"PreloadToLogin" sender: self];
}

@end
