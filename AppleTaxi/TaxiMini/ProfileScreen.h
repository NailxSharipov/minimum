//
//  ProfileScreen.h
//  TaxiMini
//
//  Created by Nail Sharipov on 14/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhoneNumberField.h"

@interface ProfileScreen : UIViewController

@property (weak, nonatomic) IBOutlet PhoneNumberField *phoneNumberField;

@property (weak, nonatomic) IBOutlet UITextField *passwordField;


@end
