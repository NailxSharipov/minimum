//
//  ProfileScreen.m
//  TaxiMini
//
//  Created by Nail Sharipov on 14/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "ProfileScreen.h"
#import "UserData.h"

@interface ProfileScreen ()

@end

@implementation ProfileScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UserData *user = [UserData getInstance];
    
    _phoneNumberField.unformatText = user.phoneNumber;
    _passwordField.text = user.password;
}

- (IBAction)navigationPressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

@end
