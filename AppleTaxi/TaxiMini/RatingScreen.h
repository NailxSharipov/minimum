//
//  RatingScreen.h
//  TaxiMini
//
//  Created by Nail Sharipov on 16/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingScreen : UIViewController<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *unlikeButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textBottomSpace;
@property (weak, nonatomic) IBOutlet UITextView *comment;

@end
