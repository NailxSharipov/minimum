//
//  RatingScreen.m
//  TaxiMini
//
//  Created by Nail Sharipov on 16/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "RatingScreen.h"
#import "TaxiMiniGetAPI.h"
#import "AlertDialog.h"
#import "UserData.h"

@interface RatingScreen ()

@end

@implementation RatingScreen
{
    BOOL flagLike;
    BOOL flagUnlike;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    flagLike = NO;
    flagUnlike = NO;
    _comment.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:@"UIKeyboardWillShowNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:@"UIKeyboardDidHideNotification"
                                               object:nil];
    
    [self like: YES];
}

- (IBAction)navigationPressed:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}


- (IBAction)likePressed:(id)sender
{
    if (flagLike == NO) {
        [self like:YES];
        [self send];
    }
}

- (IBAction)unlikePressed:(id)sender
{
    if (flagLike == YES) {
        [self like:NO];
        [self send];
    }
}

- (void) like: (BOOL)liked
{
    if (liked) {
        [_likeButton setBackgroundImage:[UIImage imageNamed:@"ico_like_highlighted"] forState:UIControlStateNormal];
        [_unlikeButton setBackgroundImage:[UIImage imageNamed:@"ico_unlike"] forState:UIControlStateNormal];
    } else {
        [_likeButton setBackgroundImage:[UIImage imageNamed:@"ico_like"] forState:UIControlStateNormal];
        [_unlikeButton setBackgroundImage:[UIImage imageNamed:@"ico_unlike_highlighted"] forState:UIControlStateNormal];
    }
    flagLike = liked;
    flagUnlike = !liked;
}


- (void) keyboardWillShow:(NSNotification *)note
{
    // resize tableView
    NSDictionary *userInfo = [note userInfo];
    CGSize kbSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _textBottomSpace.constant = kbSize.height;
}

- (void) keyboardDidHide:(NSNotification *)note
{
    // fullscreen
    _textBottomSpace.constant = 160.0f;
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    return NO;
}
- (IBAction)sendPressed:(id)sender
{
    [self send];
}

-(void)send
{
    NSString *text = _comment.text;
    
    if ((flagLike || flagUnlike) == NO) {
        [AlertDialog messageWithText:@"Поставте понравилось/не понравилось"];
    } else {
        TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
        [getAPI addRating:flagLike Commet:text OnReady:^
         {
             [AlertDialog messageWithText:@"Спасибо за отзыв"];
         }];
    }
}


@end
