//
//  RevealFronController.m
//  Minimum
//
//  Created by Nail Sharipov on 20/09/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "RevealFronController.h"

@interface RevealFronController ()

@end

@implementation RevealFronController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customSetup];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController) {
        CGFloat width = [self view].frame.size.width - 44.0f;
        [self.revealViewController setRearViewRevealWidth:width];
    }
    
    [self.revealViewController panGestureRecognizer];
}

- (void)applicationFinishedRestoringState
{
    [self customSetup];
}


@end
