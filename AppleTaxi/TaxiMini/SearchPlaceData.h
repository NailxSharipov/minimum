//
//  SearchPlaceData.h
//  TaxiMini
//
//  Created by Nail Sharipov on 10/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchPlaceData : NSObject

// type 1 - street 2 - bus station 3 - other

@property (strong, nonatomic) NSString *dataId;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *name;

- (BOOL)isStreet;

+ (BOOL)isStreet: (NSString *)type;

- (BOOL)isEqual:(SearchPlaceData *)placeData;

@end
