//
//  SearchPlaceData.m
//  TaxiMini
//
//  Created by Nail Sharipov on 10/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "SearchPlaceData.h"

@implementation SearchPlaceData

+ (BOOL)isStreet: (NSString *)type
{
    return [type isEqualToString:@"1"];
}

- (BOOL)isStreet
{
    return [SearchPlaceData isStreet:_type];
}

- (BOOL)isEqual:(SearchPlaceData *)placeData {
    if (_dataId != nil && placeData.dataId != nil && ![_dataId isEqualToString:placeData.dataId]) {
        return NO;
    }
    
    if (_name != nil && placeData.name != nil && ![_name isEqualToString:placeData.name]) {
        return NO;
    }

    if (_type != nil && placeData.type != nil && ![_type isEqualToString:placeData.type]) {
        return NO;
    }
    
    return YES;
    
}

@end
