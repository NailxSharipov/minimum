//
//  ServerData.h
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CityData.h"
#import "Brand.h"

@interface ServerData : NSObject

@property (strong, nonatomic) NSString* serverUrl;
@property (strong, nonatomic) NSString* mainServerUrl;
@property (strong, nonatomic) NSArray* cities;
//@property (strong, nonatomic) Brand* brand;

+ (instancetype)getInstance;

- (CityData *)getCityById: (NSString *)cityId;

@end
