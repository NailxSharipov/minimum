//
//  ServerData.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "ServerData.h"

@implementation ServerData

static ServerData *_instance;

+ (instancetype)getInstance
{
    if (_instance == nil) {
        _instance = [[ServerData alloc] init];
    }
    
    return _instance;
}

- (id)init
{
    self = [super init];
    if (self) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *url = [defaults objectForKey:@"url"];
        if(url != nil) {
            _serverUrl = [NSString stringWithFormat:@"%@%@%@", @"http://", url, @"/fm.html"];
        } else {
            _serverUrl = @"http://taxi-leader.ru/fm.html";
        }
        _mainServerUrl = @"http://taxi-leader.ru/fm.html";
    }
    return self;
}

- (CityData *)getCityById: (NSString *)cityId
{
    CityData *city;
    if (_cities != nil) {
        for (CityData * c in _cities) {
            if ([[c cityId] isEqualToString: cityId]) {
                city = c;
                break;
            }
        }
    }
    return city;
}


@end
