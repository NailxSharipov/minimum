//
//  TXButton.m
//  Minimum
//
//  Created by Nail Sharipov on 26/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "TXButton.h"

@implementation TXButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _localInit];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self _localInit];
}

- (UIFont *)titleFont {
    CGFloat fSize = self.titleLabel.font.pointSize;
    return [UIFont fontWithName:@"B52" size:fSize];
}

- (void)_localInit {
    self.titleLabel.font = [self titleFont];
}

@end
