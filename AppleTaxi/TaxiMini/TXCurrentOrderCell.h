//
//  CurrentOrderCell.h
//  Minimum
//
//  Created by Nail Sharipov on 17/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderData.h"
#import "TXOrderCellEvent.h"

@interface TXCurrentOrderCell : UITableViewCell

@property (nonatomic, strong)OrderData *orderData;
@property (nonatomic, weak)id<TXOrderCellEvent> delegate;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

+ (CGFloat)cellHeight:(OrderData *)orderData;

@end
