//
//  CurrentOrderCell.m
//  Minimum
//
//  Created by Nail Sharipov on 17/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "TXCurrentOrderCell.h"

@interface TXCurrentOrderCell ()

@property (weak, nonatomic) IBOutlet UILabel *place1;
@property (weak, nonatomic) IBOutlet UILabel *place2;

@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *carDescription;
@property (weak, nonatomic) IBOutlet UILabel *carNumber;
@property (weak, nonatomic) IBOutlet UILabel *driverPhone;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIButton *cancel;


@end

@implementation TXCurrentOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

#pragma Public API

+ (NSString *)cellIdentifier {
    return @"TXCurrentOrderCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXCurrentOrderCell" bundle:nil];
}

+ (CGFloat)cellHeight:(OrderData *)orderData {
    return 222.0f;
}

- (IBAction)onCancel:(id)sender {
    if (_delegate != nil) {
        [_delegate onCancel:_orderData];
    }
}

- (void)setOrderData:(OrderData *)orderData {
    _orderData = orderData;

    /*
     1    Предварительный
     2    Поиск машины
     3    Машина едет к клиенту
     4    Машина ждёт клиента
     5    Клиент в курсе
     6    Клиент в машине
     102    Отказ без машины
     103    Клиент не поехал
     104    Завершен
     */
    

    int code = orderData.code;
    if (code > 10 || orderData.isCanceled) {
        _cancel.enabled = NO;
    } else {
        _cancel.enabled = YES;
    }
    
    if (code < 10 && orderData.isCanceled) {
        _cancel.enabled = NO;
        orderData.status = @"заказ отменен";
    }

    
    _place1.text = orderData.from;
    _place2.text = orderData.to;

    if (orderData.status != nil) {
        _status.text = orderData.status;
    } else {
        _status.text = orderData.status;
    }

    if (orderData.car != nil) {
        _carDescription.text = orderData.car;
    } else {
        _carDescription.text = @"";
    }

    if (orderData.carNumber != nil) {
        _carNumber.text = [NSString stringWithFormat:@" %@", orderData.carNumber];
    } else {
        _carNumber.text = @"";
    }

    if (orderData.phone != nil) {
        _driverPhone.text = [NSString stringWithFormat:@" %@", orderData.phone];
    } else {
        _driverPhone.text = @"";
    }
    
    if (orderData.price != nil) {
        _price.text = [NSString stringWithFormat:@" %@", orderData.price];
    } else {
        _price.text = @"";
    }
}
@end
