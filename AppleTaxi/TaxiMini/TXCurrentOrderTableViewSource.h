//
//  TXCurrentOrderTableViewSource.h
//  Minimum
//
//  Created by Nail Sharipov on 17/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TXOrderCellEvent.h"
#import "OrderData.h"

@interface TXCurrentOrderTableViewSource : NSObject

- (void)setTableView:(UITableView *)tableView;

- (void)setItems:(NSArray *)items;

- (void)addItems:(NSArray *)items;

- (void)removeItemAtIndex:(NSUInteger)index;

- (void)removeItem:(OrderData *)item;

- (void)updateItem:(OrderData *)item;

- (void)updateItem:(OrderData *)item atIndex:(NSUInteger)index;

- (void)clearData;

- (void)refreshTable;


@property(nonatomic, readonly) NSInteger count;
@property(nonatomic, weak) id<TXOrderCellEvent> delegate;

@end
