//
//  TXFastCell.h
//  Minimum
//
//  Created by Nail Sharipov on 17/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlaceData.h"

@interface TXFastCell : UITableViewCell

@property (nonatomic, weak)PlaceData *place;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

+ (CGFloat)cellHeight;

@end
