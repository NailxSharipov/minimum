//
//  TXFastCell.m
//  Minimum
//
//  Created by Nail Sharipov on 17/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "TXFastCell.h"


@interface TXFastCell ()

@property (weak, nonatomic) IBOutlet UILabel *address;

@end

@implementation TXFastCell

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma Public API

+ (NSString *)cellIdentifier {
    return @"TXFastCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXFastCell" bundle:nil];
}

+ (CGFloat)cellHeight {
    return 44.0f;
}

- (void)setPlace:(PlaceData *)place {
    _address.text = place.objectAddress;
}

@end
