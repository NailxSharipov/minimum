//
//  TXFastCellEvent.h
//  Minimum
//
//  Created by Nail Sharipov on 17/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "PlaceData.h"

@protocol TXFastCellEvent <NSObject>

@required
- (void)onFastSelect:(PlaceData *)place;

@end
