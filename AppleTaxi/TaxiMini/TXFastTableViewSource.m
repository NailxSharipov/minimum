//
//  TXFastTableViewSource.m
//  Minimum
//
//  Created by Nail Sharipov on 17/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "TXFastTableViewSource.h"
#import "TXFastCell.h"

@interface TXFastTableViewSource  () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation TXFastTableViewSource {
    NSMutableArray *_itemList;
    UITableView *_tableView;
}

#pragma Initialization

- (instancetype)init {
    self = [super init];
    if (self) {
        [self localInit];
    }
    
    return self;
}

- (void)localInit {
    _itemList = [[NSMutableArray alloc] init];
}

#pragma UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _itemList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self contentCellForIndexPath:indexPath];
}

- (UITableViewCell *)contentCellForIndexPath:(NSIndexPath *)indexPath {
    TXFastCell *cell = [_tableView dequeueReusableCellWithIdentifier:[TXFastCell cellIdentifier]];
    cell.place = _itemList[(NSUInteger) indexPath.row];
    
    return cell;
}

#pragma UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index = (NSUInteger) indexPath.row;
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (index < _itemList.count) {
        PlaceData *place = _itemList[(NSUInteger) indexPath.row];
        [_delegate onFastSelect:place];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [TXFastCell cellHeight];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


#pragma Public API

- (NSInteger)count {
    return _itemList.count;
}

- (void)setTableView:(UITableView *)tableView {
    _tableView = tableView;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [_tableView registerNib:[TXFastCell cellNib] forCellReuseIdentifier:[TXFastCell cellIdentifier]];
}

- (void)clearData {
    @synchronized (_itemList) {
        [_itemList removeAllObjects];
        [_tableView reloadData];
        [_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    }
}

- (void)refreshTable {
    [_tableView reloadData];
}

- (void)updateItem:(PlaceData *)item {
    @synchronized (_itemList) {
        NSIndexPath *itemPosition = [self pathForItem:item];
        if (!itemPosition) {
            return;
        }
        
        [_tableView beginUpdates];
        [_tableView reloadRowsAtIndexPaths:@[itemPosition] withRowAnimation:UITableViewRowAnimationNone];
        [_tableView endUpdates];
    }
}

- (void)updateItem:(PlaceData *)item atIndex:(NSUInteger)index {
    if (index >= _itemList.count) {
        return;
    }
    
    @synchronized (_itemList) {
        _itemList[index] = item;
        
        [_tableView beginUpdates];
        [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index
                                                                inSection:0]]
                          withRowAnimation:UITableViewRowAnimationNone];
        [_tableView endUpdates];
    }
}

- (NSIndexPath *)pathForItem:(PlaceData *)item {
    NSIndexPath *indexPath = nil;
    for (uint i = 0; i < _itemList.count; i++) {
        if ([item isEqual:_itemList[i]]) {
            indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            break;
        }
    }
    
    return indexPath;
}

- (void)addItems:(NSArray *)items {
    @synchronized (_itemList) {
        if (items.count == 0) {
            return;
        }
        
        // если список пока пуст - заполняем его и перегружаем таблицу
        if (_itemList.count == 0) {
            [_itemList addObjectsFromArray:items];
            [self refreshTable];
            
            return;
        }
        
        // если нет - обновляем новые ячейки
        NSMutableArray *indexes = [[NSMutableArray alloc] initWithCapacity:items.count];
        for (NSUInteger i = _itemList.count, len = _itemList.count + items.count; i < len; i++) {
            [indexes addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
        [_itemList addObjectsFromArray:items];
        
        if (indexes.count > 0) {
            [_tableView beginUpdates];
            [_tableView insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationNone];
            [_tableView endUpdates];
        }
    }
}

- (void)setItems:(NSArray *)items {
    @synchronized (_itemList) {
        if (items == nil || items.count == 0) {
            return;
        }
        
        _itemList = [[NSMutableArray alloc] initWithArray:items];
        
        [self refreshTable];
    }
}


- (void)performTableUpdateWithInsert:(NSArray *)insertIndexes update:(NSArray *)updateIndexes delete:(NSArray *)deleteIndexes {
    [_tableView beginUpdates];
    if (insertIndexes.count > 0) {
        [_tableView insertRowsAtIndexPaths:insertIndexes withRowAnimation:UITableViewRowAnimationNone];
    }
    if (deleteIndexes.count > 0) {
        [_tableView deleteRowsAtIndexPaths:deleteIndexes withRowAnimation:UITableViewRowAnimationNone];
    }
    if (updateIndexes.count > 0) {
        [_tableView reloadRowsAtIndexPaths:updateIndexes withRowAnimation:UITableViewRowAnimationNone];
    }
    [_tableView endUpdates];
}

- (void)removeItemAtIndex:(NSUInteger)index {
    @synchronized (_itemList) {
        if (_itemList.count <= index) {
            return;
        }
        
        [_itemList removeObjectAtIndex:index];
        
        NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:0];
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationLeft];
        [_tableView endUpdates];
    }
}

- (void)removeItem:(PlaceData *)item {
    @synchronized (_itemList) {
        NSUInteger index = [_itemList indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            return [(PlaceData *) obj isEqual:item];
        }];
        
        if (index == NSNotFound) {
            return;
        }
        
        [self removeItemAtIndex:index];
    }
}
@end