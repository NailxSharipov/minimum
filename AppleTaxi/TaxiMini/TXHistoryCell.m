//
//  TXHistoryCell.m
//  Minimum
//
//  Created by Nail Sharipov on 16/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "TXHistoryCell.h"

@interface TXHistoryCell ()

@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *time;

@end

@implementation TXHistoryCell {
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _address.lineBreakMode = NSLineBreakByWordWrapping;
    _address.numberOfLines = 0;
}

#pragma Public API

+ (NSString *)cellIdentifier {
    return @"TXHistoryCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXHistoryCell" bundle:nil];
}

+ (CGFloat)cellHeight:(HistoryData *)historyData {
    OrderData *order = historyData.order;
    return 18.0f * order.places.count + 8.0f + 16.0f;
}

- (void)setHistoryData:(HistoryData *)historyData {
    OrderData *order = historyData.order;
    self.address.text = [order getAddress];
    self.time.text = historyData.date;
    self.backgroundColor = [UIColor clearColor];
}

@end
