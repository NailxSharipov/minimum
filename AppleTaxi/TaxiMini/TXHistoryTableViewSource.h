//
//  TXHistoryTableViewSource.h
//  Minimum
//
//  Created by Nail Sharipov on 16/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HistoryData.h"
#import "HistoryCellEvent.h"

@interface TXHistoryTableViewSource : NSObject

- (void)setTableView:(UITableView *)tableView;

- (void)setItems:(NSArray *)items;

- (void)addItems:(NSArray *)items;

- (void)removeItemAtIndex:(NSUInteger)index;

- (void)removeItem:(HistoryData *)item;

- (void)updateItem:(HistoryData *)item;

- (void)updateItem:(HistoryData *)item atIndex:(NSUInteger)index;

- (void)clearData;

- (void)refreshTable;


@property(nonatomic, readonly) NSInteger count;
@property(nonatomic, weak) id<HistoryCellEvent> delegate;

@end
