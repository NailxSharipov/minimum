//
//  RepeatMenuCell.h
//  Minimum
//
//  Created by Nail Sharipov on 02/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoryData.h"

@interface TXHotHistoryCell : UITableViewCell

@property (nonatomic, weak)HistoryData *historyData;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

+ (CGFloat)cellHeight:(HistoryData *)historyData;

@end
