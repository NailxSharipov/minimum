//
//  RepeatMenuCell.m
//  Minimum
//
//  Created by Nail Sharipov on 02/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "TXHotHistoryCell.h"
#import "PlaceData.h"

@interface TXHotHistoryCell ()

@property (weak, nonatomic) IBOutlet UILabel *address;

@end

@implementation TXHotHistoryCell {

}

- (void)awakeFromNib {
    [super awakeFromNib];

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _address.lineBreakMode = NSLineBreakByWordWrapping;
    _address.numberOfLines = 0;
}

#pragma Public API

+ (NSString *)cellIdentifier {
    return @"TXHotHistoryCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXHotHistoryCell" bundle:nil];
}

+ (CGFloat)cellHeight:(HistoryData *)historyData {
    OrderData *order = historyData.order;
    return 18.0f * order.places.count + 8.0f;
}

- (void)setHistoryData:(HistoryData *)historyData {
    OrderData *order = historyData.order;
    self.address.text = [order getAddress];
    self.backgroundColor = [UIColor clearColor];
}

@end
