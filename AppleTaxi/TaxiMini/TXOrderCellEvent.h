//
//  OrderCellEvent.h
//  Minimum
//
//  Created by Nail Sharipov on 23/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderData.h"

@protocol TXOrderCellEvent <NSObject>

@required

- (void)onCancel:(OrderData*)orderData;

@end
