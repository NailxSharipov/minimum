//
//  TXRoundButton.h
//  Minimum
//
//  Created by Nail Sharipov on 26/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TXButton.h"

@interface TXRoundButton : TXButton

/**
 * Заливать фон кнопки, или только бордер
 */
@property (nonatomic, assign) BOOL filled;
@property (nonatomic, retain) NSNumber *borderWidth;
@property (nonatomic, retain) UIColor *fontColorInNormalState;
@property (nonatomic, retain) UIColor *fontColorInPressedState;
@property (nonatomic, retain) UIColor *fontColorInDisabledState;
@property (nonatomic, retain) UIColor *backgroundColorInNormalState;
@property (nonatomic, retain) UIColor *backgroundColorInPressedState;
@property (nonatomic, retain) UIColor *borderColor;


@end
