//
//  TXRoundButton.m
//  Minimum
//
//  Created by Nail Sharipov on 26/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "TXRoundButton.h"

@implementation TXRoundButton
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self localInit];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self localInit];
}

- (void)localInit {
    self.filled = YES;
    self.borderWidth = @2;
    
    
    CGFloat height = self.layer.frame.size.height;
    self.layer.cornerRadius = (height > 0.0f) ? 0.5f * height: 20.0f;
    self.layer.masksToBounds = NO;
    
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.3f;
    self.layer.shadowRadius = 0;
    self.layer.shadowOffset = CGSizeMake(0, 1);
    
    self.backgroundColor = self.backgroundColorInNormalState;
    
    [self setTitleColor:self.fontColorInNormalState forState:UIControlStateNormal];
    [self setTitleColor:self.fontColorInDisabledState forState:UIControlStateDisabled];
}
/*
- (UIFont *)titleFont {
    CGFloat fSize = self.titleLabel.font.pointSize;
    return [TSFontPallete defaultFontOfSize:fSize];
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self styleButtonOnTouchesBegan];
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self styleButtonOnTouchesEnded];
    [super touchesCancelled:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self styleButtonOnTouchesEnded];
    [super touchesEnded:touches withEvent:event];
}

- (void)styleButtonOnTouchesBegan {
    self.backgroundColor = self.backgroundColorInPressedState;
    [self setTitleColor:self.fontColorInPressedState forState:UIControlStateNormal];
}

- (void)styleButtonOnTouchesEnded {
    self.backgroundColor = self.backgroundColorInNormalState;
    [self setTitleColor:self.fontColorInNormalState forState:UIControlStateNormal];
}

#pragma Styles configuration

- (UIColor *)fontColorInNormalState {
    return self.filled ? [UIColor blackColor] : [UIColor whiteColor];
}

- (UIColor *)fontColorInPressedState {
    return [UIColor whiteColor];
}

- (UIColor *)fontColorInDisabledState {
    return [UIColor colorWithWhite:153.0f/255.0f alpha:1.0f];
}

- (UIColor *)borderColor {
    return [UIColor colorWithWhite:243.0f/255.0f alpha:1.0f];
}

- (UIColor *)backgroundColorInNormalState {
    return self.state == UIControlStateDisabled ? [UIColor whiteColor] : ( self.filled ? [UIColor whiteColor] : [UIColor clearColor] );
//    return self.state == UIControlStateDisabled ? [UIColor colorWithWhite:204.0f/255.0f alpha:1.0f] : ( self.filled ? [self borderColor] : [UIColor clearColor] );
}

- (UIColor *)backgroundColorInDisabledState {
    return [UIColor colorWithWhite:204.0f/255.0f alpha:1.0f];
}

- (UIColor *)backgroundColorInPressedState {
    return self.filled ? [UIColor colorWithRed:1.0f green:88.0f/255.0f blue:16.0f/255.0f alpha:1.0f] : [UIColor colorWithRed:1.0f green:108.0f/255.0f blue:67.0f/255.0f alpha:0.5f];
}

- (void)setFilled:(BOOL)filled {
    _filled = filled;
    
    self.layer.borderColor = [self borderColor].CGColor;
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
    self.layer.cornerRadius = self.frame.size.width / 2;
    
    self.layer.borderWidth = 0;
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
}

- (void)setBorderWidth:(NSNumber *)borderWidth {
    _borderWidth = borderWidth;
    self.layer.borderWidth = _borderWidth ? [_borderWidth floatValue] : (1.0f / [UIScreen mainScreen].scale);
}

- (void)setEnabled:(BOOL)enabled {
    super.enabled = enabled;
    
    self.backgroundColor = [self backgroundColorInNormalState];
    self.layer.borderColor = enabled ? [self borderColor].CGColor : [UIColor colorWithWhite:204.0f/255.0f alpha:1.0f].CGColor;
}
@end
