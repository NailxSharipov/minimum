//
//  TXSearchCell.h
//  Minimum
//
//  Created by Nail Sharipov on 16/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchPlaceData.h"

@interface TXSearchCell : UITableViewCell

@property (nonatomic, weak)SearchPlaceData *place;

+ (NSString *)cellIdentifier;

+ (UINib *)cellNib;

+ (CGFloat)cellHeight:(SearchPlaceData *)searchPlaceData;

@end
