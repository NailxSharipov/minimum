//
//  TXSearchCell.m
//  Minimum
//
//  Created by Nail Sharipov on 16/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "TXSearchCell.h"

@interface TXSearchCell ()

@property (weak, nonatomic) IBOutlet UILabel *object;
@property (weak, nonatomic) IBOutlet UILabel *address;

@end

@implementation TXSearchCell

CGSize _SearchCellLabelSize;
UIFont *_SearchCellMessageFont;

- (void)awakeFromNib {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    CGFloat labelWidth = [[UIScreen mainScreen] bounds].size.width - 12.0f;
    _SearchCellMessageFont = [UIFont systemFontOfSize:12.0f];
    _SearchCellLabelSize = CGSizeMake(labelWidth, MAXFLOAT);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma Public API

+ (NSString *)cellIdentifier {
    return @"TXSearchCell";
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:@"TXSearchCell" bundle:nil];
}

+ (CGFloat)cellHeight:(SearchPlaceData *)searchPlaceData {
    if (searchPlaceData.name != nil && _SearchCellMessageFont != nil) {
        CGRect labelRect = [searchPlaceData.name boundingRectWithSize:_SearchCellLabelSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:_SearchCellMessageFont} context:nil];
            int lines = (int)ceil(labelRect.size.height / 16.0f);
        if (lines < 2) {
            return 42.0f;
        } else {
            return 58.0f;
        }
    } else {
        return 42.0f;
    }
}

- (void)setPlace:(SearchPlaceData *)place {
    if ([place.type isEqualToString:@"1"]) {
        _object.text = @"Улица";
        _address.text = place.name;
    } else if ([place.type isEqualToString:@"2"]) {
        _object.text = @"Остановка";
        _address.text = place.name;
    } else if ([place.type isEqualToString:@"3"]) {
        _object.text = @"Объект";
        _address.text = place.name;
    } else {
        _object.text = @"";
        _address.text = @"";
    }

}

@end
