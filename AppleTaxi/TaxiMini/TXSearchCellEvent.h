//
//  TXSearchCellEvent.h
//  Minimum
//
//  Created by Nail Sharipov on 16/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import "SearchPlaceData.h"

@protocol TXSearchCellEvent <NSObject>

@required

- (void)onSelect:(SearchPlaceData *)place;

@end

