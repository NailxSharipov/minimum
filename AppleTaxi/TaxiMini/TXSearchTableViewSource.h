//
//  TXSearchTableViewSource.h
//  Minimum
//
//  Created by Nail Sharipov on 16/08/15.
//  Copyright (c) 2015 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SearchPlaceData.h"
#import "TXSearchCellEvent.h"

@interface TXSearchTableViewSource : NSObject

- (void)setTableView:(UITableView *)tableView;

- (void)setItems:(NSArray *)items;

- (void)addItems:(NSArray *)items;

- (void)removeItemAtIndex:(NSUInteger)index;

- (void)removeItem:(SearchPlaceData *)item;

- (void)updateItem:(SearchPlaceData *)item;

- (void)updateItem:(SearchPlaceData *)item atIndex:(NSUInteger)index;

- (void)clearData;

- (void)refreshTable;


@property(nonatomic, readonly) NSInteger count;
@property(nonatomic, weak) id<TXSearchCellEvent> delegate;

@end
