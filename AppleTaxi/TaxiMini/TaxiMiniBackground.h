//
//  Background.h
//  TaxiMini
//
//  Created by Nail Sharipov on 02/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TaxiMiniBackgroundDelegate <NSObject>

@required

- (void) swipeLeft;

- (void) tap;

@end

@interface TaxiMiniBackground : UIImageView
{
@protected
    CGFloat width;
    CGFloat height;
}

@property (strong, nonatomic) NSObject<TaxiMiniBackgroundDelegate>* delegate;

- (void)commonInit;

- (void)setImageHidden:(BOOL)hidden;

@end
