//
//  Background.m
//  TaxiMini
//
//  Created by Nail Sharipov on 02/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "TaxiMiniBackground.h"

@implementation TaxiMiniBackground
{
    UIImage *image;
    UIView *shadow;
}
// 320 480 568
- (void)commonInit
{
    UIWindow * mainWindow = [UIApplication sharedApplication].windows.firstObject;
    width = mainWindow.frame.size.width;
    height = mainWindow.frame.size.height;
    
    self.frame = CGRectMake(0.0f, 0.0f, width, height);

    if (width / height >= 320 / 500) { // small screen
        image = [UIImage imageNamed:@"background_small"];
    } else { // big screen
        image = [UIImage imageNamed:@"background_large"];
    }
    self.image = image;
    
    
    shadow = [[UIView alloc] initWithFrame:self.frame];
    shadow.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4f];
    [self addSubview:shadow];
    shadow.hidden = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shadowViewTapped:)];
    [shadow addGestureRecognizer:tap];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(shadowSwipeLeft:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [shadow addGestureRecognizer:swipeLeft];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)setImageHidden:(BOOL)hidden
{
    if(hidden) {
        shadow.hidden = NO;
    } else {
        shadow.hidden = YES;
    }
}

- (void)shadowViewTapped:(UITapGestureRecognizer *)gr {
    if (!shadow.isHidden && self.delegate != nil) {
        [self.delegate tap];
    }
}

- (void)shadowSwipeLeft:(UISwipeGestureRecognizer *)gr {
    if (!shadow.isHidden && self.delegate != nil) {
        [self.delegate swipeLeft];
    }
}


@end
