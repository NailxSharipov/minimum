//
//  TaxiMiniGetAPI.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "TaxiMiniGetAPI.h"
#import "URLEditor.h"
#import "UserData.h"
#import "ServerData.h"
#import "TaxiMiniParser.h"
#import "HttpTextDataRequest.h"
#import "AlertDialog.h"
#import "PlaceManager.h"
#import "OrderManager.h"


@implementation TaxiMiniGetAPI
{
    TaxiMiniParser *parser;
//    NSString *serverURL;
    Reachability* appleReachability;
}

- (id) init
{
    self = [super init];
    if (self) {
//        serverURL = [[ServerData getInstance] serverUrl];
        parser = [[TaxiMiniParser alloc] init];
        NSString *serverURL = [[ServerData getInstance] mainServerUrl];
        appleReachability = [Reachability reachabilityWithHostName: serverURL];
    }
    return self;
}

- (void) loginPhoneNumber: (NSString *) phoneNumber Password: (NSString *)password OnReady:(OnLoginHandler) onReady
{
    NSString *serverURL = [[ServerData getInstance] mainServerUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
    [urlEditor addParameter:@"t" Value:@"15"];
    [urlEditor addParameter:@"l" Value:phoneNumber];
    [urlEditor addParameter:@"p" Value:password];

    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
     {
         NSArray *result = [parser login:textData];
         
         BOOL isLogin = [[result objectAtIndex:0] boolValue];
         NSString *message = [result objectAtIndex:1];
         onReady(isLogin, message);
     }
                     onError:^()
     {
         [self error];

         onReady(NO, nil);
     }];
}


- (void) remindPasswordForPhoneNumber: (NSString *)phoneNumber IsCall: (BOOL) isCall OnReady:(OnRemindHandler) onReady;

{
    
//    t=13&version=2&l=9273494490
    NSString *serverURL = [[ServerData getInstance] mainServerUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
    [urlEditor addParameter:@"t" Value:@"13"];
    [urlEditor addParameter:@"version" Value:@"2"];
    [urlEditor addParameter:@"l" Value:phoneNumber];
    if (isCall) {
        [urlEditor addEmptyParameter:@"callme"];
    }
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
     {
         NSString* message = [parser remindPassword:textData];
         onReady(message);
     }
                     onError:^()
     {
         NSLog(@"Error remind password");
         [self error];
         onReady(nil);
     }];
}

- (void) changePassword: (NSString *)password OnReady:(OnChangeHandler) onReady;

{
    UserData *user = [UserData getInstance];
    
    //    t=passw&change_pass=1&pass=7777&l=9273494490&p=123456
    NSString *serverURL = [[ServerData getInstance] mainServerUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
    [urlEditor addParameter:@"t" Value:@"passw"];
    [urlEditor addParameter:@"change_pass" Value:@"1"];
    [urlEditor addParameter:@"pass" Value:password];
    [urlEditor addParameter:@"l" Value:user.phoneNumber];
    [urlEditor addParameter:@"p" Value:user.password];
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
     {
         NSString* message = [parser remindPassword:textData];
         onReady(message);
     }
                     onError:^()
     {
         NSLog(@"Error remind password");
         [self error];
         onReady(nil);
     }];
}


- (void) loadCitiesOnReady: (OnReadyCitiesHandler) onReady
{
    NSString *serverURL = [[ServerData getInstance] mainServerUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];

    [urlEditor addParameter:@"t" Value:@"brands"];
    [urlEditor addParameter:@"a" Value:@"all"];
    [urlEditor addParameter:@"out" Value:@"js"];
    [urlEditor addParameter:@"lang" Value:@"ru"];
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
    {
        NSArray *cities = [parser getCitiesFromText:textData];
        onReady(cities);
    }
                     onError:^()
    {
        NSLog(@"Error cities are not loaded");
        [self error];
        onReady(nil);
    }];
}

- (void) searchPlace: (NSString *)key OnReady:(OnSearchPlaceHandler) onReady
{
    [self searchPlace:key ObjectType:0 OnReady:onReady];
}

// 1-улицы, 2- остановки, 3-прочие объекты
- (void) searchPlace: (NSString *)key ObjectType:(NSUInteger) objectType OnReady:(OnSearchPlaceHandler) onReady
{
    UserData *user = [UserData getInstance];
    NSString *phoneNumber = user.phoneNumber;
    NSString *password = user.password;
    
    NSString *serverURL = [[ServerData getInstance] serverUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
    [urlEditor addParameter:@"t" Value:@"6"];
    [urlEditor addParameter:@"l" Value:phoneNumber];
    [urlEditor addParameter:@"p" Value:password];
    [urlEditor addParameter:@"w" Value:key];
    [urlEditor addParameter:@"pg" Value:@"1"];
    [urlEditor addParameter:@"lm" Value:@"10"];
    if (objectType > 0) {
        [urlEditor addParameter:@"f" Value:[NSString stringWithFormat:@"%lui", (unsigned long)objectType]];
    }
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
     {
         NSArray *places = [parser getPlacesFromText:textData];
         onReady(places);
     }
                     onError:^()
     {
         NSLog(@"Error place search");
         onReady(nil);
     }];
}

- (void) addRating: (BOOL)like Commet:(NSString *)comment OnReady:(OnAddRatingHandler) onReady
{
    NSString *text;
    
    if (like) {
        text = [NSString stringWithFormat:@"Нравится/n%@", comment];
    } else {
        text = [NSString stringWithFormat:@"Не нравится/n%@", comment];
    }
    
    UserData *user = [UserData getInstance];
    NSString *phoneNumber = user.phoneNumber;
    NSString *password = user.password;
    
    NSString *serverURL = [[ServerData getInstance] serverUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
    [urlEditor addParameter:@"t" Value:@"4"];
    [urlEditor addParameter:@"l" Value:phoneNumber];
    [urlEditor addParameter:@"p" Value:password];
    [urlEditor addParameter:@"text" Value:text];
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
     {
         onReady();
     }
                     onError:^()
     {
         NSLog(@"Error add rating");
         [self error];
         onReady();
     }];
}

- (void) makeOrder: (BOOL)order OnReady:(orderPriceHandler) onReady;
{
    //t=order&a=cost&l=9273494490&p=123456&ph=9171234567&com1=комментарий&no=1&ho=0&p1=1&pn=2&ob2=100&h2=3&ob3=200&brand=1
    
    UserData *user = [UserData getInstance];

    PlaceManager *placeManager = [PlaceManager getInstance];
    
    PlaceData *from = [placeManager.places objectAtIndex:0];
    
    NSString *phoneNumber = user.phoneNumber;
    NSString *password = user.password;
    NSString *secondPhone = user.secondPhoneNumber;
    NSString *comment = nil;

    if (from.porch != nil && from.porch.length > 0) {
        comment = from.porch;
    } else {
        comment = user.comment;
    }
    
    NSString *serverURL = [[ServerData getInstance] serverUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
    [urlEditor addParameter:@"t" Value:@"order"];
    
    if (order) {
        [urlEditor addParameter:@"a" Value:@"send"];
        [urlEditor addParameter:@"ph" Value:secondPhone];
        [urlEditor addParameter:@"com1" Value:comment];
    } else {
        [urlEditor addParameter:@"a" Value:@"cost"];
    }
    [urlEditor addParameter:@"l" Value:phoneNumber];
    [urlEditor addParameter:@"p" Value:password];
    [urlEditor addParameter:@"no" Value:@"1"];
    

    [urlEditor addParameter:@"ob1" Value:from.objectId];
    [urlEditor addParameter:@"h1" Value:from.home];
    [urlEditor addParameter:@"brand" Value:user.city.brandId];
    
    NSUInteger count = [placeManager getSize];
    long cc = count - 1;
    
    [urlEditor addParameter:@"pn" Value:[NSString stringWithFormat:@"%li", cc]];

    NSArray *places = placeManager.places;
    NSUInteger i = 2;
    for (NSUInteger j = 1; j < places.count; j++) {
        PlaceData *place = places[j];
        if (place != nil && place.objectId != nil && place.objectId.length > 0) {
            NSString *ob = [NSString stringWithFormat:@"ob%lu", (unsigned long)i];
            NSString *h = [NSString stringWithFormat:@"h%lu", (unsigned long)i];
            
            [urlEditor addParameter:ob Value:place.objectId];
            [urlEditor addParameter:h Value:place.home];
            i++;
        }
    }
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
     {
         NSDictionary *dict = [parser makeOrder: textData];
         onReady(dict);
     }
                     onError:^()
     {
         [self error];
         onReady(nil);
     }];
}

- (void) getHistoryOnReady:(onHistoryLoaded) onReady
{
    UserData *user = [UserData getInstance];
    NSString *phoneNumber = user.phoneNumber;
    NSString *password = user.password;
    
    NSString *serverURL = [[ServerData getInstance] serverUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
//    t=history&l=9273494490&p=123456&pg=1&max=10&version=2
    [urlEditor addParameter:@"t" Value:@"history"];
    [urlEditor addParameter:@"l" Value:phoneNumber];
    [urlEditor addParameter:@"p" Value:password];
    [urlEditor addParameter:@"pg" Value:@"1"];
    [urlEditor addParameter:@"max" Value:@"15"];
    [urlEditor addParameter:@"version" Value:@"2"];
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
     {
         NSArray *orders = [parser getHistory: textData];
         onReady(orders);
     }
                     onError:^()
     {
         NSLog(@"Error get history");
         onReady(nil);
     }];
    
}

- (void) getCurrentOrderOnReady:(onCurrentOrderLoaded) onReady
{
    UserData *user = [UserData getInstance];
    NSString *phoneNumber = user.phoneNumber;
    NSString *password = user.password;
    
    NSString *serverURL = [[ServerData getInstance] serverUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
    //    &t=42&l=9279207433&p=111111&version=4.0.3
    //    t=history&l=9273494490&p=123456&pg=1&max=10&version=2
    [urlEditor addParameter:@"t" Value:@"42"];
    [urlEditor addParameter:@"l" Value:phoneNumber];
    [urlEditor addParameter:@"p" Value:password];
    [urlEditor addParameter:@"version" Value:@"4.0.3"];
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
     {
//         NSString *current = @"result=2\nid=60348636|from=Ленина 1 подъезд 2|to=Сагита Агиша 34ffwerw|price=105|status=Клиент не поехал серебристая/дэу-нексия/943|driverphone=|status_code=102\nid=60348633|from=Ленина 2 подъезд 2|to=Сагита Агиша 34ffwerw|price=105|status=Клиент не поехал серебристая/дэу-нексия/943|driverphone=+79872502667|status_code=2";
         
         NSArray *orders = [parser getOrders:textData];
//         NSArray *orders = [parser getOrders:current];
         onReady(orders);
     }
                     onError:^()
     {
         NSLog(@"Error get order");
         onReady(nil);
     }];
}

- (void) cancelOrder:(NSString *) orderId OnCancel:(onCancelOrder) onCancel;
{
    UserData *user = [UserData getInstance];
    NSString *phoneNumber = user.phoneNumber;
    NSString *password = user.password;
    
    NSString *serverURL = [[ServerData getInstance] serverUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
    //t=5&l=9273494490&p=123456&z=111
    [urlEditor addParameter:@"t" Value:@"5"];
    [urlEditor addParameter:@"l" Value:phoneNumber];
    [urlEditor addParameter:@"p" Value:password];
    [urlEditor addParameter:@"z" Value:orderId];

    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
     {
         NSLog(@"Order cancel: %@", textData);
         onCancel(YES);
     }
                     onError:^()
     {
         onCancel(NO);
         [self error];
     }];
}

- (void) deleteHistoryOrder:(NSString *) orderId
{
    UserData *user = [UserData getInstance];
    NSString *phoneNumber = user.phoneNumber;
    NSString *password = user.password;
    
    NSString *serverURL = [[ServerData getInstance] serverUrl];
    URLEditor *urlEditor = [[URLEditor alloc] initWithServerURL:serverURL];
    
    //t=del&id=id_заказа
    [urlEditor addParameter:@"t" Value:@"del"];
    [urlEditor addParameter:@"l" Value:phoneNumber];
    [urlEditor addParameter:@"p" Value:password];
    
    if (orderId != nil) {
        [urlEditor addParameter:@"id" Value:orderId];
    } else {
        [urlEditor addParameter:@"id" Value:@"all"];
    }
    
    NSURL *url = [urlEditor getURL];
    
    HttpTextDataRequest *request = [[HttpTextDataRequest alloc] init];
    
    [request doGetRequestURL:url
                     onReady:^(NSString* textData)
    {
        NSLog(@"History deleted");
    }
                     onError:^()
    {
        [self error];
    }];

}

- (void) error
{
    NetworkStatus status = appleReachability.currentReachabilityStatus;
    
    switch (status) {
        case NotReachable:
            NSLog(@"хост недоступен");
            [AlertDialog errorWithText:@"Сервер недоступен. Проверьте подключение к интернету."];
            // хост недоступен
            break;
        case ReachableViaWiFi:
            // доступен через WiFi
            NSLog(@"доступен через WiFi");
            break;
        case ReachableViaWWAN:
            // доступен через 3G или EDGE
            NSLog(@"доступен через 3G или EDGE");
            break;
    }
    NSLog(@"Error cancel order");
}

@end
