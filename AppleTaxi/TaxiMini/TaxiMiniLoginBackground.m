//
//  TaxiMiniLoginBackground.m
//  TaxiMini
//
//  Created by Nail Sharipov on 02/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "TaxiMiniLoginBackground.h"

@implementation TaxiMiniLoginBackground

- (void)commonInit
{
    [super commonInit];

    CGFloat size = 0.0f;
    if (width / height >= 320 / 500) { // small screen
        size = 36.0f;
    } else { // big screen
        size = 50.0f;
    }
    CGRect emblemRect = CGRectMake(0.5f * (width - 3.0f * size), 6.0f, 3.0f * size, 4.0f * size);
    UIImageView *emblem = [[UIImageView alloc] initWithFrame:emblemRect];
    emblem.image = [UIImage imageNamed:@"emblem"];
    [self addSubview:emblem];

}

@end
