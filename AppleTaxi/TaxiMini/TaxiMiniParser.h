//
//  TaxiMiniParser.h
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IRuTaxiParser.h"

@interface TaxiMiniParser : NSObject<IRuTaxiParser>


@end
