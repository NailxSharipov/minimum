//
//  TaxiMiniParser.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "TaxiMiniParser.h"
#import "CityData.h"
#import "SearchPlaceData.h"
#import "HistoryData.h"
#import "OrderData.h"
#import "PlaceData.h"
#import "LocationData.h"
#import "OrderManager.h"

@implementation TaxiMiniParser

// login

- (NSArray *)login: (NSString *) text
{
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:2];
    
    char code = [text characterAtIndex:0];
    if (code == '1') {
        [result addObject:@YES];
    } else {
        [result addObject:@NO];
    }
    
    NSString *message = nil;
    NSUInteger length = [text length];
    if (length > 2) {
        message = [text substringWithRange: NSMakeRange (2, length - 2)];
    }
    [result addObject:message];
    
    return result;
}

// remind password

- (NSString *)remindPassword: (NSString *) text
{
    NSString *message = nil;
    NSUInteger length = [text length];
    if (length > 2) {
        message = [text substringWithRange: NSMakeRange (2, length - 2)];
    }
    return message;
}

// cahnge passowrd

- (NSString *)changePassword: (NSString *) text
{
    NSString *message = nil;
    NSUInteger length = [text length];
    if (length > 2) {//error
        NSRange range = [text rangeOfString:@"error="];
        NSUInteger start = range.location + range.length;
        message = [text substringWithRange: NSMakeRange (start, length - start)];
    }
    return message;
}


// getCitiesFromText

- (CityData *)getCityFromText: (NSString *) text
{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
 
    NSUInteger n = [text length];
    
    unichar openBracket = '[';
    unichar closeBracket = ']';
    
    BOOL isOpen = YES;
    NSUInteger start = 0;
    NSUInteger end;
    
    for (NSUInteger i = 0; i < n; i++) {
        unichar current = [text characterAtIndex: i];
        if (isOpen) {
            if (current == openBracket) {
                start = i + 1;
                isOpen = NO;
            }
        } else {
            if (current == closeBracket) {
                end = i;
                isOpen = YES;
                
                NSString *block = [text substringWithRange: NSMakeRange (start, end - start)];
                if ([block length] > 2) {
                    NSArray *subStrings = [block componentsSeparatedByString:@"="];
                    NSString *parameter = [subStrings objectAtIndex:0];
                    NSString *value = [subStrings objectAtIndex:1];

                    [dictionary setObject:value forKey:parameter];
                }
            }
        }
    }
    
    CityData* city = [[CityData alloc] initWithDictionary:dictionary];
    
    return city;
}

- (NSArray *)getCitiesFromText: (NSString *) text
{

    NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error = nil;
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
    int i = 0;
    NSMutableArray *cities = [[NSMutableArray alloc] initWithCapacity:[jsonArray count]];
    for (NSDictionary *cityDict in jsonArray) {
        CityData* city = [[CityData alloc] init];
        city.cityId = cityDict[@"town_id"];
        city.title = cityDict[@"title"];
        city.brandId = cityDict[@"id"];
        city.brand = cityDict[@"name"];
        city.url = cityDict[@"url"];
        city.phonePrefix = cityDict[@"telpref"];
        NSString *money = cityDict[@"money"];
        if ([money isEqualToString: @"{rubles}"]) {
            money = @"рублей";
        }
        city.money = money;
        cities[i++] = city;
    }
   
    
    return cities;
}

// getPlaceFromText

- (SearchPlaceData *)getPlaceFromText: (NSString *) text
{
    NSUInteger n = [text length];
    
    unichar openBracket = '[';
    unichar closeBracket = ']';
    
    BOOL isOpen = NO;
    NSUInteger start = 0;
    NSUInteger end;
    
    
    for (NSUInteger i = 0; i < n; i++) {
        unichar current = [text characterAtIndex: i];
        if (isOpen) {
            if (current == openBracket) {
                start = i + 1;
                isOpen = NO;
            }
        } else {
            if (current == closeBracket) {
                end = i;
                isOpen = YES;
                
                NSString *block = [text substringWithRange: NSMakeRange (start, end - start)];
                if ([block length] > 2) {
                    NSArray *data =[block componentsSeparatedByString:@"|"];
                    SearchPlaceData *place = [[SearchPlaceData alloc] init];
                    place.dataId = [data objectAtIndex:1];
                    place.type = [data objectAtIndex:2];
                    place.name = [data objectAtIndex:3];
                    return place;
                }
            }
        }
    }

    return nil;
}
- (NSArray *)getPlacesFromText: (NSString *) text
{
    NSMutableArray *places = [[NSMutableArray alloc] init];
    
    NSUInteger n = [text length];
    
    unichar separator = '\n';
    
    NSUInteger start = 0;
    NSUInteger end;
    
    for (NSUInteger i = 0; i < n; i++) {
        if (i == n - 1 || [text characterAtIndex: i] == separator) {
            end = i;
            NSString *block = [text substringWithRange: NSMakeRange (start, end - start + 1)];
            start = end + 1;
            
            if ([block length] > 2) {
                SearchPlaceData *place = [self getPlaceFromText: block];
                if (place != nil) {
                    [places addObject:place];
                }
            }
        }
    }
    
    return places;
}

- (NSDictionary *) makeOrder: (NSString *) text
{
//    1 80 66926477
    NSString *strCode = [text substringWithRange: NSMakeRange (0, 1)];
    int code = [strCode intValue];
    NSNumber *numbCode = [NSNumber numberWithInt: code];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:numbCode forKey:@"code"];
    if (code == 1) {
        NSArray *data = [text componentsSeparatedByString:@" "];
        
        NSUInteger size = [data count];
        
        if (size > 1) {
            NSString *price = [data objectAtIndex:1];
            [dict setValue:price forKey:@"price"];
        }
        
        if (size > 2) {
            NSString *orderId = [data objectAtIndex:2];
            [dict setValue:orderId forKey:@"orderId"];
        }
    }
    return dict;
}

- (NSArray *)getHistory: (NSString *) text
{
    NSMutableArray *histories = nil;

    NSArray *lines = [text componentsSeparatedByString:@"\n"];
    NSUInteger n = [lines count];

    if (n > 0) {
        NSString *line;
        // result
        line = [lines objectAtIndex:0];
        if ([[self getValue: line] isEqualToString:@"1"]) {

            histories = [[NSMutableArray alloc] init];

            NSArray *bloks;
            NSString *tag;
            NSDictionary *values;
            NSUInteger i = 1;
            while (i < n) {
                line = [lines objectAtIndex:i];
                bloks = [line componentsSeparatedByString:@"|"];
                tag = [bloks objectAtIndex:0];
                
                i++;
                if ([tag isEqualToString:@"order"]) { // начало заказа

                    values = [self collectData:bloks];
                    
                    NSMutableArray* places = [[NSMutableArray alloc] init];
                    
                    OrderData *order = [[OrderData alloc] init];
                    order.places = places;
                    order.orderId = [values objectForKey:@"id"];
                    
                    HistoryData *historyDate = [[HistoryData alloc] init];
                    historyDate.order = order;
                    
                    historyDate.date = [values objectForKey:@"order_date"];
                    
                    [histories addObject:historyDate];
                    
                    line = [lines objectAtIndex:i];
                    bloks = [line componentsSeparatedByString:@"|"];
                    tag = [bloks objectAtIndex:0];
                    
                    while ([tag isEqualToString:@"route"]) {
                        values = [self collectData:bloks];
                        
                        PlaceData* place = [[PlaceData alloc] init];
                        [places addObject:place];
                        
                        place.objectId = [values objectForKey:@"o"];
                        place.street = [LocationData relaceReservedWords: values[@"n"]];
                        
                        NSString* home = [values objectForKey:@"h"];
                        if (home != nil && home.length > 0) {
                            place.type = @"1"; // street
                        }
                        NSArray* homeData = [home componentsSeparatedByString:@" "];
                        place.home = [homeData objectAtIndex:0];

                        i++;
                        line = [lines objectAtIndex:i];
                        bloks = [line componentsSeparatedByString:@"|"];
                        tag = [bloks objectAtIndex:0];
                    }
                }
            }
        }
    }
    if ([histories count] == 0) {
        histories = nil;
    }
    
    return histories;
}

- (NSArray *)getOrders:(NSString *)text {
    NSLog(@"order : \n%@", text);
    
    NSArray *lines = [text componentsSeparatedByString:@"\n"];
    NSMutableArray *orders = [[NSMutableArray alloc] init];
    if (lines != nil && lines.count > 1) {
        for (NSUInteger i = 1; i < lines.count; i++)  {
            NSString *line = lines[i];
            if (line != nil && line.length > 10) {
                [orders addObject:[self getOrder:line]];
            }
        }
    }
    
    return orders;
}

- (OrderData *)getOrder:(NSString *) text {
    OrderData *orderData = [[OrderData alloc] init];

    NSArray *block = [text componentsSeparatedByString:@"|"];
    NSDictionary *dict = [self collectData:block];

    orderData.orderId = dict[@"id"];
    orderData.from = dict[@"from"];
    orderData.to = dict[@"to"];
    
    orderData.price = dict[@"price"];
    orderData.phone = dict[@"driverphone"];
    
    NSString *status = dict[@"status"];
    
    NSArray *statusBlock = [status componentsSeparatedByString:@"/"];

    NSUInteger count = statusBlock.count;
    
    if (count > 0) {
        if (count == 1) {
            orderData.status = statusBlock[0];
        } else {
            NSArray *statusBlockBlock = [statusBlock[0] componentsSeparatedByString:@" "];
            NSUInteger size = statusBlockBlock.count;
            orderData.color = statusBlockBlock[size - 1];
            
            NSMutableArray* a = [[NSMutableArray alloc] initWithArray:statusBlockBlock];
            [a removeLastObject];

            orderData.status = [a componentsJoinedByString:@" "];;
        }
    }
    if (count > 1) {
        orderData.car = statusBlock[1];
    }
    if (count > 2) {
        orderData.carNumber = statusBlock[2];
    }

    NSString *code = dict[@"status_code"];
    
    orderData.code = [code intValue];
    
    orderData.isCanceled = [[OrderManager getInstance] isCanceledOrder:orderData.orderId];
    
    return orderData;
}

// common

- (NSString *) getValue: (NSString *)text
{
    NSArray *lines = [text componentsSeparatedByString:@"="];
    return [lines objectAtIndex:1];
}

- (NSDictionary *) collectData: (NSArray *)data
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    for (NSString *block in data) {
        NSArray *sep = [block componentsSeparatedByString:@"="];
        if ([sep count] > 1) {
            [dict setObject:[sep objectAtIndex:1] forKey:[sep objectAtIndex:0]];
        }
    }
    return dict;
}


@end
