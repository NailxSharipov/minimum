//
//  TextFieldHideKeyBoard.h
//  TaxiMini
//
//  Created by Nail Sharipov on 23/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "B52TextField.h"

@interface TextFieldModern : B52TextField<UITextFieldDelegate>

- (void) commonInit;

@end
