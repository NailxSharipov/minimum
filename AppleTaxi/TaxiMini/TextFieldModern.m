//
//  TextFieldHideKeyBoard.m
//  TaxiMini
//
//  Created by Nail Sharipov on 23/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "TextFieldModern.h"

@implementation TextFieldModern

- (void) commonInit
{
    [self setDelegate:self];
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];        
    }
    return self;
}

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

// hide keyboard
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// fix switch language button crush
- (id)customOverlayContainer
{
    return self;
}


/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
*/

@end
