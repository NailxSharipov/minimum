//
//  UserData.h
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "CityData.h"


@interface UserData : NSObject<CLLocationManagerDelegate>

@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *secondPhoneNumber;
@property (strong, nonatomic) NSString *comment;

@property (strong, nonatomic) CityData *city;

@property (nonatomic, readonly) double latitude;
@property (nonatomic, readonly) double longitude;
@property (weak, nonatomic, readonly) NSString *loaction;


+ (instancetype)getInstance;

typedef void (^OnReadyAddress)();
- (void)setFromAddress: (OnReadyAddress) onReady;



@end
