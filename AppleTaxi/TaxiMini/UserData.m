//
//  UserData.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "UserData.h"
#import "ServerData.h"
#import "GeoLocationGoogle.h"
#import "PlaceManager.h"
#import "TaxiMiniGetAPI.h"
#import "HistoryManager.h"

@implementation UserData
{
    CLLocationManager *_locationManager;
}
static UserData *instance;

+ (instancetype)getInstance
{
    if (instance == nil) {
        instance = [[UserData alloc] init];
    }
    return instance;
}

- (id) init
{
    self = [super init];
    
    if (self) {
        [self initGeoLocation];
    }
    
    return self;
}

@synthesize city = _city;
@synthesize phoneNumber = _phoneNumber;
@synthesize password = _password;
@synthesize secondPhoneNumber = _secondPhoneNumber;
@synthesize comment = _comment;

- (void) setCity:(CityData *)city
{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[city cityId] forKey:@"cityId"];
    [defaults setObject:[city title] forKey:@"title"];
    [defaults setObject:[city url] forKey:@"url"];
    [defaults setObject:[city brand] forKey:@"brand"];
    [defaults setObject:[city brandId] forKey:@"brandId"];
    [defaults setObject:[city money] forKey:@"money"];

    
    [defaults synchronize];
    
    [ServerData getInstance].serverUrl = [NSString stringWithFormat:@"%@%@%@", @"http://", [city url], @"/fm.html"];[city url];

    [[HistoryManager getInstance] clear];
    [[HistoryManager getInstance] loadHistory];
    
    _city = city;
}

- (CityData *)city
{
    if (_city == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

        NSString *cityId = [defaults objectForKey:@"cityId"];

        _city = [[ServerData getInstance] getCityById:cityId];
      
        if (_city == nil) {
            _city = [[CityData alloc] init];

            NSString *title = [defaults objectForKey:@"title"];
            NSString *url = [defaults objectForKey:@"url"];
            NSString *brand = [defaults objectForKey:@"brand"];
            NSString *brandId = [defaults objectForKey:@"brandId"];
            NSString *money = [defaults objectForKey:@"money"];
            
            if (cityId != nil) {
                _city.cityId = [defaults objectForKey:@"cityId"];
            }
            if (title != nil) {
                _city.title = [defaults objectForKey:@"title"];
            }
            if (url != nil) {
                _city.url = [defaults objectForKey:@"url"];
            }
            if (brand != nil) {
                _city.brand = [defaults objectForKey:@"brand"];
            }
            if (brandId != nil) {
                _city.brandId = [defaults objectForKey:@"brandId"];
            }
            if (money != nil) {
                _city.money = [defaults objectForKey:@"money"];
            }
        }
    }
    return _city;
}

- (void) setPhoneNumber:(NSString *)phoneNumber
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:phoneNumber forKey:@"phoneNumber"];
    [defaults synchronize];
    
    _phoneNumber = phoneNumber;
}

- (NSString *) phoneNumber
{
    if (_phoneNumber == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        _phoneNumber = [defaults objectForKey:@"phoneNumber"];
        int number = [_phoneNumber intValue];
        if (_phoneNumber == nil || number == 0) {
            _phoneNumber = @"";
        }
    }
    return _phoneNumber;
}

- (void) setPassword:(NSString *)password
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:password forKey:@"password"];
    [defaults synchronize];
    
    _password = password;
}

- (NSString *) password
{
    if (_password == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        _password = [defaults objectForKey:@"password"];
        if (_password == nil) {
            _password = @"";
        }
    }
    return _password;
}

- (void) setSecondPhoneNumber:(NSString *)secondPhoneNumber
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:secondPhoneNumber forKey:@"secondPhoneNumber"];
    [defaults synchronize];
    
    _secondPhoneNumber = secondPhoneNumber;
}

- (NSString *) secondPhoneNumber
{
    if (_secondPhoneNumber == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        _secondPhoneNumber = [defaults objectForKey:@"secondPhoneNumber"];
        int number = [_secondPhoneNumber intValue];
        if (_secondPhoneNumber == nil || number == 0) {
            _secondPhoneNumber = @"";
        }
    }
    return _secondPhoneNumber;
}

- (void) setComment:(NSString *)comment
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:comment forKey:@"comment"];
    [defaults synchronize];
    
    _comment = comment;
}

- (NSString *) comment
{
    if (_comment == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        _comment = [defaults objectForKey:@"comment"];
        if (_comment == nil) {
            _comment = @"";
        }
    }
    return _comment;
}

- (void)setFromAddress: (OnReadyAddress) onReady
{
    PlaceData *place = [[PlaceManager getInstance].places objectAtIndex:0];
    if (place.objectId == nil) {
        GeoLocationGoogle *geo = [[GeoLocationGoogle alloc] init];
        
        [geo searchByLocationLat: _latitude Lon:_longitude OnReadyLocation:^(LocationData *data)
         {
             if (data != nil) {
                 // find database id
                 TaxiMiniGetAPI *getAPI = [[TaxiMiniGetAPI alloc] init];
                 [getAPI searchPlace:[data streetName] OnReady:^(NSArray *places)
                  {
                      if (places != nil && [places count] > 0) {
                          if (place.objectId == nil) { //check again
                              SearchPlaceData *searchPlace = [places objectAtIndex:0];
                              place.objectId = searchPlace.dataId;
                              NSLog(@"objectId: %@", searchPlace.dataId);
                              place.locationData = data;
                              place.type = @"1";
                              if (onReady != nil) {
                                  onReady();
                              }
                          }
                      }
                  }];
             }
         }];
    }
}



- (NSString*)loaction
{
    NSString *loaction = [NSString stringWithFormat:@"%f,%f", _latitude,_longitude];
    NSString *encodedURL =[loaction stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    return encodedURL;
}

- (void) initGeoLocation {
    if ([CLLocationManager locationServicesEnabled]) {
        _latitude = 0.0;
        _longitude = 0.0;
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        
        SEL selector = NSSelectorFromString(@"requestWhenInUseAuthorization");
        if ([_locationManager respondsToSelector:selector]) {
            [_locationManager requestWhenInUseAuthorization];
        } else {
            [_locationManager startUpdatingLocation];
        }
    } else {

    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [_locationManager startUpdatingLocation];
    } else if (status == kCLAuthorizationStatusAuthorized) {
        // iOS 7 will redundantly call this line.
        [_locationManager startUpdatingLocation];
    } else if (status > kCLAuthorizationStatusNotDetermined) {

    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    _latitude = location.coordinate.latitude;
    _longitude = location.coordinate.longitude;
    
    [self setFromAddress: nil];
    
    NSLog(@"GeoLocation updated [%f, %f]", location.coordinate.latitude, location.coordinate.longitude);
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"GeoLocation fail %@", error);
}



@end
