//
//  WhiteButton.h
//  TaxiMini
//
//  Created by Nail Sharipov on 02/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "B52Button.h"

@interface WhiteButton : B52Button

- (id)initWithFrame:(CGRect)frame Radius:(CGFloat)radius;

@end
