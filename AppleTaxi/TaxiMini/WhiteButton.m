//
//  WhiteButton.m
//  TaxiMini
//
//  Created by Nail Sharipov on 02/08/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import "WhiteButton.h"

@implementation WhiteButton
{
    CGFloat _radius;
}

static CGFloat DEFAULT_RADIUS = 18.0f;

- (void)commonInit
{
    UIImage *buttonImage = [[UIImage imageNamed:@"button_white"] resizableImageWithCapInsets:UIEdgeInsetsMake(_radius, _radius, _radius, _radius)];
    UIImage *buttonImageHighlight = [[UIImage imageNamed:@"button_white_highlight"] resizableImageWithCapInsets:UIEdgeInsetsMake(_radius, _radius, _radius, _radius)];
    
    // Set the background for any states you plan to use
    [self setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self setBackgroundImage:buttonImageHighlight forState:UIControlStateSelected | UIControlStateHighlighted];
}

- (id)initWithFrame:(CGRect)frame Radius:(CGFloat)radius
{
    _radius = radius;
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _radius = DEFAULT_RADIUS;
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _radius = DEFAULT_RADIUS;
        [self commonInit];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        _radius = DEFAULT_RADIUS;
        [self commonInit];
    }
    return self;
}


@end
