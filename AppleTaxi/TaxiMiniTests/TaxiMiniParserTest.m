//
//  TaxiMiniParserTest.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TaxiMiniParser.h"
#import "CityData.h"
#import "SearchPlaceData.h"

@interface TaxiMiniParserTest : XCTestCase

@end

@implementation TaxiMiniParserTest

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testCitiesFromText
{
    NSString* data = @"[id=abakan][title=Абакан][url=abakan.rutaxi.ru][telpref=+7][tellen=10][lux=1][money=рублей][smsFrom=TaksuAbkn][tel=(3902)220-220][brand=1]\n[id=almaty][title=Алматы][url=taxi-leader.kz][telpref=+77][tellen=9][lux=0][money=тенге][smsFrom=LEADER][tel=(27)3-550-550][brand=1]";

    TaxiMiniParser *parser = [[TaxiMiniParser alloc] init];
    NSArray* cities = [parser getCitiesFromText:data];
    
    XCTAssertTrue([cities count] == 2, @"check cities count");
    
    XCTAssertTrue([[(CityData *)[cities objectAtIndex:0] cityId] isEqualToString: @"abakan"], @"check city id");
    XCTAssertTrue([[(CityData *)[cities objectAtIndex:0] title] isEqualToString: @"Абакан"], @"check city title");
    
}

- (void)testPlacesFromText
{
    NSString* data = @"{1}\n[|225|1|Достоевского|]\n[|12785|2|Достоевского по Аксакова (Аксакова 94)|]\n[|10929|2|Достоевского по Достоевского (Достоевского 102)|]";
    
    TaxiMiniParser *parser = [[TaxiMiniParser alloc] init];
    NSArray* places = [parser getPlacesFromText:data];
    
    XCTAssertTrue([places count] == 3, @"check place count");
    
    XCTAssertTrue([[(SearchPlaceData *)[places objectAtIndex:0] dataId] isEqualToString: @"225"], @"check place id");
    XCTAssertTrue([[(SearchPlaceData *)[places objectAtIndex:1] type] isEqualToString: @"2"], @"check place type");
    XCTAssertTrue([[(SearchPlaceData *)[places objectAtIndex:2] name] isEqualToString: @"Достоевского по Достоевского (Достоевского 102)"], @"check place type");
}

- (void)testChangePassword
{
    {
        NSString* data = @"result=0|error=Вы ввели неверный пароль. Пожалуйста, проверьте язык ввода и регистр (прописные и строчные буквы считаются разными).";
        
        TaxiMiniParser *parser = [[TaxiMiniParser alloc] init];
        NSString* result = [parser changePassword:data];
        
        XCTAssertTrue(result != nil, @"check is nil");
        
        XCTAssertTrue([result isEqualToString: @"Вы ввели неверный пароль. Пожалуйста, проверьте язык ввода и регистр (прописные и строчные буквы считаются разными)."], @"check error message");
    }
    {
        NSString* data = @"1";
        
        TaxiMiniParser *parser = [[TaxiMiniParser alloc] init];
        NSString* result = [parser changePassword:data];
        
        XCTAssertTrue(result == nil, @"check is nil");
    }
}

- (void)testGetHistory
{
    {
        NSString* data = @"result=1\norder|from=Ленина, 22/2 корп2|to=[Худайбердина, 2]|order_date=-|done_date=|status=Отменен|id=45296984|brand=1\nroute|id=45296984|type=3|o=365|n=Ленина|h=22/2 корп2|c=|e=1\nroute|id=45296984|type=3|o=791|n=Худайбердина|h=2|c=|e=1\norder|from=\"лени\" (Ленина, 3)|to=[ДК железнодорожников в сторону Жд вокзала]|order_date=-|done_date=|status=Отменен|id=45295998|brand=1\nroute|id=45295998|type=3|o=365|n=Ленина|h=3|c=|e=1\nroute|id=45295998|type=4|o=10918|n=ДК железнодорожников в сторону Жд вокзала|h=|c=|e=t=order&a=cost&l=9273494490&p=123456&ph=9171234567&com1=комментарий&\nno=1&\nho=0&p1=1&\npn=2&\nob2=100&\nh2=3&\nob3=200&\nbrand=1";
        
        TaxiMiniParser *parser = [[TaxiMiniParser alloc] init];
        NSArray* result = [parser getHistory:data];
        
        XCTAssertTrue([result count] == 2, @"check count");
    }
}

- (void)testGetCurrentOrder
{
    {
        NSString* data = @"result=1\n"
        "id=60348636|from=Ленина 1 подъезд 2|to=Сагита Агиша 34ffwerw|price=105|status=Клиент не поехал серебристая/дэу-нексия/943|driverphone=|status_code=7";
        
        TaxiMiniParser *parser = [[TaxiMiniParser alloc] init];
        OrderData* order = [parser getOrder:data];
        
        XCTAssertTrue([order.orderId isEqualToString:@"60348636"], @"check id");
        XCTAssertTrue([order.from isEqualToString:@"Ленина 1 подъезд 2"], @"check from");
        XCTAssertTrue([order.to isEqualToString:@"Сагита Агиша 34ffwerw"], @"check to");
        XCTAssertTrue([order.price isEqualToString:@"105"], @"check price");
        XCTAssertTrue([order.status isEqualToString:@"Клиент не поехал серебристая"], @"check status");
        XCTAssertTrue([order.car isEqualToString:@"дэу-нексия"], @"check car");
        XCTAssertTrue([order.carNumber isEqualToString:@"943"], @"check carNumber");
        XCTAssertTrue([order.phone isEqualToString:@""], @"check phone");
        XCTAssertTrue(order.code == 7, @"check code");
    }
}

@end
