//
//  GetURLEditorTest.m
//  TaxiMini
//
//  Created by Nail Sharipov on 21/07/14.
//  Copyright (c) 2014 Nail Sharipov. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "URLEditor.h"

@interface URLEditorTest : XCTestCase

@end

@implementation URLEditorTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testEmpty
{
    NSString* template = @"";
    
    URLEditor* urlEditor = [[URLEditor alloc] init];
    [urlEditor setServerURL:@""];
    
    NSString* url = [urlEditor getFormattedURL];
    
    XCTAssertTrue([template isEqualToString:url], @"compare: \n%@\n%@", template, url);
}

- (void)testReset
{
    NSString* template = @"";
    
    URLEditor* urlEditor = [[URLEditor alloc] init];
    [urlEditor setServerURL:@"localhost"];
    [urlEditor addParameter:@"a" Value:@"1"];
    [urlEditor addEmptyParameter:@"b"];
    [urlEditor reset];
    
    NSString* url = [urlEditor getFormattedURL];
    
    XCTAssertTrue([template isEqualToString:url], @"compare: \n%@\n%@", template, url);
}

- (void)testServerURL
{
    NSString* template = @"localhost";
    
    URLEditor* urlEditor = [[URLEditor alloc] init];
    [urlEditor setServerURL:@"localhost"];

    NSString* url = [urlEditor getFormattedURL];
    
    XCTAssertTrue([template isEqualToString:url], @"compare: \n%@\n%@", template, url);
}

- (void)testOneParameter
{
    NSString* template = @"?a=1";
    
    URLEditor* urlEditor = [[URLEditor alloc] init];
    [urlEditor addParameter:@"a" Value:@"1"];
    
    NSString* url = [urlEditor getFormattedURL];
    
    XCTAssertTrue([template isEqualToString:url], @"compare: \n%@\n%@", template, url);
}

- (void)testMultyParameters
{
    NSString* template = @"?a=1&b=2";
    
    URLEditor* urlEditor = [[URLEditor alloc] init];
    [urlEditor addParameter:@"a" Value:@"1"];
    [urlEditor addParameter:@"b" Value:@"2"];
    
    NSString* url = [urlEditor getFormattedURL];
    
    XCTAssertTrue([template isEqualToString:url], @"compare: \n%@\n%@", template, url);
}

- (void)testOneEmptyParameter
{
    NSString* template = @"?a";
    
    URLEditor* urlEditor = [[URLEditor alloc] init];
    [urlEditor addEmptyParameter:@"a"];
    
    NSString* url = [urlEditor getFormattedURL];
    
    XCTAssertTrue([template isEqualToString:url], @"compare: \n%@\n%@", template, url);
}

- (void)testMultyEmptyParameters
{
    NSString* template = @"?a&b";
    
    URLEditor* urlEditor = [[URLEditor alloc] init];
    [urlEditor addEmptyParameter:@"a"];
    [urlEditor addEmptyParameter:@"b"];
    
    NSString* url = [urlEditor getFormattedURL];
    
    XCTAssertTrue([template isEqualToString:url], @"compare: \n%@\n%@", template, url);
}

- (void)testComplex
{
    NSString* template = @"localhost?a=1&b=2&c&d";
    
    URLEditor* urlEditor = [[URLEditor alloc] init];
    [urlEditor setServerURL:@"localhost"];
    [urlEditor addParameter:@"a" Value:@"1"];
    [urlEditor addParameter:@"b" Value:@"2"];
    [urlEditor addEmptyParameter:@"c"];
    [urlEditor addEmptyParameter:@"d"];
    
    NSString* url = [urlEditor getFormattedURL];
    
    XCTAssertTrue([template isEqualToString:url], @"compare: \n%@\n%@", template, url);
}

@end
