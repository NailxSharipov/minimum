package com.weakteam.mobile.taxi;

import android.app.Activity;
import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.weakteam.mobile.taxi.data.realm.DataAccess;
import com.weakteam.mobile.taxi.service.ServerSession;
import com.weakteam.mobile.taxi.state.Reachability;

import io.fabric.sdk.android.Fabric;

/**
 *  Created by Nail on 22.03.2015.
 */
public class Minimum extends Application {

    private static Minimum instance;

    public static Minimum getInstance() {
        return instance;
    }

    private Activity currentActivity = null;

    public Activity getCurrentActivity(){
        return currentActivity;
    }
    public void setCurrentActivity(Activity mCurrentActivity){
        this.currentActivity = mCurrentActivity;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        DataAccess.getInstance().open(this);
        Reachability.registerReachability(this.getApplicationContext());
        ServerSession.getInstance().init(this);
        instance = this;
    }

    @Override
    public void onTerminate() {
        DataAccess.getInstance().close(this);
        instance = null;
        super.onTerminate();
    }

    public boolean isOnTop(Activity activity) {
        return activity != null && currentActivity != null && currentActivity == activity;
    }

}