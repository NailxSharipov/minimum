package com.weakteam.mobile.taxi.data;

/**
 *  Created by Nail on 13.01.2015.
 */
public class Brand {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
