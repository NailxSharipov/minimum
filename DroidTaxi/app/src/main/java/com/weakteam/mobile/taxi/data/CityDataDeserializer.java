package com.weakteam.mobile.taxi.data;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.weakteam.mobile.taxi.data.model.CityData;

import java.lang.reflect.Type;

/**
 *  Created by Nail on 27.01.2015.
 */
public class CityDataDeserializer implements JsonDeserializer<CityData> {

    @Override
    public CityData deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        final CityData cityData = new CityData();
        final JsonObject cityObject = jsonElement.getAsJsonObject();

        final String cityId = cityObject.get("town_id").getAsString();
        final String title = cityObject.get("title").getAsString();
        final String brandId = cityObject.get("id").getAsString();
        final String brand = cityObject.get("name").getAsString();
        final String url = cityObject.get("url").getAsString();
        final String phonePrefix = cityObject.get("telpref").getAsString();
        String money = cityObject.get("money").getAsString();
        if (money.equalsIgnoreCase("{rubles}")) {
            money = "рублей";
        }

        cityData.setCityId(cityId);
        cityData.setTitle(title);
        cityData.setBrandId(brandId);
        cityData.setBrand(brand);
        cityData.setUrl(url);
        cityData.setPhonePrefix(phonePrefix);
        cityData.setMoney(money);

        return cityData;
    }
}
