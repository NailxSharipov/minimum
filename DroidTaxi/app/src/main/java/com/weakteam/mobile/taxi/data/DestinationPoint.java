package com.weakteam.mobile.taxi.data;

/**
 *  Created by Nail on 14.12.2014.
 */
public class DestinationPoint {

    private Integer id;

    private String action;
    private boolean isFrom;

    private String objectId; // in our database

    private Location.Type type;
    private String street;
    private String home;
    private String porch;

    public DestinationPoint(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getText() {
        String text = this.getObjectAddress();
        if (text == null) {
            return action;
        } else {
            return text;
        }
    }

    public String getObjectAddress() {
        if (street != null) {
            if (home == null) {
                home = "";
            }
            //return Location.replaceReservedWords(street) + " " + home;
            return street + " " + home;
        } else {
            return null;
        }
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isFrom() {
        return isFrom;
    }

    public void setFrom(boolean isFrom) {
        this.isFrom = isFrom;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Location.Type getType() {
        return type;
    }

    public void setType(Location.Type type) {
        this.type = type;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getPorch() {
        return porch;
    }

    public void setPorch(String porch) {
        this.porch = porch;
    }

    public void clear() {
        this.objectId = null;
        this.street = null;
        this.home = null;
        this.porch = null;
    }

    public void setWithSearchPlaceResult(SearchPlaceResult result) {

        this.street = result.getName();

        this.type = result.getLocationType();
        this.objectId = result.getObjectId();
        this.home = result.getHome();
    }


    public void setWithPoint(DestinationPoint point) {
        this.street = point.street;
        this.type = point.type;
        this.objectId = point.objectId;
        this.home = point.home;
    }

    public boolean isEmpty() {
        return (objectId == null && street == null);
    }

    public boolean isEqual(DestinationPoint destinationPoint) {

        if (objectId != null && destinationPoint.objectId != null) {
            if (objectId.compareToIgnoreCase(destinationPoint.objectId) != 0) {
                return false;
            }
        }

        if (type == null && destinationPoint.type != null ) {
            return false;
        }

        if (street != null && destinationPoint.street != null) {
            if (street.compareToIgnoreCase(destinationPoint.street) != 0) {
                return false;
            }
        }

        if (home != null && destinationPoint.home != null) {
            if (home.compareToIgnoreCase(destinationPoint.home) != 0) {
                return false;
            }
        }


        return true;
    }

}
