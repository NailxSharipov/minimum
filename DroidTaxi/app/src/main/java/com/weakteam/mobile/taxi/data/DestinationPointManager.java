package com.weakteam.mobile.taxi.data;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Nail on 14.12.2014.
 */
public class DestinationPointManager {

    private static DestinationPointManager instance;
    private Integer counter;

    public static DestinationPointManager getInstance() {
        if (instance == null) {
            instance = new DestinationPointManager();
            instance.addPoint();
            instance.addPoint();
        }
        return instance;
    }

    private DestinationPoint activePoint;

    private List<DestinationPoint> points;

    public List<DestinationPoint> getPoints() {
        return points;
    }

    public DestinationPoint getActivePoint() {
        return activePoint;
    }

    public void setActivePoint(DestinationPoint activePoint) {
        this.activePoint = activePoint;
    }

    private DestinationPointManager() {
        counter = 0;
        points = new ArrayList<>();
    }

    public DestinationPoint addPoint() {
        final DestinationPoint point = new DestinationPoint(counter);
        this.points.add(point);
        if (this.points.size() == 1) {
            point.setAction("Откуда");
            point.setFrom(true);
        } else {
            point.setAction("Куда");
            point.setFrom(false);
        }
        counter++;
        return point;
    }

    public int getOrderIndex(final DestinationPoint point) {
        int id = point.getId();
        int n = points.size();
        for (int i = 0; i < n; i++) {
            if (points.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }


    public void removePlace(DestinationPoint point) {
        int index = this.getOrderIndex(point);
        if (index >= 2) {
            this.points.remove(index);
        } else if (index == 1) {
            if (points.size() > 2) {
                points.remove(index);
            } else {
                point.clear();
            }
        } else {
            point.clear();
        }
    }

    public int getSize() {
        int count = 0;
        for (DestinationPoint point : points) {
            if (point.getObjectId() != null && point.getObjectId().length() > 0) {
                count++;
            }
        }
        return count;
    }

    public void clearAll() {
        int n = points.size();
        for (int i = n - 1; i >= 0; i--) {
            if (i >= 2) {
                points.remove(i);
            } else {
                points.get(i).clear();
            }
        }
    }

    public void setWithHistory(History history) {
        DestinationPointManager.getInstance().clearAll();
        List<DestinationPoint> pointList = history.getOrder().getPoints();
        int n = pointList.size();
        for (int i = 0; i < n; i++) {
            if (i < 2) {
                this.points.get(i).setWithPoint(pointList.get(i));
            } else {
                final DestinationPoint point = this.addPoint();
                point.setWithPoint(pointList.get(i));
            }
        }
    }


}
