package com.weakteam.mobile.taxi.data;

import com.weakteam.mobile.taxi.util.SimpleLog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *  Created by Nail on 18.01.2015.
 */
public class History {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy kk:mm", new Locale("ru"));

    private Date datetime;
    private Order order;

    public void setDate(String date) {
        try {
            this.datetime = dateFormat.parse(date);
        } catch (Exception ex) {
            SimpleLog.info(ex.toString());
        }
    }

    public Date getDatetime() {
        return datetime;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public boolean isEqualTo(History second)
    {
        if (order.getPoints().size() > 1 && second.order.getPoints().size() > 1) {
            DestinationPoint pointA = order.getPoints().get(0);
            DestinationPoint pointB = order.getPoints().get(1);
            DestinationPoint pointC = second.order.getPoints().get(0);
            DestinationPoint pointD = second.order.getPoints().get(1);

            return pointA.isEqual(pointC) && pointB.isEqual(pointD);
        } else {
            return false;
        }
    }


}
