package com.weakteam.mobile.taxi.data;

import android.app.Activity;

import com.weakteam.mobile.taxi.service.ITaskEvent;
import com.weakteam.mobile.taxi.service.ServerSession;
import com.weakteam.mobile.taxi.service.task.GetHistory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *  Created by Nail on 18.01.2015.
 */
public class HistoryManager {

    public interface IHistoryUpdate {
        void onHistoryUpdate();
    }

    private static HistoryManager instance;
    private List<History> historyList;
    private List<DestinationPoint> pointList;
    private Set<IHistoryUpdate> historyListeners;

    public List<History> getHistoryList() {
        return historyList;
    }

    public List<SearchPlaceResult> getPointList(String key) {
        if (pointList!= null && pointList.size() > 0) {
            List<SearchPlaceResult> result = new ArrayList<>();
            for (DestinationPoint point : pointList) {
                final String street = point.getStreet();
                if (street.toLowerCase().contains(key.toLowerCase())) {
                    SearchPlaceResult searchPlace = new SearchPlaceResult();
                    searchPlace.setName(street);
                    searchPlace.setLocationType(point.getType());
                    searchPlace.setObjectId(point.getObjectId());
                    searchPlace.setHome(point.getHome());
                    result.add(searchPlace);
                }
            }
            return result;
        } else {
            return null;
        }

    }

    public List<DestinationPoint> getPointList() {
        return pointList;
    }

    public static HistoryManager getInstance() {
        if (instance == null) {
            instance = new HistoryManager();
        }
        return instance;
    }

    private HistoryManager() {
        historyListeners = new HashSet<>();
    }

    public void addListener(IHistoryUpdate listener) {
        historyListeners.add(listener);
    }

    public void removeListener(IHistoryUpdate listener) {
        historyListeners.remove(listener);
    }

    public void loadHistory() {
        final GetHistory task = ServerSession.getInstance().buildAsyncRequest(GetHistory.class);

        task.setOnPostExecute(new ITaskEvent() {
            @Override
            public void onAction() {
                final List<History> historyList = task.getList();
                fillHistory(historyList);
                for(IHistoryUpdate listener : historyListeners) {
                    if (listener != null) {
                        listener.onHistoryUpdate();
                    }
                }
            }
        });
        task.send();
    }

    private void fillHistory(List<History> histories) {

        if (histories != null) {
            this.historyList = histories;

            Collections.sort(this.historyList, new Comparator<History>() {
                @Override
                public int compare(History h0, History h1) {
                    return ((h1.getDatetime()).compareTo(h0.getDatetime()));
                }
            });

            // delete repeats
            {
                int n = this.historyList.size();
                for(int i = n - 1; i > 0; i--) {
                    for(int j = i - 1; j >= 0; j--) {
                        if (this.historyList.get(i).isEqualTo(this.historyList.get(j))) {
                            this.historyList.remove(i);
                            break;
                        }
                    }
                }
            }

            final List<PointDataTime> allPoints = new ArrayList<>();

            for (History history : this.historyList) {
                for (DestinationPoint point : history.getOrder().getPoints()) {
                    final PointDataTime pointTime = new PointDataTime();
                    pointTime.setTime(history.getDatetime());
                    pointTime.setPoint(point);
                    allPoints.add(pointTime);
                }
            }

            Collections.sort(allPoints, new Comparator<PointDataTime>() {
                @Override
                public int compare(PointDataTime pdt0, PointDataTime pdt1) {
                    return ((pdt1.getTime()).compareTo(pdt0.getTime()));
                }
            });



            final List<DestinationPoint> uniquePoints = new ArrayList<>();
            for (PointDataTime pointTime : allPoints) {

                final String idPoint = pointTime.getPoint().getObjectAddress();
                boolean isUnique = true;
                for (DestinationPoint point : uniquePoints) {
                    if (point.getObjectAddress().compareToIgnoreCase(idPoint) == 0) {
                        isUnique = false;
                        break;
                    }
                }

                if (isUnique) {
                    uniquePoints.add(pointTime.getPoint());
                }
            }

            this.pointList = uniquePoints;
        }
    }

}
