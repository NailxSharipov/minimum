package com.weakteam.mobile.taxi.data;

/**
 *  Created by Nail on 21.12.2014.
 */
public class Location {
    // 1-улицы, 2- остановки, 3-прочие объекты
    public enum Type {
        None,
        Street,
        Station,
        Other
    }

    public static String getTextCode(Type type) {
        return Integer.toString(type.ordinal());
    }

    public static Type setWith(String text) {
        int order = Integer.parseInt(text);
        return Type.values()[order];
    }

    public static String replaceReservedWords (String original) {

        String shortName = new String(original);

        shortName = shortName.trim()
                .replace("улица", "")
                .replace("площадь", "")
                .replace("проспект", "")
                .replace("набережная", "")
                .replace("бульвар", "")
                .replace("просек", "")
                .replace("переулок", "")
                .replace("шоссе", "")
                .replace("аллея", "")
                .replace("тупик", "")
                .replace("холм", "")
                .replace("проезд", "")
                .replace("район", "")
                .replace("метро", "")
                .trim();

        return shortName;
    }

}
