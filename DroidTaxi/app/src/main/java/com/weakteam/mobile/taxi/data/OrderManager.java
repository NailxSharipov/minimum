package com.weakteam.mobile.taxi.data;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Nail on 18.01.2015.
 */
public class OrderManager {

    private static OrderManager instance;

    private List<String> canceled;

    public static OrderManager getInstance() {
        if (instance == null) {
            instance = new OrderManager();
        }
        return instance;
    }

    public void cancelOrderId(String orderId) {
        if (canceled == null) {
            canceled = new ArrayList<>(1);
        }
        this.canceled.add(orderId);
    }

    public boolean isCanceledOrder(String orderId) {
        if (canceled != null) {
            for (String objId : canceled) {
                if (objId.compareToIgnoreCase(orderId) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

}
