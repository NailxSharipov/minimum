package com.weakteam.mobile.taxi.data;

import java.util.Date;

/**
 *  Created by Nail on 18.01.2015.
 */
public class PointDataTime {

    private DestinationPoint point;
    private Date time;

    public DestinationPoint getPoint() {
        return point;
    }

    public void setPoint(DestinationPoint point) {
        this.point = point;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

}
