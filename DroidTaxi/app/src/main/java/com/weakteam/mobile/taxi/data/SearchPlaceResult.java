package com.weakteam.mobile.taxi.data;

/**
 *  Created by Nail on 21.12.2014.
 */
public class SearchPlaceResult {

    private String objectId;
    private Location.Type locationType;
    private String name;
    private String home;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Location.Type getLocationType() {
        return locationType;
    }

    public void setLocationType(Location.Type locationType) {
        this.locationType = locationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }
}
