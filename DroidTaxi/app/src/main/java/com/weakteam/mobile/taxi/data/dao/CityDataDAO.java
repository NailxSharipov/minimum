package com.weakteam.mobile.taxi.data.dao;

import com.weakteam.mobile.taxi.data.model.CityData;

import java.util.List;

/**
 * Created by Nail on 12.03.2015.
 */
public interface CityDataDAO {

    void create(List<CityData> cityDataList);

    void create(CityData cityData);

    void update(CityData cityData);

    CityData getById(String id);

    List<CityData> getAll();

}
