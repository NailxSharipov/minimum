package com.weakteam.mobile.taxi.data.dao;

import android.content.Context;

import com.weakteam.mobile.taxi.data.model.CityData;
import com.weakteam.mobile.taxi.data.model.CityDataModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Nail on 12.03.2015.
 */
public class CityDataDAOImpl implements CityDataDAO {

    private Context context;

    public CityDataDAOImpl(Context context) {
        this.context = context;
    }

    @Override
    public void create(List<CityData> cityDataList) {
        if (context != null) {
            final Realm realm = Realm.getInstance(context);
            realm.beginTransaction();

            List<CityDataModel> models = new ArrayList<>(cityDataList.size());
            for (CityData cityData : cityDataList) {
                models.add(cityData.getModel(realm));
            }

            realm.copyToRealmOrUpdate(models);
            realm.commitTransaction();
        }
    }

    @Override
    public void create(CityData cityData) {
        if (context != null) {
            final Realm realm = Realm.getInstance(context);
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(cityData.getModel(realm));
            realm.commitTransaction();
        }
    }

    @Override
    public void update(CityData cityData) {
        if (context != null) {
            final Realm realm = Realm.getInstance(context);
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(cityData.getModel(realm));
            realm.commitTransaction();
        }
    }

    @Override
    public CityData getById(String id) {
        if (context != null) {
            final Realm realm = Realm.getInstance(context);
            final CityDataModel model = realm.where(CityDataModel.class).equalTo("cityId", id).findFirst();

            return new CityData(model);
        } else {
            return null;
        }
    }

    @Override
    public List<CityData> getAll() {
        if (context != null) {
            final Realm realm = Realm.getInstance(context);
            final RealmResults<CityDataModel> models = realm.where(CityDataModel.class).findAll();

            List<CityData> cityList = new ArrayList<>(models.size());
            for (CityDataModel model : models) {
                cityList.add(new CityData(model));
            }

            return cityList;
        } else {
            return null;
        }
    }
}
