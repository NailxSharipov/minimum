package com.weakteam.mobile.taxi.data.model;

import java.util.Map;

import io.realm.Realm;

/**
 * Created by Nail Sharipov (nailxsharipov@gmail.com) on 11.11.2014.
 */
public class CityData {

    private String cityId;
    private String title;
    private String brand;
    private String brandId;
    private String url;
    private String phonePrefix;
    private String money;

    public CityData(){}

    public CityData(CityDataModel model){
        this.cityId = model.getCityId();
        this.title = model.getTitle();
        this.brand = model.getBrand();
        this.brandId = model.getBrandId();
        this.url = model.getUrl();
        this.phonePrefix = model.getPhonePrefix();
        this.money = model.getMoney();
    }

    public CityDataModel getModel(final Realm realm) {
        CityDataModel model = null;
        if (cityId != null) {
            model = realm.where(CityDataModel.class).equalTo("cityId", this.cityId).findFirst();
            if (model == null) {
                model = realm.createObject(CityDataModel.class);
            }
        }
        assert model != null;
        model.setCityId(this.cityId);
        model.setTitle(this.title);
        model.setBrand(this.brand);
        model.setBrandId(this.brandId);
        model.setUrl(this.url);
        model.setPhonePrefix(this.phonePrefix);
        model.setMoney(this.money);
        return model;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}


