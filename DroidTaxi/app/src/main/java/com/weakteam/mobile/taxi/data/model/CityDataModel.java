package com.weakteam.mobile.taxi.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 *  Created by Nail on 12.03.2015.
 */
public class CityDataModel extends RealmObject {

    @PrimaryKey
    private String cityId;
    private String title;
    private String brand;
    private String brandId;
    private String url;
    private String phonePrefix;
    private String money;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }

    public void setPhonePrefix(String phonePrefix) {
        this.phonePrefix = phonePrefix;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}