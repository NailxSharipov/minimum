package com.weakteam.mobile.taxi.data.realm;

import android.content.Context;
import android.util.Log;

import com.weakteam.mobile.taxi.data.dao.CityDataDAO;
import com.weakteam.mobile.taxi.data.dao.CityDataDAOImpl;


/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs and Services used by the other classes.
 */
public class DataAccess {

    public static final String TAG = DataAccess.class.getSimpleName();

    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME ="sbis.db";

    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;

    private static DataAccess instance;

    // the DAO object we use to access the Dialog table
    private CityDataDAO cityDataDAO = null;

    public static DataAccess getInstance() {
        if (instance == null) {
            instance = new DataAccess();
        }
        return instance;
    }

    private Context context;

    private DataAccess() {

    }

    public void open(Context context) {
        this.context = context;
        this.cityDataDAO = null;
    }

    /**
     * Returns the Database Access Object (DAO) for our Dialog class. It will create it or just give the cached
     * value.
     */
    public CityDataDAO getCityDataDAO() {
        if(cityDataDAO == null) {
            cityDataDAO = new CityDataDAOImpl(this.context);
        }
        return cityDataDAO;
    }
    /**
     * Close the database connections and clear any cached DAOs and Services.
     */
    public void close(Context context) {
        if (context == this.context) {
            instance = null;
            this.context = null;
            this.cityDataDAO = null;
        }
    }
}