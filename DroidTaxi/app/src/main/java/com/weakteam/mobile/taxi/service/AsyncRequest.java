package com.weakteam.mobile.taxi.service;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.Toast;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.state.Reachability;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;


/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 21.10.2014.
 */

public abstract class AsyncRequest extends AsyncTask<Void, Integer, Void> {

    protected static final int CONNECTION_TIME_OUT = 20 * 1000; // 10 sec

    protected boolean showNoConnectionToast = false;

    protected String baseUrl;

    private ITaskEvent onPostExecute;

    protected String error;

    public void setOnPostExecute(ITaskEvent onPostExecute) {
        this.onPostExecute = onPostExecute;
    }

    void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    private boolean isNetworkAvailable() {
        final Activity activity = Minimum.getInstance().getCurrentActivity();
        if (activity != null) {
            final boolean isNetworkAvailable = Reachability.reachability.isReachable();
            if (!isNetworkAvailable && showNoConnectionToast) {
                Toast.makeText(activity, activity.getResources().getString(R.string.activity_initialize_no_connection), Toast.LENGTH_SHORT).show();
            }
            return isNetworkAvailable;
        } else {
            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.error = null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        final Activity activity = Minimum.getInstance().getCurrentActivity();

        if (activity != null) {
            if (this.onPostExecute != null) {
                this.onPostExecute.onAction();
            }

            if (this.error != null) {
                Toast.makeText(activity, this.error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean send() {
        if (this.isNetworkAvailable()) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
                this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                this.execute();
            }
            return true;
        } else {
            return false;
        }
    }

    protected String readStream(final HttpURLConnection connection) {
        String content = null;
        try {
            InputStream inputStream = connection.getInputStream();
            if (inputStream != null) {
                //final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "windows-1251"));
                final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "utf8"));
                final StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                br.close();
                content = sb.toString();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return content;
    }
}

