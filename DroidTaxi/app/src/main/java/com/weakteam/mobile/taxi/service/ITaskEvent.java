package com.weakteam.mobile.taxi.service;

/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 21.10.2014.
 */
public interface ITaskEvent {

    void onAction();

}
