package com.weakteam.mobile.taxi.service;


import com.weakteam.mobile.taxi.data.model.CityData;

/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 11.11.2014.
 */
public class ServerData {

    private String localServer;

    private static ServerData instance;

    public static ServerData getInstance() {
        if (instance == null) {
            instance = new ServerData();
        }
        return instance;
    }

    public String getLocalServerUrl() {
        return localServer;
    }

    public String getBaseServerUrl() {
        return "http://taxi-leader.ru/am.php";
    }

    public void setCityData (CityData data) {
        if (data != null) {
            this.localServer = "http://" + data.getUrl() + "/am.php";
        }
    }
}
