package com.weakteam.mobile.taxi.service;

import android.content.Context;

import com.weakteam.mobile.taxi.state.AppSettings;

/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 21.10.2014.
 */

public class ServerSession {

    private static ServerSession instance;

    private String serverBaseUrl;

    public static ServerSession getInstance() {
        if (instance == null) {
            instance = new ServerSession();
        }
        return instance;
    }

    private ServerSession() {}

    public void init(Context context) {
        AppSettings.getInstance().load(context);
        this.setServerBaseUrl(AppSettings.getInstance().getServerBaseUrl());
    }

    private void setServerBaseUrl(String serverBaseUrl) {
        this.serverBaseUrl = serverBaseUrl;
    }

    public <T extends AsyncRequest> T buildAsyncRequest(Class<T> taskClass) {
        T asyncRequest = null;
        try {
            asyncRequest = taskClass.newInstance();
            asyncRequest.setBaseUrl(this.serverBaseUrl);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return asyncRequest;
    }

}
