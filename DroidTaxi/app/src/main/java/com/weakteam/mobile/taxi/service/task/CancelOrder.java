package com.weakteam.mobile.taxi.service.task;

import com.weakteam.mobile.taxi.service.AsyncRequest;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.util.SimpleLog;
import com.weakteam.mobile.taxi.util.UrlBuilder;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 *  Created by Nail on 21.01.2015.
 */
public class CancelOrder extends AsyncRequest {

    private boolean result;
    private String orderId;

    {
        this.showNoConnectionToast = true;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public boolean getResult() {
        return result;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SimpleLog.info("CancelOrder");
        final String baseURL = ServerData.getInstance().getLocalServerUrl();
        if (baseURL != null) {
            final String phoneNumber = UserSettings.getInstance().getLogin();
            final String password = UserSettings.getInstance().getPassword();

            final URL url = new UrlBuilder().setServerURL(baseURL)
                    .addParameter("t", "5")
                    .addParameter("l", phoneNumber)
                    .addParameter("p", password)
                    .addParameter("z", orderId)
                    .addParameter("enc", "utf8")
                    .getURL();

            try {
                final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setConnectTimeout(CONNECTION_TIME_OUT);
                connection.setReadTimeout(CONNECTION_TIME_OUT);

                // get response
                int status = connection.getResponseCode();

                switch (status) {
                    case HttpURLConnection.HTTP_OK:
                        this.result = true;
                        break;
                    default:
                        SimpleLog.info("status code: " + status);
                }

            } catch (Exception ex) {
                this.result = false;
                ex.printStackTrace();
            }
        }
        return null;
    }
}
