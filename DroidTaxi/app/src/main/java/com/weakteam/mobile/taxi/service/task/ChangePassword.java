package com.weakteam.mobile.taxi.service.task;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.service.AsyncRequest;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.util.ResponseParser;
import com.weakteam.mobile.taxi.util.SimpleLog;
import com.weakteam.mobile.taxi.util.UrlBuilder;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Nail on 03.02.2015.
 */
public class ChangePassword extends AsyncRequest {

    private String newPassword;
    private String message;
    private Integer code;

    {
        this.showNoConnectionToast = true;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SimpleLog.info("ChangePassword");
        final String baseURL = ServerData.getInstance().getLocalServerUrl();
        if (baseURL != null) {
            final String phoneNumber = UserSettings.getInstance().getLogin();
            final String password = UserSettings.getInstance().getPassword();

            final URL url = new UrlBuilder().setServerURL(baseURL)
                    .addParameter("t", "passw")
                    .addParameter("change_pass", "1")
                    .addParameter("pass", newPassword)
                    .addParameter("l", phoneNumber)
                    .addParameter("p", password)
                    .addParameter("enc", "utf8")
                    .getURL();

            try {
                final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setConnectTimeout(CONNECTION_TIME_OUT);
                connection.setReadTimeout(CONNECTION_TIME_OUT);

                // get response
                int status = connection.getResponseCode();

                switch (status) {
                    case HttpURLConnection.HTTP_OK:
                        final String response = this.readStream(connection);
                        if (ResponseParser.isHtml(response)) {
                            error = Minimum.getInstance().getString(R.string.not_available);
                        } else {
                            int length = response.length();
                            if (length > 2) {
                                this.code = Integer.parseInt(response.substring(0, 1));
                                this.message = response.substring(2, length);
                                this.message = this.message.replaceAll("<br>", "\n");
                            }
                        }
                        break;
                    default:
                        SimpleLog.info("status code: " + status);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
}