package com.weakteam.mobile.taxi.service.task;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.weakteam.mobile.taxi.service.AsyncRequest;
import com.weakteam.mobile.taxi.util.SimpleLog;
import com.weakteam.mobile.taxi.util.UrlBuilder;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import java.lang.reflect.Type;

/**
 * Created by Nail on 25.01.2015.
 */
public class GeoLocationOSMNominatim  extends AsyncRequest {

    private GeoLocation geoLocation;
    private Location position;
    private LatLng latlng;

    public void setPosition(Location position) {
        this.position = position;
    }

    public void setLatLng(LatLng latlng) {
        this.latlng = latlng;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SimpleLog.info("GeoLocationOSMNominatim");

        double lat = 0.0f;
        double lng = 0.0f;
        if (this.position != null) {
            lat = position.getLatitude();
            lng = position.getLongitude();
        } else if (latlng != null) {
            lat = latlng.latitude;
            lng = latlng.longitude;
        }

        final URL url = new UrlBuilder().setServerURL("http://nominatim.openstreetmap.org/reverse")
                .addParameter("format", "json")
                .addParameter("lat", Double.toString(lat))
                .addParameter("lon", Double.toString(lng))
                .getURL();
        try {
            final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.setConnectTimeout(CONNECTION_TIME_OUT);
            connection.setReadTimeout(CONNECTION_TIME_OUT);

            // get response
            int status = connection.getResponseCode();

            switch (status) {
                case HttpURLConnection.HTTP_OK:
                    final String response = this.readStream(connection);
                    if (response.length() > 0) {
                        try {
                            Gson gson = new GsonBuilder().registerTypeAdapter(GeoLocation.class, new GeoLocationDeserializer()).create();
                            this.geoLocation = gson.fromJson(response, GeoLocation.class);
                        } catch (Exception ex) {
                            SimpleLog.info("Error parse GeoLocationOSMNominatim");
                        }
                        break;
                    }
                    break;
                default:
                    SimpleLog.info("status code: " + status);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public class GeoLocation {
        private String number;
        private String street;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }
    }

    public class GeoLocationDeserializer  implements JsonDeserializer<GeoLocation> {
        @Override
        public GeoLocation deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            GeoLocation geoLocation = null;

            JsonObject jsonObj = jsonElement.getAsJsonObject();

            final JsonObject address = jsonObj.get("address").getAsJsonObject();

            final String houseNumber = address.get("house_number").getAsString();
            final String road = address.get("road").getAsString();

            if (houseNumber != null && road != null) {
                geoLocation = new GeoLocation();
                geoLocation.setStreet(com.weakteam.mobile.taxi.data.Location.replaceReservedWords(road));
                geoLocation.setNumber(houseNumber);
            }

            return geoLocation;
        }
    }
}