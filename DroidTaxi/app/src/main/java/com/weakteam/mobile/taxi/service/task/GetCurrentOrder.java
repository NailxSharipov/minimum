package com.weakteam.mobile.taxi.service.task;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.Order;
import com.weakteam.mobile.taxi.service.AsyncRequest;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.util.ResponseParser;
import com.weakteam.mobile.taxi.util.SimpleLog;
import com.weakteam.mobile.taxi.util.UrlBuilder;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  Created by Nail on 18.01.2015.
 */
public class GetCurrentOrder extends AsyncRequest {

    private List<Order> orderList;

    public List<Order> getOrderList() {
        return orderList;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SimpleLog.info("GetCurrentOrder");
        final String baseURL = ServerData.getInstance().getLocalServerUrl();
        if (baseURL != null) {
            final String phoneNumber = UserSettings.getInstance().getLogin();
            final String password = UserSettings.getInstance().getPassword();

            final URL url = new UrlBuilder().setServerURL(baseURL)
                    .addParameter("t", "42")
                    .addParameter("l", phoneNumber)
                    .addParameter("p", password)
                    .addParameter("version", "4.0.3")
                    .addParameter("enc", "utf8")
                    .getURL();

            try {
                final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setConnectTimeout(CONNECTION_TIME_OUT);
                connection.setReadTimeout(CONNECTION_TIME_OUT);

                // get response
                int status = connection.getResponseCode();

                switch (status) {
                    case HttpURLConnection.HTTP_OK:
                        final String response = this.readStream(connection);
                        if (ResponseParser.isHtml(response)) {
                            error = Minimum.getInstance().getString(R.string.not_available);
                        } else {
                            this.orderList = this.parseOrders(response);
                        }
                        break;
                    default:
                        SimpleLog.info("status code: " + status);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private List<Order> parseOrders(String text) {
        /*
        String test = "result=2\n" +
        "id=60348636|from=Ленина 1 подъезд 2|to=Сагита Агиша 34ffwerw|price=105|status=Клиент не поехал серебристая/дэу-нексия/943|driverphone=|status_code=7\n"+
        "id=60348633|from=Ленина 2 подъезд 2|to=Сагита Агиша 34ffwerw|price=105|status=Клиент не поехал серебристая/дэу-нексия/943|driverphone=|status_code=2\n";
        String[] lines = test.split("\n");
        */
        final String[] lines = text.split("\n");
        final String result = this.getValue(lines[0]).trim();

        int resCount = 0;

        try {
            resCount = Integer.parseInt(result);
        } catch(Exception ex) {
            SimpleLog.info("GetCurrentOrder could not parse integer");
        }

        List<Order> orders = null;

        if (resCount > 0) {
            orders = new ArrayList<>();
            for(int i = 1; i < lines.length; i++) {
                final String line = lines[i];
                final Order order = this.parseOrder(line);
                if (order != null) {
                    orders.add(order);
                }
            }
        }

        return orders;
    }

    private Order parseOrder(String text) {
        Order order = null;
        if (text != null && text.length() > 1) {
            order = new Order();
            Map<String, String> map = this.collectData(text.split("\\|"));

            if (map != null) {
                order.setOrderId(map.get("id"));
                order.setFromPoint(map.get("from"));
                order.setToPoint(map.get("to"));
                order.setPrice(map.get("price"));
                order.setPhone(map.get("phone"));

                String status = map.get("status");

                String[] statusBlock = status.split("/");

                int count = statusBlock.length;

                if (count > 0) {
                    if (count == 1) {
                        order.setStatus(statusBlock[0]);
                    } else {
                        String[] statusBlockBlock = statusBlock[0].split(" ");
                        int size = statusBlockBlock.length;
                        order.setColor(statusBlockBlock[size - 1]);

                        StringBuilder statusBuilder = new StringBuilder();
                        for (int i = 0; i < statusBlockBlock.length - 1; i++) {
                            statusBuilder.append(statusBlockBlock[i]).append(" ");
                        }
                        order.setStatus(statusBuilder.toString());
                    }
                }
                if (count > 1) {
                    order.setCar(statusBlock[1]);
                }
                if (count > 2) {
                    order.setCarNumber(statusBlock[2]);
                }

                final String code = map.get("status_code");
                order.setCode(Integer.parseInt(code.trim()));
            }
        }
        return order;
    }


    private String getValue(String text) {
        String[] lines = text.split("=");
        if (lines.length > 1) {
            return  lines[1];
        } else {
            return "";
        }
    }

    private Map<String, String> collectData(String[] data) {
        Map<String, String> map = new HashMap<>();

        for (String block : data) {
            String[] sep = block.split("=");
            if (sep.length > 1) {
                map.put(sep[0], sep[1]);
            }
        }
        return map;
    }
}
