package com.weakteam.mobile.taxi.service.task;


import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.data.History;
import com.weakteam.mobile.taxi.data.Location;
import com.weakteam.mobile.taxi.data.Order;
import com.weakteam.mobile.taxi.service.AsyncRequest;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.util.ResponseParser;
import com.weakteam.mobile.taxi.util.SimpleLog;
import com.weakteam.mobile.taxi.util.UrlBuilder;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  Created by Nail on 18.01.2015.
 */
public class GetHistory extends AsyncRequest {

    private List<History> historyList;

    public List<History> getList() {
        return historyList;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SimpleLog.info("GetHistory");
        final String baseURL = ServerData.getInstance().getLocalServerUrl();
        if (baseURL != null) {
            final String phoneNumber = UserSettings.getInstance().getLogin();
            final String password = UserSettings.getInstance().getPassword();

            final URL url = new UrlBuilder().setServerURL(baseURL)
                    .addParameter("t", "history")
                    .addParameter("l", phoneNumber)
                    .addParameter("p", password)
                    .addParameter("pg", "1")
                    .addParameter("max", "15")
                    .addParameter("version", "2")
                    .addParameter("enc", "utf8")
                    .getURL();

            try {
                final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setConnectTimeout(CONNECTION_TIME_OUT);
                connection.setReadTimeout(CONNECTION_TIME_OUT);

                // get response
                int status = connection.getResponseCode();

                switch (status) {
                    case HttpURLConnection.HTTP_OK:
                        final String response = this.readStream(connection);
                        if (ResponseParser.isHtml(response)) {
                            error = Minimum.getInstance().getString(R.string.not_available);
                        } else {
                            this.historyList = this.parseHistory(response);
                        }
                        break;
                    default:
                        SimpleLog.info("status code: " + status);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private List<History> parseHistory(String text) {
        List<History> histories = null;

        String[] lines = text.split("\n");
        int n = lines.length;

        if (n > 0) {
            String line;
            // result
            line = lines[0];
            if (this.getValue(line).compareToIgnoreCase("1") == 0) {

                histories = new ArrayList<>();

                String[] bloks;
                String tag;
                Map<String, String> values;
                int i = 1;
                while (i < n) {
                    line = lines[i];
                    bloks = line.split("\\|");
                    tag = bloks[0];

                    i++;
                    if (tag.compareToIgnoreCase("order") == 0) { // начало заказа

                        values = this.collectData(bloks);

                        final List<DestinationPoint> pointList = new ArrayList<>();

                        final Order order = new Order();
                        order.setPoints(pointList);
                        order.setOrderId(values.get("id"));

                        final History history = new History();
                        history.setOrder(order);

                        history.setDate(values.get("order_date"));

                        histories.add(history);

                        line = lines[i];
                        bloks = line.split("\\|");
                        tag = bloks[0];

                        while (tag.compareToIgnoreCase("route") == 0) {
                            values = this.collectData(bloks);

                            try {
                                final DestinationPoint point = new DestinationPoint(-1);

                                point.setObjectId(values.get("o"));
                                final String street = values.get("n");
                                point.setStreet(Location.replaceReservedWords(street));


                                String home = values.get("h");
                                if (home != null && home.length() > 0) {
                                    point.setType(Location.Type.Street); // street
                                    String[] homeData = home.split(" ");
                                    point.setHome(homeData[0]);
                                }

                                pointList.add(point);
                            } catch (Exception ex) {
                                SimpleLog.info("parse history " + ex.toString());
                            }

                            i++;
                            if (i < n) {
                                line = lines[i];
                                bloks = line.split("\\|");
                                tag = bloks[0];
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (histories != null && histories.size() == 0) {
            histories = null;
        }
        return histories;
    }

    private String getValue(String text) {
        String[] lines = text.split("=");
        if (lines.length > 1) {
            return  lines[1];
        } else {
            return "";
        }
    }

    private Map<String, String> collectData(String[] data) {
        Map<String, String> map = new HashMap<>();

        for (String block : data) {
            String[] sep = block.split("=");
            if (sep.length > 1) {
                map.put(sep[0], sep[1]);
            }
        }
        return map;
    }

}
