package com.weakteam.mobile.taxi.service.task;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.model.CityData;
import com.weakteam.mobile.taxi.data.CityDataDeserializer;
import com.weakteam.mobile.taxi.service.AsyncRequest;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.util.ResponseParser;
import com.weakteam.mobile.taxi.util.SimpleLog;
import com.weakteam.mobile.taxi.util.UrlBuilder;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

/**
 *  Created by Nail on 27.01.2015.
 */
public class GetMinimumCities  extends AsyncRequest {

    private List<CityData> cityList;

    public List<CityData> getCityList() {
        return cityList;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SimpleLog.info("GetCities");
        final String baseURL = ServerData.getInstance().getBaseServerUrl();
        if (baseURL != null) {

            final URL url = new UrlBuilder().setServerURL(baseURL)
                    .addParameter("t", "brands")
                    .addParameter("a", "all")
                    .addParameter("out", "js")
                    .addParameter("enc", "utf8")
                    .getURL();

            try {
                final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setConnectTimeout(CONNECTION_TIME_OUT);
                connection.setReadTimeout(CONNECTION_TIME_OUT);

                // get response
                int status = connection.getResponseCode();

                switch (status) {
                    case HttpURLConnection.HTTP_OK:
                        final String response = this.readStream(connection);
                        if (ResponseParser.isHtml(response)) {
                            final Context context = Minimum.getInstance();
                            if (context != null) {
                                error = context.getString(R.string.not_available);
                            }
                        } else {
                            Gson gson = new GsonBuilder().registerTypeAdapter(CityData.class, new CityDataDeserializer()).create();
                            final CityData[] cityArray = gson.fromJson(response, CityData[].class);
                            this.cityList = Arrays.asList(cityArray);
                        }
                        break;
                    default:
                        SimpleLog.info("status code: " + status);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
}
