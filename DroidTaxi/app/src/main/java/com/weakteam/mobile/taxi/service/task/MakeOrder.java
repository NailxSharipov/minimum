package com.weakteam.mobile.taxi.service.task;

import android.app.Activity;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.data.DestinationPointManager;
import com.weakteam.mobile.taxi.service.AsyncRequest;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.util.ResponseParser;
import com.weakteam.mobile.taxi.util.SimpleLog;
import com.weakteam.mobile.taxi.util.UrlBuilder;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nail on 13.01.2015.
 */
public class MakeOrder extends AsyncRequest {

    private boolean isOrder;

    private String price;
    private String orderId;

    {
        this.showNoConnectionToast = true;
    }

    public void setOrder(boolean isOrder) {
        this.isOrder = isOrder;
    }

    public String getPrice() {
        return price;
    }

    public String getOrderId() {
        return orderId;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SimpleLog.info("MakeOrder");

        final String baseURL = ServerData.getInstance().getLocalServerUrl();
        if (baseURL != null) {
            final String phoneNumber = UserSettings.getInstance().getLogin();
            final String secondPhone = UserSettings.getInstance().getSecondPhone();
            final String password = UserSettings.getInstance().getPassword();
            final String brand = UserSettings.getInstance().getCityData().getBrandId();
            final String comment;

            final DestinationPointManager manager = DestinationPointManager.getInstance();

            final DestinationPoint from = manager.getPoints().get(0);

            if (from.getPorch() == null || from.getPorch().length() == 0) {
                comment = "";
            } else {
                comment = from.getPorch();
            }

            final UrlBuilder urlBuilder = new UrlBuilder().setServerURL(baseURL);
            urlBuilder.addParameter("t", "order");

            if (isOrder) {
                urlBuilder
                        .addParameter("a", "send")
                        .addParameter("ph", secondPhone)
                        .addParameter("com1", comment);
            } else {
                urlBuilder.addParameter("a", "cost");
            }
            urlBuilder
                    .addParameter("l", phoneNumber)
                    .addParameter("p", password)
                    .addParameter("no", "1")
                    .addParameter("ob1", from.getObjectId())
                    .addParameter("h1", from.getHome())
                    .addParameter("brand", brand)
                    .addParameter("enc", "utf8");


            int count = manager.getSize();
            long cc = count - 1;
            urlBuilder.addParameter("pn", Long.toString(cc));

            List<DestinationPoint> points = manager.getPoints();
            int i = 2;
            for (int j = 1; j < points.size(); j++) {
                final DestinationPoint point = points.get(j);
                if (point != null && point.getObjectId() != null && point.getObjectId().length() > 0) {
                    String ob = "ob" + i;
                    String h = "h" + i;
                    urlBuilder
                            .addParameter(ob, point.getObjectId())
                            .addParameter(h, point.getHome());
                    i++;
                }
            }

            final URL url = urlBuilder.getURL();

            try {
                final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setConnectTimeout(CONNECTION_TIME_OUT);
                connection.setReadTimeout(CONNECTION_TIME_OUT);

                // get response
                int status = connection.getResponseCode();

                switch (status) {
                    case HttpURLConnection.HTTP_OK:
                        final String response = this.readStream(connection);
                        if (ResponseParser.isHtml(response)) {
                            error = Minimum.getInstance().getString(R.string.not_available);
                        } else {
                            Map<String, String> map = this.parseOrder(response);
                            this.price = map.get("price");
                            this.orderId = map.get("orderId");
                        }
                        break;
                    default:
                        SimpleLog.info("status code: " + status);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private Map<String, String> parseOrder(String text) {
        final Map<String, String> map = new HashMap<>();

        final String strCode = text.substring(0, 1);
        final int code = Integer.parseInt(strCode);

        map.put("code", strCode);

        if (code == 1) {
            String[] data = text.split(" ");
            int size = data.length;

            if (size > 1) {
                final String price = data[1];
                map.put("price", price);
            }

            if (size > 2) {
                String orderId = data[2];
                map.put("orderId", orderId);
            }
        }

        return map;
    }

}
