package com.weakteam.mobile.taxi.service.task;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.service.AsyncRequest;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.util.ResponseParser;
import com.weakteam.mobile.taxi.util.SimpleLog;
import com.weakteam.mobile.taxi.util.UrlBuilder;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 *  Created by Nail on 08.02.2015.
 */
public class RemindPassword extends AsyncRequest {

    private boolean bySms;
    private String result;
    private String phoneNumber;

    {
        this.showNoConnectionToast = true;
    }

    public void setBySms(boolean bySms) {
        this.bySms = bySms;
    }

    public String getResult() {
        return result;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SimpleLog.info("RemindPassword");
        final String baseURL = ServerData.getInstance().getBaseServerUrl();
        if (baseURL != null) {
            //final String phoneNumber = UserSettings.getInstance().getLogin();

            final UrlBuilder urlBuilder = new UrlBuilder().setServerURL(baseURL)
                    .addParameter("t", "13")
                    .addParameter("version", "2")
                    .addParameter("l", phoneNumber)
                    .addParameter("enc", "utf8");

            if (!bySms) {
                urlBuilder.addEmptyParameter("callme");
            }

            final URL url = urlBuilder.getURL();

            try {
                final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setConnectTimeout(CONNECTION_TIME_OUT);
                connection.setReadTimeout(CONNECTION_TIME_OUT);

                // get response
                int status = connection.getResponseCode();

                switch (status) {
                    case HttpURLConnection.HTTP_OK:
                        final String response = this.readStream(connection);
                        if (ResponseParser.isHtml(response)) {
                            error = Minimum.getInstance().getString(R.string.not_available);
                        } else {
                            this.result = response;
                            int length = response.length();
                            if (length > 2) {
                                this.result = response.substring(2, length);
                                this.result = this.result.replaceAll("<br>", "\n");
                            }
                        }
                        break;
                    default:
                        SimpleLog.info("status code: " + status);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
}