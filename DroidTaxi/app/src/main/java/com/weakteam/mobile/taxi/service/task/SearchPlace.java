package com.weakteam.mobile.taxi.service.task;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.Location;
import com.weakteam.mobile.taxi.data.SearchPlaceResult;
import com.weakteam.mobile.taxi.service.AsyncRequest;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.util.ResponseParser;
import com.weakteam.mobile.taxi.util.SimpleLog;
import com.weakteam.mobile.taxi.util.UrlBuilder;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Nail on 21.12.2014.
 */
public class SearchPlace extends AsyncRequest {

    private String key;
    private Location.Type type = Location.Type.None;
    private List<SearchPlaceResult> searchPlaceResultList;

    public void setKey(String key) {
        this.key = key;
    }

    public void setType(Location.Type type) {
        this.type = type;
    }

    public List<SearchPlaceResult> getSearchPlaceResultList() {
        return searchPlaceResultList;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SimpleLog.info("SearchPlace");
        final String baseURL = ServerData.getInstance().getLocalServerUrl();
        if (baseURL != null) {
            final String phoneNumber = UserSettings.getInstance().getLogin();
            final String password = UserSettings.getInstance().getPassword();


            final UrlBuilder urlBuilder = new UrlBuilder().setServerURL(baseURL)
                    .addParameter("t", "6")
                    .addParameter("l", phoneNumber)
                    .addParameter("p", password)
                    .addParameter("w", key)
                    .addParameter("pg", "1")
                    .addParameter("lm", "20")
                    .addParameter("enc", "utf8");
            if (this.type != Location.Type.None) {
                urlBuilder.addParameter("f", Location.getTextCode(this.type));
            }
            final URL url = urlBuilder.getURL();

            try {
                final HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(false);
                connection.setConnectTimeout(CONNECTION_TIME_OUT);
                connection.setReadTimeout(CONNECTION_TIME_OUT);

                // get response
                int status = connection.getResponseCode();

                switch (status) {
                    case HttpURLConnection.HTTP_OK:
                        final String response = this.readStream(connection);
                        if (ResponseParser.isHtml(response)) {
                            error = Minimum.getInstance().getString(R.string.not_available);
                        } else {
                            this.searchPlaceResultList = this.getSearchPlacesFromText(response);
                        }
                        break;
                    default:
                        SimpleLog.info("status code: " + status);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private List<SearchPlaceResult> getSearchPlacesFromText(String text) {
        final List<SearchPlaceResult> resultList = new ArrayList<>();

        int n = text.length();

        char separator = '\n';

        int start = 0;
        int end;

        for (int i = 0; i < n; i++) {
            if (i == n - 1 || text.charAt(i) == separator) {
                end = i;
                String block = text.substring(start, end);
                start = end + 1;

                if (block.length() > 2) {
                    final SearchPlaceResult result = this.getPlaceFromText(block);
                    if (result != null) {
                        resultList.add(result);
                    }
                }
            }
        }

        return resultList;
    }

    private SearchPlaceResult getPlaceFromText(String text) {
        int n = text.length();

        char openBracket = '[';
        char closeBracket = ']';

        boolean isOpen = false;
        int start = 0;
        int end;

        for (int i = 0; i < n; i++) {
            char current = text.charAt(i);
            if (isOpen) {
                if (current == openBracket) {
                    start = i + 1;
                    isOpen = false;
                }
            } else {
                if (current == closeBracket) {
                    end = i;
                    isOpen = true;

                    String block = text.substring(start, end + 1);
                    if (block.length() > 5) {
                        String[] data = block.split("\\|");

                        SearchPlaceResult result = new SearchPlaceResult();
                        result.setObjectId(data[1]);
                        result.setLocationType(Location.setWith(data[2]));
                        result.setName(data[3]);

                        return result;
                    }
                }
            }
        }

        return null;
    }

}