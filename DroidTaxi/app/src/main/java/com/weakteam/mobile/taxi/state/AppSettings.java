package com.weakteam.mobile.taxi.state;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.weakteam.mobile.taxi.R;


/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 21.10.2014.
 */
public class AppSettings {

    private static final String USER_DATA = "TAXI_MINIMUM_APP_DATA";

    private static final AppSettings instance = new AppSettings();

    private String serverBaseUrl;

    public static AppSettings getInstance() {
        return instance;
    }

    private AppSettings() {}

    public String getServerBaseUrl() {
        return serverBaseUrl;
    }

    public void load(final Context context) {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);

        final Resources resources = context.getResources();

        final String baseSocket = resources.getString(R.string.server_base_url);
        this.serverBaseUrl = settings.getString("serverBaseUrl", baseSocket);

    }

}
