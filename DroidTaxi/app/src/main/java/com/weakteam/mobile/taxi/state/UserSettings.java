package com.weakteam.mobile.taxi.state;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

import com.weakteam.mobile.taxi.data.model.CityData;
import com.weakteam.mobile.taxi.data.realm.DataAccess;
import com.weakteam.mobile.taxi.util.SimpleLog;

/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 30.10.2014.
 */
public class UserSettings {

    private static final String USER_DATA = "TAXI_MINIMUM_USER_DATA";

    private static final UserSettings instance = new UserSettings();

    private String cityId;
    private String login;
    private String password;
    private String secondPhone;
    private CityData cityData;

    private boolean exit = false;

    public static UserSettings getInstance() {
        return instance;
    }

    private UserSettings() {}

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecondPhone() {
        if (secondPhone == null) {
            secondPhone = "";
        }
        return secondPhone;
    }
    public void setSecondPhone(String secondPhone) {
        this.secondPhone = secondPhone;
    }
    public void save(Context context) {
        SharedPreferences settings = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        if (this.login != null) {
            editor.putString("login", this.login);
        }
        if (this.password != null) {
            editor.putString("password", this.password);
        }
        if (this.secondPhone != null) {
            editor.putString("secondPhone", this.secondPhone);
        }
        if (this.cityId != null) {
            editor.putString("cityId", this.cityId);
        }

        editor.apply();
    }

    public void load(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        this.login = settings.getString("login", "");
        this.secondPhone = settings.getString("secondPhone", "");
        this.password = settings.getString("password", "");
        this.cityId  = settings.getString("cityId", null);

        if (login.length() == 0) {
            try {
                TelephonyManager tMgr = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
                String phoneNumber = tMgr.getLine1Number();
                if (phoneNumber != null) {
                    phoneNumber = phoneNumber.replace("+7", "");
                    int count = phoneNumber.length();
                    if (count > 10) {
                        phoneNumber = phoneNumber.substring(count - 10, count);
                    }
                }
                this.login = phoneNumber;
            } catch(Exception ex) {
                SimpleLog.info(ex.toString());
            }
        }
    }

    public CityData getCityData() {
        if (this.cityData == null && cityId != null) {
            this.cityData = DataAccess.getInstance().getCityDataDAO().getById(cityId);
        }
        return this.cityData;
    }

    public void setCityData(CityData cityData) {
        if (cityData != null) {
            this.cityId = cityData.getCityId();
            this.cityData = cityData;
        }
    }
}
