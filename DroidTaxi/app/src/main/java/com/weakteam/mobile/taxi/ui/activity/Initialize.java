package com.weakteam.mobile.taxi.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.model.CityData;
import com.weakteam.mobile.taxi.data.realm.DataAccess;
import com.weakteam.mobile.taxi.service.ITaskEvent;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.service.ServerSession;
import com.weakteam.mobile.taxi.service.task.Authenticate;
import com.weakteam.mobile.taxi.service.task.GetMinimumCities;
import com.weakteam.mobile.taxi.state.UserSettings;

import java.util.List;

/**
 *  Created by Nail on 09.03.2015.
 */
public class Initialize extends Activity {

    private ProgressBar progressBar;
    private List<CityData> cities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initialize);
        this.progressBar = (ProgressBar)this.findViewById(R.id.progressBar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Minimum.getInstance().setCurrentActivity(this);
        this.loadCities();
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void loadCities() {
        if (cities == null || cities.size() == 0) {
            final GetMinimumCities task = ServerSession.getInstance().buildAsyncRequest(GetMinimumCities.class);
            task.setOnPostExecute(new ITaskEvent() {
                @Override
                public void onAction() {
                    if (Minimum.getInstance().isOnTop(Initialize.this)) {
                        cities = task.getCityList();
                        if (cities != null) {
                            DataAccess.getInstance().getCityDataDAO().create(cities);

                            final CityData city = UserSettings.getInstance().getCityData();
                            if (city != null) {
                                ServerData.getInstance().setCityData(city);
                            }
                            Initialize.this.authenticate();
                        } else {
                            loadCitiesNoConnection();
                        }
                    }
                }
            });
            boolean success = task.send();
            if (!success) {
                loadCitiesNoConnection();
            }
        } else {
           this.authenticate();
        }
    }

    private void loadCitiesNoConnection() {
        if (Minimum.getInstance().isOnTop(this)) {
            this.progressBar.setVisibility(View.INVISIBLE);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(Initialize.this);
            alertDialog.setMessage(R.string.activity_initialize_no_connection);
            alertDialog.setPositiveButton(R.string.activity_initialize_repeat, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    progressBar.setVisibility(View.VISIBLE);
                    loadCities();
                }
            });
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
    }

    private void authenticate() {
        UserSettings.getInstance().load(this);
        final String phoneNumber = UserSettings.getInstance().getLogin();
        final String password = UserSettings.getInstance().getPassword();

        if (phoneNumber != null && phoneNumber.length() > 0 && password != null && password.length() > 0) {
            final Authenticate task = ServerSession.getInstance().buildAsyncRequest(Authenticate.class);
            task.setPhoneNumber(phoneNumber);
            task.setPassword(password);
            task.setOnPostExecute(new ITaskEvent() {
                @Override
                public void onAction() {
                    if (Minimum.getInstance().isOnTop(Initialize.this)) {
                        if (task.isSuccess()) {
                            final Intent intent = new Intent(Initialize.this, Main.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            DataAccess.getInstance().close(Initialize.this);
                            startActivity(intent);
                            finish();
                        } else {
                            final Intent intent = new Intent(Initialize.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            });
            task.send();
        } else {
            final Intent intent = new Intent(Initialize.this, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
            finish();
        }
    }

    private void clearReferences(){
        Activity currActivity = Minimum.getInstance().getCurrentActivity();
        if (currActivity != null && currActivity.equals(this))
            Minimum.getInstance().setCurrentActivity(null);
    }

}