package com.weakteam.mobile.taxi.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.model.CityData;
import com.weakteam.mobile.taxi.service.ITaskEvent;
import com.weakteam.mobile.taxi.service.ServerSession;
import com.weakteam.mobile.taxi.service.task.Authenticate;
import com.weakteam.mobile.taxi.service.task.RemindPassword;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.widget.PhoneNumberEditField;

public class Login extends Activity {

    private EditText phoneNumberField;
    private EditText passwordField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        final CityData cityData = UserSettings.getInstance().getCityData();
        if (cityData!= null) {
            String prefix = cityData.getPhonePrefix();
            if (prefix != null && prefix.length() > 0) {
                if (prefix.equalsIgnoreCase("8")) {
                    prefix = "+7";
                }
                final TextView phonePrefix = (TextView)this.findViewById(R.id.phonePrefix);
                phonePrefix.setText(prefix);
            }
        }

        this.phoneNumberField = (EditText)this.findViewById(R.id.phoneNumber);
        this.passwordField = (EditText)this.findViewById(R.id.password);

        final LinearLayout phoneNumberLine = (LinearLayout)this.findViewById(R.id.phoneNumberLine);
        phoneNumberLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!phoneNumberField.isFocused()) {
                    phoneNumberField.requestFocus();
                }
            }
        });

        final LinearLayout passwordLine = (LinearLayout)this.findViewById(R.id.passwordLine);
        passwordLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!passwordField.isFocused()) {
                    passwordField.requestFocus();
                }
            }
        });

        final Button loginButton = (Button)this.findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login.this.authenticate();
            }
        });

        final TextView getBySmsPassword = (TextView)this.findViewById(R.id.getBySmsPassword);
        getBySmsPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remindPassword(true);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Minimum.getInstance().setCurrentActivity(this);

        UserSettings.getInstance().load(this);
        this.phoneNumberField.setText(UserSettings.getInstance().getLogin());
        this.passwordField.setText(UserSettings.getInstance().getPassword());

        this.phoneNumberField.clearFocus();
        this.passwordField.clearFocus();

    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void authenticate() {
        final String phoneNumber = this.phoneNumberField.getText().toString();
        final String password = this.passwordField.getText().toString();

        if (phoneNumber.length() < PhoneNumberEditField.PHONE_NUMBER_LENGTH) {
            Toast.makeText(Login.this, R.string.activity_login_alert_phone_number, Toast.LENGTH_SHORT).show();

        } else if (password.length() == 0) {
            Toast.makeText(Login.this, R.string.activity_login_alert_password, Toast.LENGTH_SHORT).show();
        } else {
            final Authenticate task = ServerSession.getInstance().buildAsyncRequest(Authenticate.class);
            task.setPhoneNumber(phoneNumber);
            task.setPassword(password);
            task.setOnPostExecute(new ITaskEvent() {
                @Override
                public void onAction() {
                    if (Minimum.getInstance().isOnTop(Login.this)) {
                        if (task.isSuccess()) {
                            UserSettings.getInstance().setLogin(phoneNumber);
                            UserSettings.getInstance().setPassword(password);
                            UserSettings.getInstance().save(Login.this);
                            Intent intent = new Intent(Login.this, Main.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                            finish();
                        } else {
                            final String message = task.getErrorMessage();
                            Toast.makeText(Login.this,message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            task.send();
        }
    }

    private void remindPassword(boolean bySms) {
        final RemindPassword task = ServerSession.getInstance().buildAsyncRequest(RemindPassword.class);
        final String phoneNumber = this.phoneNumberField.getText().toString();

        task.setPhoneNumber(phoneNumber);
        task.setBySms(bySms);
        task.setOnPostExecute(new ITaskEvent() {
            @Override
            public void onAction() {
                if (Minimum.getInstance().isOnTop(Login.this)) {
                    final String message = task.getResult();
                    if (message != null) {
                        Toast.makeText(Login.this, message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        task.send();
    }

    private void clearReferences(){
        Activity currActivity = Minimum.getInstance().getCurrentActivity();
        if (currActivity != null && currActivity.equals(this))
            Minimum.getInstance().setCurrentActivity(null);
    }

}
