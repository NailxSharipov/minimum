package com.weakteam.mobile.taxi.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.ui.fragment.AdditionalScreen;
import com.weakteam.mobile.taxi.ui.fragment.CurrentOrderScreen;
import com.weakteam.mobile.taxi.ui.fragment.MenuNavigationScreen;
import com.weakteam.mobile.taxi.ui.fragment.ProfileChangePasswordScreen;
import com.weakteam.mobile.taxi.ui.fragment.ProfileScreen;
import com.weakteam.mobile.taxi.util.CurrentLocationManager;
import com.weakteam.mobile.taxi.widget.MenuLayout;


/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 10.11.2014.
 */
public class Main extends Activity {

    private FrameLayout content;
    private MenuLayout menuLayout;

    private Fragment currentFragment;
    private MenuNavigationScreen menuNavigation;
    private ProfileScreen profile;
    private ProfileChangePasswordScreen profileChangePassword;
    private AdditionalScreen additional;
    private CurrentOrderScreen currentOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.menuLayout = (MenuLayout)this.getLayoutInflater().inflate(R.layout.activity_main, null);
        setContentView(this.menuLayout);

        this.content = (FrameLayout)findViewById(R.id.menu_content);
        this.setMenuNavigation();
    }
    @Override
    protected void onResume() {
        super.onResume();

        Minimum.getInstance().setCurrentActivity(this);
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    public void setAdditional() {
        if (this.additional == null) {
            this.additional = new AdditionalScreen();
        }
        this.setFragment(this.additional);
    }

    public void setCurrentOrder() {
        if (this.currentOrder == null) {
            this.currentOrder = new CurrentOrderScreen();
        }
        this.setFragment(this.currentOrder);
    }

    private void setMenuNavigation() {
        if (this.menuNavigation == null) {
            this.menuNavigation = new MenuNavigationScreen();
        }
        this.setFragment(this.menuNavigation);
    }

    public void setProfileChangePassword() {
        if (this.profileChangePassword == null) {
            this.profileChangePassword = new ProfileChangePasswordScreen();
        }
        this.setFragment(this.profileChangePassword);
    }

    public void setProfile() {
        if (this.profile == null) {
            this.profile = new ProfileScreen();
        }
        this.setFragment(this.profile);
    }


    private void setFragment(final Fragment fragment) {
        if (fragment != null) {
            final FragmentManager manager = this.getFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(this.content.getId(), fragment);
            transaction.addToBackStack(null);
            //transaction.commit();
            transaction.commitAllowingStateLoss();
            manager.executePendingTransactions();
            this.currentFragment = fragment;
        }
    }

    @Override
    public void onBackPressed() {
        this.back();
    }

    public void back() {
        if (this.menuLayout.isMenuShown()) {
            if (currentFragment == menuNavigation) {
                this.menuLayout.toggleMenu();
            } else if (currentFragment == profileChangePassword) {
                this.setProfile();
            } else {
                this.setMenuNavigation();
            }
        } else {
            final String question = this.getResources().getString(R.string.exit_question);
            final String yes = this.getResources().getString(R.string.yes);
            final String no = this.getResources().getString(R.string.no);

            final AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setMessage(question);

            alert.setPositiveButton(yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });

            alert.setNegativeButton(no, null);

            alert.show();
        }
    }

    public void openMenu() {
        if (!this.menuLayout.isMenuShown()) {
            this.menuLayout.toggleMenu();
        }
    }

    public void toggleMenu() {
        this.menuLayout.toggleMenu();
    }

    @Override
    public boolean dispatchTouchEvent (@NonNull MotionEvent ev) {
        // Do your calcluations
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        CurrentLocationManager.getInstance().connect(this);
    }

    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        CurrentLocationManager.getInstance().disconnect();
        super.onStop();
    }

    private void clearReferences(){
        Activity currActivity = Minimum.getInstance().getCurrentActivity();
        if (currActivity != null && currActivity.equals(this))
            Minimum.getInstance().setCurrentActivity(null);
    }

}
