package com.weakteam.mobile.taxi.ui.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.data.DestinationPointManager;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.ui.fragment.SetDestinationPointFields;
import com.weakteam.mobile.taxi.ui.fragment.SetDestinationPointSearch;

/**
 * Created by Nail on 16.12.2014.
 */
public class SetDestinationPoint extends Activity {

    private FrameLayout content;
    private Fragment currentFragment;
    private SetDestinationPointSearch searchFragment;
    private SetDestinationPointFields fieldsFragment;
    private TextView title;

    public SetDestinationPointFields getFieldsFragment() {
        return fieldsFragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_set_destination_point);

        this.title = (TextView)findViewById(R.id.title);
        final LinearLayout navigationBar = (LinearLayout)findViewById(R.id.navigationBar);
        navigationBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetDestinationPoint.this.back();
            }
        });

        this.content = (FrameLayout)findViewById(R.id.content);
        this.setSearch();
    }

    public void setSearch() {
        if (this.searchFragment == null) {
            this.searchFragment = new SetDestinationPointSearch();
        }
        this.setFragment(this.searchFragment);
    }

    public void setFields() {
        if (this.fieldsFragment == null) {
            this.fieldsFragment = new SetDestinationPointFields();
        }
        this.setFragment(this.fieldsFragment);
    }

    private void setFragment(final Fragment fragment) {
        if (fragment != null && fragment != this.currentFragment) {
            final FragmentManager manager = this.getFragmentManager();
            if (this.currentFragment != null) {
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(this.content.getId(), fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            } else {
                final String name = fragment.getClass().getName();
                manager.beginTransaction().add(this.content.getId(), fragment, name).commit();
            }
        }
        this.currentFragment = fragment;
    }

    @Override
    protected void onResume() {
        super.onResume();

        Minimum.getInstance().setCurrentActivity(this);
        final DestinationPoint point = DestinationPointManager.getInstance().getActivePoint();

        this.title.setText(point.getAction());
        if (point.isEmpty()) {
            this.setSearch();
        } else {
            this.setFields();
        }
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        this.back();
    }

    public void back() {
        final Intent intent = new Intent(this, Main.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        this.finish();
    }

    private void clearReferences(){
        Activity currActivity = Minimum.getInstance().getCurrentActivity();
        if (currActivity != null && currActivity.equals(this))
            Minimum.getInstance().setCurrentActivity(null);
    }

}