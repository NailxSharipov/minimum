package com.weakteam.mobile.taxi.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.data.DestinationPointManager;
import com.weakteam.mobile.taxi.data.SearchPlaceResult;
import com.weakteam.mobile.taxi.service.ITaskEvent;
import com.weakteam.mobile.taxi.service.ServerSession;
import com.weakteam.mobile.taxi.service.task.GeoLocationOSMNominatim;
import com.weakteam.mobile.taxi.service.task.SearchPlace;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.util.CurrentLocationManager;

import java.util.List;

/**
 *  Created by Nail on 15.02.2015.
 */
public class SetDestinationPointOnMap extends Activity implements OnMapReadyCallback {

    private TextView title;
    private TextView place;
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_set_destination_point_on_map);

        this.title = (TextView)findViewById(R.id.title);
        this.place = (TextView)findViewById(R.id.place);
        final LinearLayout navigationBar = (LinearLayout)findViewById(R.id.navigationBar);
        navigationBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DestinationPoint point = DestinationPointManager.getInstance().getActivePoint();
                if (point != null && !point.isFrom()) {
                    Intent intent = new Intent(SetDestinationPointOnMap.this, Main.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                    SetDestinationPointOnMap.this.finish();
                } else {
                    final Intent intent = new Intent(SetDestinationPointOnMap.this, SetDestinationPoint.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                    SetDestinationPointOnMap.this.finish();
                }
            }
        });

        final GoogleMap googleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
        this.initMap(googleMap);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Minimum.getInstance().setCurrentActivity(this);

        final DestinationPoint point = DestinationPointManager.getInstance().getActivePoint();
        this.title.setText(point.getAction());
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        this.back();
        final Intent intent = new Intent(this, SetDestinationPoint.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        this.finish();
    }

    public void back() {
        this.finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.initMap(googleMap);
    }

    private void initMap(GoogleMap googleMap) {
        if (this.map == null) {
            this.map = googleMap;
            final Location location = CurrentLocationManager.getInstance().getPosition();
            if (location != null) {
                final LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 17.0f);
                googleMap.animateCamera(cameraUpdate);
                googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition cameraPosition) {
                        GetAddress(cameraPosition.target);
                    }
                });
            }
        }
    }

    private void GetAddress(LatLng latLng) {
        final GeoLocationOSMNominatim task = ServerSession.getInstance().buildAsyncRequest(GeoLocationOSMNominatim.class);
        task.setLatLng(latLng);
        task.setOnPostExecute(new ITaskEvent() {
            @Override
            public void onAction() {
                if (Minimum.getInstance().isOnTop(SetDestinationPointOnMap.this)) {
                    final GeoLocationOSMNominatim.GeoLocation geoLocation = task.getGeoLocation();
                    if (geoLocation != null) {
                        final SearchPlace searchTask = ServerSession.getInstance().buildAsyncRequest(SearchPlace.class);
                        searchTask.setKey(geoLocation.getStreet());
                        searchTask.setType(com.weakteam.mobile.taxi.data.Location.Type.Street);
                        searchTask.setOnPostExecute(new ITaskEvent() {
                            @Override
                            public void onAction() {
                                final List<SearchPlaceResult> resultList = searchTask.getSearchPlaceResultList();
                                if (resultList != null && resultList.size() > 0) {
                                    for (SearchPlaceResult result : resultList) {
                                        if (result.getLocationType() == com.weakteam.mobile.taxi.data.Location.Type.Street) {
                                            final DestinationPoint point = DestinationPointManager.getInstance().getActivePoint();
                                            point.setWithSearchPlaceResult(result);
                                            point.setHome(geoLocation.getNumber());
                                            place.setText(point.getObjectAddress());
                                            break;
                                        }
                                    }
                                } else {
                                    place.setText(R.string.activity_point_on_map_no_result);
                                }
                            }
                        });
                        searchTask.send();
                    } else {
                        place.setText(R.string.activity_point_on_map_no_result);
                    }
                }
            }
        });
        task.send();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void clearReferences(){
        Activity currActivity = Minimum.getInstance().getCurrentActivity();
        if (currActivity != null && currActivity.equals(this))
            Minimum.getInstance().setCurrentActivity(null);
    }

}