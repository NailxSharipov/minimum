package com.weakteam.mobile.taxi.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.weakteam.mobile.taxi.data.SearchPlaceResult;
import com.weakteam.mobile.taxi.ui.screen.setdestinationpoint.AddressSearchResultCell;

import java.util.List;

/**
 * Created by Nail on 21.12.2014.
 */
public class AddressSearchResultAdapter extends BaseAdapter {

    private Context context;
    private List<SearchPlaceResult> items;

    public AddressSearchResultAdapter(Context context, List<SearchPlaceResult> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    SearchPlaceResult getSearchPlaceResult(int position) {
        return (SearchPlaceResult)getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        AddressSearchResultCell cell = (AddressSearchResultCell)convertView;
        if (cell == null) {
            cell = new AddressSearchResultCell(this.context);
        }

        final SearchPlaceResult result = this.getSearchPlaceResult(position);
        cell.setSearchPlaceResult(result);

        return cell;
    }
}