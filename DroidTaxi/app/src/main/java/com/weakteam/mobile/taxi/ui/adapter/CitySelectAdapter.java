package com.weakteam.mobile.taxi.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.model.CityData;
import com.weakteam.mobile.taxi.ui.screen.main.CitySelectCell;

import java.util.List;

/**
 * Created by Nail on 25.01.2015.
 */
public class CitySelectAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater lInflater;
    private List<CityData> items;

    public CitySelectAdapter(Context context, List<CityData> items) {
        this.context = context;
        this.items = items;
        this.lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (items != null) {
            return items.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (position >= items.size()) {
            final CityData cityData = new CityData();
            final String selectCity = this.context.getString(R.string.fragment_main_select_city);
            cityData.setTitle(selectCity);

            return cityData;
        } else {
            return this.items.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public CityData getCityData(int position) {
        return (CityData)getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (position != items.size()) {
            CitySelectCell cell = (CitySelectCell)convertView;
            if (cell == null) {
                cell = new CitySelectCell(this.context);
            }

            final CityData cityData = this.getCityData(position);
            cell.setCityData(cityData);

            return cell;

        } else {
            CitySelectCell cell = (CitySelectCell)convertView;
            if (cell == null) {
                cell = new CitySelectCell(this.context);
            }
            final CityData cityData = new CityData();
            final String selectCity = this.context.getString(R.string.fragment_main_select_city);
            cityData.setTitle(selectCity);

            cell.setCityData(cityData);
            return cell;
        }
    }
}