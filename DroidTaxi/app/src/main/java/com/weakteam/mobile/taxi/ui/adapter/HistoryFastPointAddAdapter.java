package com.weakteam.mobile.taxi.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.ui.screen.setdestinationpoint.HistoryFastPointAddCell;

import java.util.List;

/**
 * Created by Nail on 18.01.2015.
 */
public class HistoryFastPointAddAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater lInflater;
    private List<DestinationPoint> items;

    public HistoryFastPointAddAdapter(Context context, List<DestinationPoint> items) {
        this.context = context;
        this.items = items;
        this.lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    DestinationPoint getDestinationPoint(int position) {
        return (DestinationPoint)getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        HistoryFastPointAddCell cell = (HistoryFastPointAddCell)convertView;
        if (cell == null) {
            cell = new HistoryFastPointAddCell(this.context);
        }

        final DestinationPoint point = this.getDestinationPoint(position);
        cell.setDestinationPoint(point);

        return cell;
    }
}