package com.weakteam.mobile.taxi.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.weakteam.mobile.taxi.data.History;
import com.weakteam.mobile.taxi.ui.screen.main.HistoryOrderCell;

import java.util.List;

/**
 * Created by Nail on 12.04.2015.
 */
public class HistoryOrderAdapter extends BaseAdapter {

    private Context context;
    private List<History> items;

    public HistoryOrderAdapter(Context context, List<History> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        if (items != null) {
            return items.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public History getHistory(int position) {
        return (History)getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HistoryOrderCell cell = (HistoryOrderCell)convertView;
        if (cell == null) {
            cell = new HistoryOrderCell(this.context);
        }

        final History history = this.getHistory(position);
        cell.setHistory(history);

        return cell;
    }
}