package com.weakteam.mobile.taxi.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.weakteam.mobile.taxi.data.Order;
import com.weakteam.mobile.taxi.ui.screen.main.OrderCell;

import java.util.List;

/**
 * Created by Nail on 12.04.2015.
 */
public class OrderAdapter extends BaseAdapter {

    private Context context;
    //private LayoutInflater lInflater;
    private List<Order> items;
    private ICancelOrder cancelOrderListener;

    public void setCancelOrderListener(ICancelOrder cancelOrderListener) {
        this.cancelOrderListener = cancelOrderListener;
    }

    public OrderAdapter(Context context, List<Order> items) {
        this.context = context;
        this.items = items;
        //this.lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (items != null) {
            return items.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public Order getOrder(int position) {
        return (Order)getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OrderCell cell = (OrderCell)convertView;
        if (cell == null) {
            cell = new OrderCell(this.context);
        }

        final Order order = this.getOrder(position);
        cell.setCancelOrderListener(this.cancelOrderListener);
        cell.setOrder(order);

        return cell;
    }

    public interface ICancelOrder {
        void onCancelOrder(Order order);
    }
}