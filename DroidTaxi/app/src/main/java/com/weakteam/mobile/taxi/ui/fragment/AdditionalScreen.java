package com.weakteam.mobile.taxi.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.model.CityData;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.ui.activity.Main;

/**
 *  Created by Nail on 11.01.2015.
 */

public class AdditionalScreen extends Fragment {

    private TextView phoneNumber;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_additional, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final CityData cityData = UserSettings.getInstance().getCityData();
        if (cityData!= null) {
            String prefix = cityData.getPhonePrefix();
            if (prefix != null && prefix.length() > 0) {
                if (prefix.equalsIgnoreCase("8")) {
                    prefix = "+7";
                }
                final TextView phonePrefix = (TextView)this.getActivity().findViewById(R.id.phonePrefix);
                phonePrefix.setText(prefix);
            }
        }
        this.phoneNumber = (TextView)this.getActivity().findViewById(R.id.phoneNumber);

        final LinearLayout navigationBar = (LinearLayout)this.getActivity().findViewById(R.id.navigationBar);
        navigationBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Main)AdditionalScreen.this.getActivity()).back();
            }
        });
    }

    public void onResume() {
        super.onResume();
        final String secondPhone = UserSettings.getInstance().getSecondPhone();
        this.phoneNumber.setText(secondPhone);
    }

    @Override
    public void onPause() {
        super.onPause();

        this.save();

        final Activity activity = this.getActivity();

        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(this.phoneNumber.getWindowToken(), 0);
        }
    }

    private void save() {

        final String secondPhone = this.phoneNumber.getText().toString();
        if (secondPhone.length() > 4) {
            UserSettings.getInstance().setSecondPhone(secondPhone);
            UserSettings.getInstance().save(this.getActivity());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

}