package com.weakteam.mobile.taxi.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.Order;
import com.weakteam.mobile.taxi.data.OrderManager;
import com.weakteam.mobile.taxi.service.ITaskEvent;
import com.weakteam.mobile.taxi.service.ServerSession;
import com.weakteam.mobile.taxi.service.task.CancelOrder;
import com.weakteam.mobile.taxi.service.task.GetCurrentOrder;
import com.weakteam.mobile.taxi.ui.activity.Main;
import com.weakteam.mobile.taxi.ui.adapter.OrderAdapter;
import com.weakteam.mobile.taxi.util.SimpleLog;

import java.util.List;

/**
 * Created by Nail on 18.01.2015.
 */
public class CurrentOrderScreen extends Fragment {

    private ListView orderView;
    private View loadProgress;
    private List<Order> orders;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_current_order, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.loadProgress = this.getActivity().findViewById(R.id.loadProgress);
        this.orderView = (ListView) this.getActivity().findViewById(R.id.orderView);

        final LinearLayout navigationBar = (LinearLayout) this.getActivity().findViewById(R.id.navigationBar);
        navigationBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Main) CurrentOrderScreen.this.getActivity()).back();
            }
        });

        orderView.setVisibility(View.INVISIBLE);
    }

    public void onResume() {
        super.onResume();

        SimpleLog.info("CurrentOrder");

        this.updateOrder();
    }

    private void updateOrder() {
        this.loadProgress.setVisibility(View.VISIBLE);
        final GetCurrentOrder task = ServerSession.getInstance().buildAsyncRequest(GetCurrentOrder.class);
        task.setOnPostExecute(new ITaskEvent() {
            @Override
            public void onAction() {
                loadProgress.setVisibility(View.INVISIBLE);
                orders = task.getOrderList();
                if (orders != null && orders.size() > 0) {

                    orderView.setVisibility(View.VISIBLE);
                    final OrderAdapter adapter = new OrderAdapter(CurrentOrderScreen.this.getActivity(), orders);
                    adapter.setCancelOrderListener(new OrderAdapter.ICancelOrder() {
                        @Override
                        public void onCancelOrder(Order order) {
                            cancelOrder(order);
                        }
                    });
                    orderView.setAdapter(adapter);
                } else {
                    orderView.setVisibility(View.INVISIBLE);
                }

            }
        });
        task.send();
    }

    public void cancelOrder(final Order order) {
        if (order != null) {
            final CancelOrder task = ServerSession.getInstance().buildAsyncRequest(CancelOrder.class);
            task.setOrderId(order.getOrderId());
            task.setOnPostExecute(
                    new ITaskEvent() {
                        @Override
                        public void onAction() {
                            if (Minimum.getInstance().isOnTop(CurrentOrderScreen.this.getActivity())) {
                                if (task.getResult()) {
                                    OrderManager.getInstance().cancelOrderId(order.getOrderId());
                                    updateOrder();
                                    final String canceled = CurrentOrderScreen.this.getActivity().getResources().getString(R.string.fragment_current_order_canceled);
                                    Toast.makeText(CurrentOrderScreen.this.getActivity(), canceled, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
            );
            task.send();

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }
}