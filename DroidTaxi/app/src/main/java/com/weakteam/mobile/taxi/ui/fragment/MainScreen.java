package com.weakteam.mobile.taxi.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.model.CityData;
import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.data.DestinationPointManager;
import com.weakteam.mobile.taxi.data.HistoryManager;
import com.weakteam.mobile.taxi.data.SearchPlaceResult;
import com.weakteam.mobile.taxi.data.realm.DataAccess;
import com.weakteam.mobile.taxi.service.ITaskEvent;
import com.weakteam.mobile.taxi.service.ServerData;
import com.weakteam.mobile.taxi.service.ServerSession;
import com.weakteam.mobile.taxi.service.task.GeoLocationOSMNominatim;
import com.weakteam.mobile.taxi.service.task.MakeOrder;
import com.weakteam.mobile.taxi.service.task.SearchPlace;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.ui.activity.Main;
import com.weakteam.mobile.taxi.ui.activity.SetDestinationPoint;
import com.weakteam.mobile.taxi.ui.adapter.CitySelectAdapter;
import com.weakteam.mobile.taxi.ui.screen.main.DestinationPointsView;
import com.weakteam.mobile.taxi.util.CurrentLocationManager;
import com.weakteam.mobile.taxi.util.SimpleLog;

import java.util.List;


/**
 * Created by Nail on 29.11.2014.
 */
public class MainScreen extends Fragment {

    private DestinationPointsView destinationPointsView;

    private TextView priceTextView;
    private String currency;

    public DestinationPointsView getDestinationPointsView() {
        return destinationPointsView;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FrameLayout menuButton = (FrameLayout) getActivity().findViewById(R.id.menuButton);

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main main = (Main) MainScreen.this.getActivity();
                main.toggleMenu();
            }
        });

        this.destinationPointsView = (DestinationPointsView) getActivity().findViewById(R.id.destinationPointList);
        this.destinationPointsView.setPointViewEvents(new DestinationPointsView.PointViewEvents() {
            @Override
            public void onSelect(DestinationPoint destinationPoint) {
                final CityData cityData = UserSettings.getInstance().getCityData();
                if (cityData != null) {
                    DestinationPointManager.getInstance().setActivePoint(destinationPoint);
                    Intent intent = new Intent(getActivity(), SetDestinationPoint.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                } else {
                    final String selectCity = MainScreen.this.getResources().getString(R.string.fragment_main_select_city);
                    Toast.makeText(MainScreen.this.getActivity(), selectCity, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onClose() {
                MainScreen.this.updatePrice();
            }
        });

        this.priceTextView = (TextView) getActivity().findViewById(R.id.price);
        final Button callTaxiButton = (Button) getActivity().findViewById(R.id.callTaxiButton);
        callTaxiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeOrder();
            }
        });

        Spinner citySpinner = (Spinner) getActivity().findViewById(R.id.citySelect);

        final List<CityData> cities = DataAccess.getInstance().getCityDataDAO().getAll();
        final CitySelectAdapter adapter = new CitySelectAdapter(this.getActivity(), cities);
        citySpinner.setAdapter(adapter);

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                final CityData cityData = adapter.getCityData(position);
                if (cityData.getCityId() != null) {
                    UserSettings.getInstance().setCityData(cityData);
                    UserSettings.getInstance().save(MainScreen.this.getActivity());
                    ServerData.getInstance().setCityData(cityData);
                    setCurrency();
                    HistoryManager.getInstance().loadHistory();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        final CityData cityData = UserSettings.getInstance().getCityData();

        if (cities != null) {
            if (cityData == null) {
                citySpinner.setSelection(cities.size());
            } else {
                int index = 0;
                for (CityData data : cities) {
                    if (data.getCityId().compareToIgnoreCase(cityData.getCityId()) == 0) {
                        citySpinner.setSelection(index);
                        break;
                    }
                    index++;
                }
            }
        }

        this.setCurrency();

        CurrentLocationManager.getInstance().setIPosition(new CurrentLocationManager.IPosition() {
            @Override
            public void onUpdate(Location position) {
                loadLocation();
            }
        });
    }

    public void onResume() {
        super.onResume();
        this.destinationPointsView.reloadData();
        this.updatePrice();
        HistoryManager.getInstance().loadHistory();
    }

    private void setCurrency() {
        final CityData cityData = UserSettings.getInstance().getCityData();
        if (cityData != null) {
            this.currency = cityData.getMoney();
        } else {
            this.currency = this.getActivity().getResources().getString(R.string.fragment_main_currency);
        }

    }

    private void updatePrice() {
        this.priceTextView.setText("");
        if (DestinationPointManager.getInstance().getSize() >= 2 && !DestinationPointManager.getInstance().getPoints().get(0).isEmpty()) {
            final MakeOrder task = ServerSession.getInstance().buildAsyncRequest(MakeOrder.class);
            task.setOrder(false);
            task.setOnPostExecute(new ITaskEvent() {
                @Override
                public void onAction() {
                    if (Minimum.getInstance().isOnTop(MainScreen.this.getActivity())) {
                        if (task.getPrice() != null) {
                            priceTextView.setText(task.getPrice() + " " + currency);
                        }
                    }
                }
            });
            task.send();
        }
    }

    private void makeOrder() {
        final String priceText = priceTextView.getText().toString();
        final Activity activity = this.getActivity();
        if (DestinationPointManager.getInstance().getSize() > 1 && priceText.length() > 0) {
            this.doOrder();
        } else if (DestinationPointManager.getInstance().getSize() < 2) {
            final String notEnoughPoints = activity.getResources().getString(R.string.fragment_main_order_not_enough_points);
            Toast.makeText(activity, notEnoughPoints, Toast.LENGTH_SHORT).show();
        } else if (priceText.length() == 0) {
            this.updatePrice();
            final String canNotCalculatePrice = activity.getResources().getString(R.string.fragment_main_order_can_not_calculate_price);
            Toast.makeText(activity, canNotCalculatePrice, Toast.LENGTH_SHORT).show();
        }
    }

    public void doOrder() {
        final Activity activity = this.getActivity();
        final DestinationPoint from = DestinationPointManager.getInstance().getPoints().get(0);

        final String porch = from.getPorch();
        if (porch == null || porch.length() == 0) {

            final String attention = this.getActivity().getResources().getString(R.string.attention);
            final String hint = this.getActivity().getResources().getString(R.string.fragment_main_porch_hint);
            final String question = this.getActivity().getResources().getString(R.string.fragment_main_porch_question);

            final AlertDialog.Builder alert = new AlertDialog.Builder(activity);
            final EditText input = new EditText(activity);
            input.setHint(hint);

            alert.setTitle(attention);
            alert.setMessage(question);
            alert.setView(input);

            alert.setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    final String porch = input.getText().toString();
                    from.setPorch(porch);
                    doOrder();
                }
            });
            alert.show();

        } else {
            final String question = activity.getResources().getString(R.string.fragment_main_order_question);
            final String yes = activity.getResources().getString(R.string.yes);
            final String no = activity.getResources().getString(R.string.no);

            final AlertDialog.Builder alert = new AlertDialog.Builder(this.getActivity());

            alert.setMessage(question);

            alert.setPositiveButton(yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    SimpleLog.info("Заказ принят");

                    final MakeOrder task = ServerSession.getInstance().buildAsyncRequest(MakeOrder.class);
                    task.setOrder(true);
                    task.setOnPostExecute(new ITaskEvent() {
                        @Override
                        public void onAction() {
                            final String orderId = task.getOrderId();
                            if (orderId != null) {
                                final String accepted = activity.getResources().getString(R.string.fragment_main_order_accepted);
                                Toast.makeText(activity, accepted, Toast.LENGTH_SHORT).show();

                                final Main main = (Main) activity;
                                main.setCurrentOrder();
                                main.openMenu();

                                DestinationPointManager.getInstance().clearAll();
                            }
                        }
                    });
                    task.send();
                }
            });

            alert.setNegativeButton(no, null);

            alert.show();
        }
    }

    public void loadLocation() {
        final List<DestinationPoint> points = DestinationPointManager.getInstance().getPoints();
        final DestinationPoint from = points.get(0);
        if (from.getObjectId() == null) {
            final GeoLocationOSMNominatim task = ServerSession.getInstance().buildAsyncRequest(GeoLocationOSMNominatim.class);
            task.setPosition(CurrentLocationManager.getInstance().getPosition());
            task.setOnPostExecute(
                    new ITaskEvent() {
                        @Override
                        public void onAction() {
                            if (Minimum.getInstance().isOnTop(MainScreen.this.getActivity())) {
                                final GeoLocationOSMNominatim.GeoLocation geoLocation = task.getGeoLocation();
                                if (geoLocation != null) {
                                    final SearchPlace searchTask = ServerSession.getInstance().buildAsyncRequest(SearchPlace.class);
                                    searchTask.setKey(geoLocation.getStreet());
                                    searchTask.setOnPostExecute(new ITaskEvent() {
                                        @Override
                                        public void onAction() {
                                            final List<SearchPlaceResult> resultList = searchTask.getSearchPlaceResultList();
                                            if (resultList != null) {
                                                for (SearchPlaceResult result : resultList) {
                                                    if (result.getLocationType() == com.weakteam.mobile.taxi.data.Location.Type.Street) {
                                                        from.setWithSearchPlaceResult(result);
                                                        from.setHome(geoLocation.getNumber());
                                                        destinationPointsView.reloadData();
                                                        updatePrice();
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    });
                                    searchTask.send();
                                }
                            }
                        }
                    }

            );
            task.send();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

}