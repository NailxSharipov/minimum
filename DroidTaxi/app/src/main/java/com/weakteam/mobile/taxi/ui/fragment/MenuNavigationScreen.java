package com.weakteam.mobile.taxi.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPointManager;
import com.weakteam.mobile.taxi.data.History;
import com.weakteam.mobile.taxi.data.HistoryManager;
import com.weakteam.mobile.taxi.ui.activity.Main;
import com.weakteam.mobile.taxi.ui.adapter.HistoryOrderAdapter;
import com.weakteam.mobile.taxi.ui.screen.main.HistoryOrderCell;

import java.util.List;


/**
 *  Created by Nail on 04.12.2014.
 */
public class MenuNavigationScreen extends Fragment implements HistoryManager.IHistoryUpdate {

    private ListView historyOrderView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu_navigation, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Main main = (Main)this.getActivity();

        final LinearLayout profileButton = (LinearLayout)getActivity().findViewById(R.id.profileButton);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.setProfile();
            }
        });

        final LinearLayout additionalButton = (LinearLayout)getActivity().findViewById(R.id.additionalButton);
        additionalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.setAdditional();
            }
        });

        final LinearLayout currentOrderButton = (LinearLayout)getActivity().findViewById(R.id.currentOrderButton);
        currentOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main.setCurrentOrder();
            }
        });

        this.historyOrderView = (ListView)getActivity().findViewById(R.id.historyOrderView);
        this.historyOrderView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View item, int position, long id) {
                final History history = ((HistoryOrderCell)item).getHistory();
                DestinationPointManager.getInstance().setWithHistory(history);
                MainScreen mainScreen = (MainScreen)getFragmentManager().findFragmentById(R.id.content_frame);
                mainScreen.getDestinationPointsView().reloadData();
                mainScreen.doOrder();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        HistoryManager.getInstance().removeListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HistoryManager.getInstance().addListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.onHistoryUpdate();
    }

    @Override
    public void onHistoryUpdate() {
        if (Minimum.getInstance() != null && Minimum.getInstance().isOnTop(this.getActivity())) {
            final List<History> historyList = HistoryManager.getInstance().getHistoryList();

            if (historyList != null && historyList.size() > 0 && this.historyOrderView != null) {
                final HistoryOrderAdapter adapter = new HistoryOrderAdapter(this.getActivity(), historyList);
                this.historyOrderView.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }
}