package com.weakteam.mobile.taxi.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.service.ITaskEvent;
import com.weakteam.mobile.taxi.service.ServerSession;
import com.weakteam.mobile.taxi.service.task.ChangePassword;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.ui.activity.Main;

/**
 *  Created by Nail on 11.01.2015.
 */
public class ProfileChangePasswordScreen extends Fragment {

    private EditText newPassword;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_change_password, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final LinearLayout navigationBar = (LinearLayout)this.getActivity().findViewById(R.id.navigationBar);
        navigationBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Main)ProfileChangePasswordScreen.this.getActivity()).back();
            }
        });

        this.newPassword = (EditText)this.getActivity().findViewById(R.id.newPassword);

        final Button buttonChangePassword = (Button)this.getActivity().findViewById(R.id.change_password);
        buttonChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword();
            }
        });
    }

    public void onResume() {
        super.onResume();
    }

    private void changePassword() {
        final Main activity = (Main)this.getActivity();
        final String newPassword = this.newPassword.getText().toString();
        final ChangePassword task = ServerSession.getInstance().buildAsyncRequest(ChangePassword.class);
        task.setNewPassword(newPassword);
        task.setOnPostExecute(new ITaskEvent() {
            @Override
            public void onAction() {
                if (Minimum.getInstance().isOnTop(ProfileChangePasswordScreen.this.getActivity())) {
                    if (task.getCode() != null && task.getCode() == 1) {
                        final String changed = activity.getString(R.string.fragment_profile_change_password_success);
                        Toast.makeText(activity, changed + " " + newPassword, Toast.LENGTH_SHORT).show();
                        UserSettings.getInstance().setPassword(newPassword);
                        UserSettings.getInstance().save(activity);
                        activity.back();
                        activity.back();
                    } else if (task.getMessage() != null) {
                        Toast.makeText(activity, task.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        task.send();
    }

    @Override
    public void onPause() {
        super.onPause();
        final Activity activity = this.getActivity();
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(this.newPassword.getWindowToken(), 0);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }
}