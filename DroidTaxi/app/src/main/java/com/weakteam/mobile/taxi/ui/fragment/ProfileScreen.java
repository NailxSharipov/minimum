package com.weakteam.mobile.taxi.ui.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.model.CityData;
import com.weakteam.mobile.taxi.state.UserSettings;
import com.weakteam.mobile.taxi.ui.activity.Login;
import com.weakteam.mobile.taxi.ui.activity.Main;

/**
 *  Created by Nail on 07.12.2014.
 */
public class ProfileScreen extends Fragment {

    private TextView phoneNumber;
    private TextView password;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final CityData cityData = UserSettings.getInstance().getCityData();
        if (cityData!= null) {
            String prefix = cityData.getPhonePrefix();
            if (prefix != null && prefix.length() > 0) {
                if (prefix.equalsIgnoreCase("8")) {
                    prefix = "+7";
                }
                final TextView phonePrefix = (TextView)this.getActivity().findViewById(R.id.phonePrefix);
                phonePrefix.setText(prefix);
            }
        }

        this.phoneNumber = (TextView)this.getActivity().findViewById(R.id.phoneNumber);
        this.password = (TextView)this.getActivity().findViewById(R.id.password);
        final LinearLayout changePassword = (LinearLayout) this.getActivity().findViewById(R.id.change_password);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Main main = (Main) ProfileScreen.this.getActivity();
                main.setProfileChangePassword();
            }
        });

        final LinearLayout navigationBar = (LinearLayout)this.getActivity().findViewById(R.id.navigationBar);
        navigationBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Main)ProfileScreen.this.getActivity()).back();
            }
        });

        final Button loginButton = (Button)this.getActivity().findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserSettings.getInstance().save(ProfileScreen.this.getActivity());
                Intent intent = new Intent(getActivity(), Login.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });
    }

    public void onResume() {
        super.onResume();

        this.phoneNumber.setText(UserSettings.getInstance().getLogin());
        this.password.setText(UserSettings.getInstance().getPassword());

    }

    @Override
    public void onPause() {
        super.onPause();
        final Activity activity = this.getActivity();
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText()) {
            imm.hideSoftInputFromWindow(this.phoneNumber.getWindowToken(), 0);
            imm.hideSoftInputFromWindow(this.password.getWindowToken(), 0);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }
}