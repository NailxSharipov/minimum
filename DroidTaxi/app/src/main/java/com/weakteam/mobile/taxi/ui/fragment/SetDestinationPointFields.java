package com.weakteam.mobile.taxi.ui.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.data.DestinationPointManager;
import com.weakteam.mobile.taxi.data.Location;
import com.weakteam.mobile.taxi.ui.activity.SetDestinationPoint;
import com.weakteam.mobile.taxi.ui.activity.SetDestinationPointOnMap;

/**
 *  Created by Nail on 21.12.2014.
 */
public class SetDestinationPointFields extends Fragment {

    private TextView searchField;
    private EditText homeField;
    private EditText porchField;

    private View homeArea;
    private View porchArea;

    private boolean focusPorch = false;

    public void setFocusPorch(boolean focusPorch) {
        this.focusPorch = focusPorch;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_set_point_fields, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.searchField = (TextView)this.getActivity().findViewById(R.id.search_field);
        this.homeField = (EditText)this.getActivity().findViewById(R.id.home_field);
        this.porchField = (EditText)this.getActivity().findViewById(R.id.porch_field);
        final Button saveButton = (Button)this.getActivity().findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SetDestinationPoint)SetDestinationPointFields.this.getActivity()).back();
            }
        });

        this.homeArea = this.getActivity().findViewById(R.id.home_area);
        this.porchArea = this.getActivity().findViewById(R.id.porch_area);

        this.searchField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SetDestinationPoint)SetDestinationPointFields.this.getActivity()).setSearch();
                SetDestinationPointFields.this.save();
            }
        });

        final View onMap = getActivity().findViewById(R.id.pointOnMap);
        onMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SetDestinationPointFields.this.getActivity(), SetDestinationPointOnMap.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });
    }

    public void onResume() {
        super.onResume();

        final DestinationPoint point = DestinationPointManager.getInstance().getActivePoint();
        this.searchField.setText(point.getStreet());
        this.homeField.setText(point.getHome());
        this.porchField.setText(point.getPorch());

        if (point.isFrom()) {
            this.porchArea.setVisibility(View.VISIBLE);
        } else {
            this.porchArea.setVisibility(View.GONE);
        }

        if (point.getType() == Location.Type.Street) {
            this.homeArea.setVisibility(View.VISIBLE);
            if (point.isFrom()) {
                this.porchField.setText(point.getPorch());
            }
        } else {
            this.homeArea.setVisibility(View.GONE);
        }

        if (this.focusPorch) {
            this.porchField.requestFocus();
            this.focusPorch = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        this.save();
    }

    private void save() {
        final DestinationPoint point = DestinationPointManager.getInstance().getActivePoint();
        point.setHome(this.homeField.getText().toString());
        point.setPorch(this.porchField.getText().toString());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }
}