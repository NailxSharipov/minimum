package com.weakteam.mobile.taxi.ui.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.data.DestinationPointManager;
import com.weakteam.mobile.taxi.data.HistoryManager;
import com.weakteam.mobile.taxi.data.SearchPlaceResult;
import com.weakteam.mobile.taxi.service.ITaskEvent;
import com.weakteam.mobile.taxi.service.ServerSession;
import com.weakteam.mobile.taxi.service.task.SearchPlace;
import com.weakteam.mobile.taxi.ui.activity.Main;
import com.weakteam.mobile.taxi.ui.activity.SetDestinationPoint;
import com.weakteam.mobile.taxi.ui.activity.SetDestinationPointOnMap;
import com.weakteam.mobile.taxi.ui.adapter.AddressSearchResultAdapter;
import com.weakteam.mobile.taxi.ui.adapter.HistoryFastPointAddAdapter;
import com.weakteam.mobile.taxi.ui.screen.setdestinationpoint.AddressSearchResultCell;
import com.weakteam.mobile.taxi.ui.screen.setdestinationpoint.HistoryFastPointAddCell;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Nail on 21.12.2014.
 */
public class SetDestinationPointSearch extends Fragment {

    private EditText searchField;
    private ListView historyList;
    private ListView searchResult;
    private TextView textTip;
    private boolean isHistory;
    private View clear;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_set_point_search, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.searchResult = (ListView)getActivity().findViewById(R.id.search_result);
        this.historyList = (ListView)getActivity().findViewById(R.id.historyList);
        this.clear = getActivity().findViewById(R.id.clear);

        this.clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchField.setText("");
            }
        });

        this.historyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View item, int position, long id) {
                final DestinationPoint point = ((HistoryFastPointAddCell)item).getDestinationPoint();
                final DestinationPoint activePoint = DestinationPointManager.getInstance().getActivePoint();
                activePoint.setWithPoint(point);
                if (activePoint.isFrom()) {
                    SetDestinationPoint activity = (SetDestinationPoint)SetDestinationPointSearch.this.getActivity();
                    activity.setFields();
                    activity.getFieldsFragment().setFocusPorch(true);
                } else {
                    Intent intent = new Intent(SetDestinationPointSearch.this.getActivity(), Main.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                }
            }
        });

        final ImageView onMap = (ImageView)getActivity().findViewById(R.id.pointOnMap);
        onMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SetDestinationPointSearch.this.getActivity(), SetDestinationPointOnMap.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });

        searchResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View item, int position, long id) {
                final SearchPlaceResult searchPlaceResult = ((AddressSearchResultCell)item).getSearchPlaceResult();
                DestinationPointManager.getInstance().getActivePoint().setWithSearchPlaceResult(searchPlaceResult);

                // чистим поисковый лист
                final AddressSearchResultAdapter adapter = new AddressSearchResultAdapter(SetDestinationPointSearch.this.getActivity(), new ArrayList<SearchPlaceResult>());
                searchResult.setAdapter(adapter);

                ((SetDestinationPoint)SetDestinationPointSearch.this.getActivity()).setFields();
            }
        });

        this.textTip = (TextView)getActivity().findViewById(R.id.tip);

        this.searchField = (EditText)getActivity().findViewById(R.id.search_field);

        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String key = editable.toString();

                if (key.length() > 8) {
                    clear.setVisibility(View.VISIBLE);
                } else {
                    clear.setVisibility(View.GONE);
                }
                if (key.length() == 0) {
                    setHistory();
                    clear.setVisibility(View.GONE);
                } else if (key.length() > 2 && key.length() < 24) {

                    final SearchPlace task = ServerSession.getInstance().buildAsyncRequest(SearchPlace.class);
                    task.setKey(key);

                    task.setOnPostExecute(new ITaskEvent() {
                        @Override
                        public void onAction() {
                            if (Minimum.getInstance().isOnTop(SetDestinationPointSearch.this.getActivity())) {
                                final List<SearchPlaceResult> places = new ArrayList<>();

                                final List<SearchPlaceResult> historyList = HistoryManager.getInstance().getPointList(key);

                                final List<SearchPlaceResult> searchedPlaces = task.getSearchPlaceResultList();

                                if (historyList != null) {
                                    places.addAll(historyList);
                                }
                                if (searchedPlaces != null) {
                                    // уберем себя же из поиска
                                    if (searchedPlaces.size() == 1) {
                                        final String name = searchedPlaces.get(0).getName();
                                        if (searchField.getText().toString().startsWith(name)) {
                                            searchedPlaces.remove(0);
                                        }
                                    }

                                    places.addAll(searchedPlaces);
                                }

                                if (places.size() > 0) {
                                    setSearch();
                                } else {
                                    setEmpty();
                                    searchResult.setVisibility(View.GONE);
                                    textTip.setVisibility(View.VISIBLE);
                                }

                                final AddressSearchResultAdapter adapter = new AddressSearchResultAdapter(SetDestinationPointSearch.this.getActivity(), places);
                                searchResult.setAdapter(adapter);
                            }
                        }
                    });
                    task.send();
                } else {
                    setEmpty();
                }
            }
        });
    }

    private void setHistory() {
        this.searchResult.setVisibility(View.GONE);
        if (isHistory) {
            this.historyList.setVisibility(View.VISIBLE);
            this.textTip.setVisibility(View.GONE);
        } else {
            this.textTip.setVisibility(View.VISIBLE);
        }
    }

    private void setEmpty() {
        this.searchResult.setVisibility(View.GONE);
        this.historyList.setVisibility(View.GONE);
        this.textTip.setVisibility(View.VISIBLE);
    }

    private void setSearch() {
        this.searchResult.setVisibility(View.VISIBLE);
        this.historyList.setVisibility(View.GONE);
        this.textTip.setVisibility(View.GONE);
    }


    @Override
    public void onResume() {
        super.onResume();
        final DestinationPoint point = DestinationPointManager.getInstance().getActivePoint();
        if (point.getStreet()!= null && point.getStreet().length() > 0) {
            this.searchField.setText(point.getStreet());
        }
        this.searchField.requestFocus();

        final List<DestinationPoint> points = HistoryManager.getInstance().getPointList();

        if (points!= null && points.size() > 0) {
            this.isHistory = true;
            final HistoryFastPointAddAdapter adapter = new HistoryFastPointAddAdapter(this.getActivity(), points);
            this.historyList.setAdapter(adapter);
            this.setHistory();
        } else {
            this.isHistory = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }
}