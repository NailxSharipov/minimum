package com.weakteam.mobile.taxi.ui.screen.main;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.model.CityData;

/**
 *  Created by Nail on 25.01.2015.
 */
public class CitySelectCell extends FrameLayout {

    // ui items
    private TextView title;

    private CityData cityData;

    public CityData getCityData() {
        return cityData;
    }

    public void setCityData(CityData cityData) {
        this.cityData = cityData;
        this.title.setText(cityData.getTitle());
    }

    public CitySelectCell(final Context context) {
        super(context);
        View view = inflate(context, R.layout.cell_city_drop_down_list, null);
        addView(view);

        this.title = (TextView)findViewById(R.id.title);

    }
}