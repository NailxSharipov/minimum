package com.weakteam.mobile.taxi.ui.screen.main;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;

/**
 * Created by Nail on 16.12.2014.
 */
public class DestinationPointCell extends FrameLayout {

    // ui items
    private TextView destination;
    private FrameLayout cross;
    private View divider;

    private DestinationPoint destinationPoint;

    public void setLast(boolean isLast) {
        if (isLast) {
            this.divider.setVisibility(GONE);
        } else {
            this.divider.setVisibility(VISIBLE);
        }
    }

    public DestinationPoint getDestinationPoint() {
        return destinationPoint;
    }

    public void setDestinationPoint(DestinationPoint point) {
        this.destinationPoint = point;
        this.destination.setText(point.getText());
        this.setVisibility(View.VISIBLE);
    }

    public TextView getDestination() {
        return destination;
    }

    public FrameLayout getCross() {
        return cross;
    }

    public DestinationPointCell(final Context context) {
        super(context);
        View view = inflate(context, R.layout.cell_destination_point, null);
        addView(view);

        this.destination = (TextView)findViewById(R.id.destination);
        this.cross = (FrameLayout)findViewById(R.id.cross);
        this.divider = findViewById(R.id.divider);
    }
}