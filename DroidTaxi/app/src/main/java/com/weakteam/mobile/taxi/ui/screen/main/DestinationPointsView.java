package com.weakteam.mobile.taxi.ui.screen.main;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.data.DestinationPointManager;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by Nail on 16.12.2014.
 */
public class DestinationPointsView extends FrameLayout {

    public interface PointViewEvents {
        void onSelect(DestinationPoint destinationPoint);

        void onClose();
    }

    private Context context;

    private DestinationPointManager manager;
    private List<DestinationPointCell> cellList;
    private RelativeLayout plus;

    private LinearLayout contentView;

    private PointViewEvents pointViewEvents;

    public void setPointViewEvents(PointViewEvents pointViewEvents) {
        this.pointViewEvents = pointViewEvents;
    }

    public DestinationPointsView(Context context) {
        super(context);
        this.init(context);
    }

    public DestinationPointsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public DestinationPointsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.init(context);
    }
/*
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DestinationPointsView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.init(context);
    }
*/
    private void init(Context context) {
        this.context = context;
        View view = inflate(context, R.layout.view_destination_points, null);
        addView(view);

        this.plus = (RelativeLayout)findViewById(R.id.plus);

        plus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DestinationPointManager.getInstance().getSize() < 7) {
                    DestinationPointManager.getInstance().addPoint();
                    reloadData();
                }
            }
        });

        this.contentView = (LinearLayout)findViewById(R.id.content);

        this.manager = DestinationPointManager.getInstance();

        this.cellList = new ArrayList<>();

        this.reloadData();
    }

    public void reloadData() {
        int cellCount = this.cellList.size();
        int dataCount = this.manager.getPoints().size();

        // add new cells
        {
            for(int i = cellCount; i < dataCount; i++) {
                final DestinationPointCell cell = new DestinationPointCell(this.context);
                cell.getCross().setOnClickListener(onCross);
                cell.getDestination().setOnClickListener(onMain);
                cell.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                this.cellList.add(cell);
                this.contentView.addView(cell);
            }
        }
        // hide unused cells
        for(int i = dataCount; i < cellCount; i++) {
            this.cellList.get(i).setVisibility(View.GONE);
        }

        // set data
        for(int i = 0; i < dataCount; i++) {
            final DestinationPointCell cell = this.cellList.get(i);
            cell.setLast(i == (dataCount - 1));
            final DestinationPoint point = this.manager.getPoints().get(i);
            cell.setDestinationPoint(point);
        }

        if (dataCount >= 6) {
            this.plus.setVisibility(View.GONE);
        } else {
            this.plus.setVisibility(View.VISIBLE);
        }

    }

    private OnClickListener onCross = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final DestinationPointCell cell = (DestinationPointCell) view.getParent().getParent().getParent();
            final DestinationPoint destinationPoint = cell.getDestinationPoint();

            final boolean isEmpty = destinationPoint.getObjectId() == null && destinationPoint.getStreet() == null;

            DestinationPointManager.getInstance().removePlace(destinationPoint);
            reloadData();

            if (!isEmpty) {
                pointViewEvents.onClose();
            }
        }
    };

    private OnClickListener onMain = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final DestinationPointCell cell = (DestinationPointCell) view.getParent().getParent().getParent();
            pointViewEvents.onSelect(cell.getDestinationPoint());
        }
    };

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        onTouchEvent(ev);
        return false;
    }
}
