package com.weakteam.mobile.taxi.ui.screen.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;
import com.weakteam.mobile.taxi.data.History;
import com.weakteam.mobile.taxi.data.Order;

import java.util.Iterator;
import java.util.List;

/**
 *  Created by Nail on 12.04.2015.
 */
public class HistoryOrderCell extends LinearLayout {

    private TextView textView;
    private History history;

    public History getHistory() {
        return history;
    }

    public void setHistory(final History history) {
        this.history = history;
        final Order order = history.getOrder();
        final List<DestinationPoint> points = order.getPoints();

        final StringBuilder address = new StringBuilder();
        for (Iterator<DestinationPoint> it = points.iterator(); it.hasNext();) {
            final DestinationPoint point = it.next();
            String ad = point.getObjectAddress().replace('\n', ' ');
            if (ad.length() > 28) {
                ad = ad.substring(0, 28);
            }
            address.append(ad);

            if (it.hasNext()) {
                address.append('\n');
            }
        }
        this.textView.setText(address.toString());
    }

    public HistoryOrderCell(final Context context) {
        super(context);
        final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.cell_history_order, this);
        this.textView = (TextView)this.findViewById(R.id.address);
    }
}