package com.weakteam.mobile.taxi.ui.screen.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.Order;
import com.weakteam.mobile.taxi.data.OrderManager;
import com.weakteam.mobile.taxi.ui.adapter.OrderAdapter;

/**
 *  Created by Nail on 12.04.2015.
 */
public class OrderCell extends LinearLayout {

    private OrderAdapter.ICancelOrder cancelOrderListener;

    private Button cancelButton;
    private TextView driver;
    private TextView carDescription;
    private TextView carNumber;
    private TextView status;
    private TextView toPoint;
    private TextView fromPoint;
    private TextView price;

    public void setOrder(final Order order) {
        final int code = order.getCode();
        if (OrderManager.getInstance().isCanceledOrder(order.getOrderId()) || code == 102 || code == 103) {
            cancelButton.setVisibility(View.INVISIBLE);
            order.setStatus("Отказ без машины");
        } else {
            cancelButton.setVisibility(View.VISIBLE);
            cancelButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelOrderListener.onCancelOrder(order);
                }
            });
        }

        fromPoint.setText(order.getFromPoint());
        toPoint.setText(order.getToPoint());
        status.setText(order.getStatus());

        if (order.getCar() != null) {
            carDescription.setText(order.getColor() + " " + order.getCar());
            carNumber.setText(order.getCarNumber());
            final String phone = order.getPhone();
            if (phone != null && phone.length() > 5) {
                driver.setText("номер водителя " + phone);
            } else {
                driver.setText("");
            }
        } else {
            carDescription.setText("");
            carNumber.setText("");
            driver.setText("");
        }

        price.setText(order.getPrice());
    }

    public OrderCell(final Context context) {
        super(context);
        final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.cell_order, this);

        this.cancelButton = (Button)findViewById(R.id.cancelButton);
        this.driver = (TextView)findViewById(R.id.driver);
        this.carDescription = (TextView)this.findViewById(R.id.carDescription);
        this.carNumber = (TextView)this.findViewById(R.id.carNumber);
        this.status = (TextView)this.findViewById(R.id.status);
        this.toPoint = (TextView)this.findViewById(R.id.toPoint);
        this.fromPoint = (TextView)this.findViewById(R.id.fromPoint);
        this.cancelButton = (Button)this.findViewById(R.id.cancelButton);
        this.price = (TextView)this.findViewById(R.id.price);
    }

    public void setCancelOrderListener(OrderAdapter.ICancelOrder cancelOrderListener) {
        this.cancelOrderListener = cancelOrderListener;
    }

}
