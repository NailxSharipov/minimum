package com.weakteam.mobile.taxi.ui.screen.setdestinationpoint;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weakteam.mobile.taxi.Minimum;
import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.Location;
import com.weakteam.mobile.taxi.data.SearchPlaceResult;

/**
 *  Created by Nail on 21.12.2014.
 */
public class AddressSearchResultCell extends LinearLayout {

    // ui items
    private TextView addressType;
    private TextView addressTitle;

    private SearchPlaceResult searchPlaceResult;

    public SearchPlaceResult getSearchPlaceResult() {
        return searchPlaceResult;
    }

    public void setSearchPlaceResult(SearchPlaceResult result) {
        this.searchPlaceResult = result;

        String name = result.getName();
        if (result.getHome()!= null) {
            name = name + " " + result.getHome();
        }
        this.addressTitle.setText(name);
        if (result.getLocationType() == Location.Type.Street) {
            this.addressType.setText(Minimum.getInstance().getResources().getString(R.string.location_street));
        } else if (result.getLocationType() == Location.Type.Station) {
            this.addressType.setText(Minimum.getInstance().getResources().getString(R.string.location_station));
        } else if (result.getLocationType() == Location.Type.Other) {
            this.addressType.setText(Minimum.getInstance().getResources().getString(R.string.location_object));
        } else {
            this.addressType.setText("");
        }

    }

    public AddressSearchResultCell(final Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null){
            inflater.inflate(R.layout.cell_address_search_result, this);
        }

        //this.inflate(activity, R.layout.cell_address_search_result, null);
        this.addressType = (TextView)findViewById(R.id.addressType);
        this.addressTitle = (TextView)findViewById(R.id.addressTitle);
    }
}