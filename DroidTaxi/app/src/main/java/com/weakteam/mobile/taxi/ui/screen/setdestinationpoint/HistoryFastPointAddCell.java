package com.weakteam.mobile.taxi.ui.screen.setdestinationpoint;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.data.DestinationPoint;

/**
 *  Created by Nail on 18.01.2015.
 */
public class HistoryFastPointAddCell extends LinearLayout {

    // ui items
    private TextView address;

    private DestinationPoint point;

    public DestinationPoint getDestinationPoint() {
        return point;
    }

    public void setDestinationPoint(DestinationPoint point) {
        this.point = point;

        this.address.setText(point.getObjectAddress());
    }

    public HistoryFastPointAddCell(final Context context) {
        super(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater != null) {
            inflater.inflate(R.layout.cell_history_fast_point_add, this);
        }

        this.address = (TextView)findViewById(R.id.address);
    }
}