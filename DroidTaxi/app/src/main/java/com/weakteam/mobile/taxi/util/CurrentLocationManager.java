package com.weakteam.mobile.taxi.util;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 11.11.2014.
 */
public class CurrentLocationManager implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public interface IPosition {
        void onUpdate(Location position);
    }

    private static CurrentLocationManager instance;

    private IPosition iPosition;

    public void setIPosition(IPosition iPosition) {
        this.iPosition = iPosition;
    }

    private GoogleApiClient mGoogleApiClient;

    public static CurrentLocationManager getInstance() {
        if (instance == null) {
            instance = new CurrentLocationManager();
        }
        return instance;
    }

    private Location position;

    public Location getPosition() {
        return position;
    }

    public void connect(final Context context) {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    public void disconnect() {
        // Disconnecting the client invalidates it.
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        final LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5000); // Update location every second

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest,
                this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        SimpleLog.info("GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        SimpleLog.info("GoogleApiClient connection has failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        this.position = location;
        if (this.iPosition != null) {
            this.iPosition.onUpdate(location);
        }
        SimpleLog.info("Location received: " + location.toString());
    }
}
