package com.weakteam.mobile.taxi.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

import java.util.HashMap;

/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 10.11.2014.
 */
public class FontManager {

    private static final HashMap<String, Typeface> fontsCache = new HashMap<>();

    public static Typeface loadFont(String fontName, Context context) {
        Typeface font = fontsCache.get(fontName);

        if (font == null) {
            //font = Typeface.createFromAsset(activity.getAssets(), "assets/fonts/" + fontName);
            final AssetManager assetManager = context.getAssets();
            font = Typeface.createFromAsset(assetManager, fontName);
            fontsCache.put(fontName, font);
        }

        return font;
    }
}
