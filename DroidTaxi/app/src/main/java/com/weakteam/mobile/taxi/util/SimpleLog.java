package com.weakteam.mobile.taxi.util;

import android.util.Log;

/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 20.10.2014.
 */
public class SimpleLog {

    private static final String TAG = "SimpleLog";

    public static void info(final String message) {
        Log.i(TAG, message);
    }

}
