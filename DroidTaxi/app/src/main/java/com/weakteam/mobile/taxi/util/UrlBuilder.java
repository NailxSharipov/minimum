package com.weakteam.mobile.taxi.util;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 11.11.2014.
 */
public class UrlBuilder {

    private final Map<String, String> parameterMap;
    private final List<String> emptyParameterList;
    private String serverURL;

    public UrlBuilder() {
        this.parameterMap = new HashMap<>();
        this.emptyParameterList = new ArrayList<>();
    }

    public UrlBuilder setServerURL(String serverURL) {
        this.serverURL = serverURL;
        return this;
    }

    public UrlBuilder addParameter(String key, String value) {
        this.parameterMap.put(key, value);
        return this;
    }

    public UrlBuilder addEmptyParameter(String value) {
        this.emptyParameterList.add(value);
        return this;
    }

    public String getFormattedURL() {
        final StringBuilder builder = new StringBuilder(this.serverURL);

        final boolean isParameter = !this.parameterMap.isEmpty();
        final boolean isEmptyParameter = !this.emptyParameterList.isEmpty();

        if (isParameter || isEmptyParameter) {
            builder.append("?");
        }

        if (isParameter) {
            final Set<String> keys = this.parameterMap.keySet();
            for (Iterator<String> iterator = keys.iterator(); iterator.hasNext(); ) {
                final String key = iterator.next();
                try {
                    final String value = this.parameterMap.get(key);
                    if (value != null) {
                        final String encoded = URLEncoder.encode(value, "UTF-8");
                        builder.append(key).append("=").append(encoded);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (iterator.hasNext()) {
                    builder.append("&");
                }
            }
        }

        if (isEmptyParameter) {
            if (isParameter) {
                builder.append("&");
            }

            for (Iterator<String> iterator = this.emptyParameterList.iterator(); iterator.hasNext(); ) {
                final String key = iterator.next();
                builder.append(key);
                if (iterator.hasNext()) {
                    builder.append("&");
                }
            }
        }
        return builder.toString();
    }

    public URL getURL() {
        URL url = null;
        try {
            url = new URL(this.getFormattedURL());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }
}
