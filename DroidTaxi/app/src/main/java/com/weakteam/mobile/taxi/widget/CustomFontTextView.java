package com.weakteam.mobile.taxi.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.weakteam.mobile.taxi.R;
import com.weakteam.mobile.taxi.util.FontManager;

/**
 *  Created by Nail Sharipov (nailxsharipov@gmail.com) on 10.11.2014.
 */
public class CustomFontTextView extends TextView {

    public CustomFontTextView(Context context) {
        super(context);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        /*
        Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "B52.ttf");
        this.setTypeface(myTypeface);
        */
        if (!isInEditMode()) {
            TypedArray atSet = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CustomFontText,
                    0, 0);
            try {
                final String fontName = atSet.getString(R.styleable.CustomFontText_customFont);
                if (fontName == null) {
                    throw new IllegalArgumentException("You must provide customFont for your edit text");
                } else {
                    final Typeface customTypeface = FontManager.loadFont(fontName, context);
                    setTypeface(customTypeface);
                }
            } finally {
                atSet.recycle();
            }
        }

    }
}