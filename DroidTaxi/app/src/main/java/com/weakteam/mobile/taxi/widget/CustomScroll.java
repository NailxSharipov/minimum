package com.weakteam.mobile.taxi.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Created by Nail on 22.12.2014.
 */
public class CustomScroll extends ScrollView {

    public CustomScroll(Context context) {
        this(context, null);
    }

    public CustomScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomScroll(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(@NonNull MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent ev) {
        // no more tocuh events for this ScrollView
        return false;
    }

    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        // although the ScrollView doesn't get touch events , its children will get them so intercept them.
        return super.dispatchTouchEvent(ev);
    }
}
