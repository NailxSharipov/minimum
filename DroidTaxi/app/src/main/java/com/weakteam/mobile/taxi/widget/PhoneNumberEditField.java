package com.weakteam.mobile.taxi.widget;

import android.content.Context;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

/**
 *  Created by Nail Sharipov on 16.10.2014.
 */
public class PhoneNumberEditField extends EditText {

    public static final int PHONE_NUMBER_LENGTH = 10;
    private static final String COUNTRY_CODE = "+7";
    private final StringBuilder numberBuilder = new StringBuilder();

    public String getPhoneNumber() {
        return this.numberBuilder.toString();
    }
    public void setPhoneNumber(String phoneNumber) {
        this.numberBuilder.delete(0, this.numberBuilder.length());
        this.numberBuilder.append(phoneNumber);
        this.format();
    }
    public PhoneNumberEditField(Context context) {
        super(context);
        init();
    }

    public PhoneNumberEditField(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public PhoneNumberEditField(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            if (before > count) {
                if (numberBuilder.length() > 0) {
                    removeTextChangedListener(watcher);
                    numberBuilder.deleteCharAt(numberBuilder.length() - 1);
                    //format();
                    addTextChangedListener(watcher);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

            removeTextChangedListener(watcher);
            format();
            addTextChangedListener(watcher);

        }
    };

    private void init() {

        this.setText(COUNTRY_CODE);
        this.placeCursorToEnd();

        this.addTextChangedListener(watcher);

        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode >= KeyEvent.KEYCODE_0 && keyCode <= KeyEvent.KEYCODE_9 && numberBuilder.length() < PHONE_NUMBER_LENGTH) {
                        int number = keyCode - KeyEvent.KEYCODE_0;
                        PhoneNumberEditField.this.numberBuilder.append(number);
                    }
                }
                return false;
            }
        });


        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                PhoneNumberEditField.this.placeCursorToEnd();
            }
        });
    }

    public void format() {
        final String number = this.numberBuilder.toString();

        // build format string
        final int len = number.length();

        StringBuilder buffer = new StringBuilder();
        buffer.append(COUNTRY_CODE);

        if (len > 0) {
            buffer.append(" (");
            buffer.append(number.substring(0, Math.min(3, len)));
        }

        if (len > 2) {
            buffer.append(") ");
            buffer.append(number.substring(3, Math.min(6, len)));
        }

        if (len >= 6) {
            buffer.append("-");
            buffer.append(number.substring(6, Math.min(8, len)));
        }

        if (len >= 8) {
            buffer.append("-");
            buffer.append(number.substring(8, Math.min(PHONE_NUMBER_LENGTH, len)));
        }

        final String curText = this.getText().toString();

        if (curText.compareTo(buffer.toString()) != 0) {
            this.setText(buffer);
            if (curText.length() > 1) {
                this.placeCursorToEnd();
            }
        }
    }

    private void placeCursorToEnd() {
        final Editable eText = this.getText();
        Selection.setSelection(eText, eText.length());
    }

}
